﻿#if UNITY_EDITOR

#elif UNITY_ANDROID
#define PLAYFAB_ANDROID
#elif UNITY_IOS
#define PLAYFAB_IOS
#endif

using UnityEngine;

using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;

public class PlayFabManager : MonoBehaviour {
	public string TitleId;
	public string callStatus = "** PlayFab Console ** \n Run on an Android or iOS device to see automatic device ID authentication.";
    private const string TEST_STAT_NAME = "Challange";
    public string PlayFabId;
    public int totalWins;
    private int totalWinsOld;

    private bool leaderboardLoaded = false;
    private bool showLeaderboard;

    private Dictionary<string, uint> leaderboardHighScores = new Dictionary<string, uint>();

	void Awake () {
		PlayFabSettings.TitleId = TitleId;
	}

	void Start () {
		Debug.Log("Starting Auto-login Process");
        #if UNITY_EDITOR
        LoginWithCustomIDRequest(PlayFabSettings.TitleId);
		#elif PLAYFAB_IOS
		PlayFabClientAPI.LoginWithIOSDeviceID (new LoginWithIOSDeviceIDRequest
		{
			DeviceId = SystemInfo.deviceUniqueIdentifier,
			OS = SystemInfo.operatingSystem,
			DeviceModel = SystemInfo.deviceModel,
			CreateAccount = true
		}, onLoginSuccess, null);
		#elif PLAYFAB_ANDROID
		PlayFabClientAPI.LoginWithAndroidDeviceID (new LoginWithAndroidDeviceIDRequest
		{
			AndroidDeviceId = SystemInfo.deviceUniqueIdentifier,
			OS = SystemInfo.operatingSystem,
			AndroidDevice = SystemInfo.deviceModel,
			CreateAccount = true
		}, onLoginSuccess, null);
		#endif
	}


	private void onLoginSuccess(LoginResult result)
	{
        
        Time.timeScale = 1.0f;	// unpause...
        PlayFabData.AuthKey = result.SessionTicket;
		this.callStatus = string.Format("PlayFab Authentication Successful! -- Player ID:{0}", result.PlayFabId);
		Debug.Log(callStatus);
        
        ConfigurePlayer();
	}

    private void onLoginError(PlayFabError error)
    {
        this.callStatus = string.Format("Error {0}: {1}", error.HttpCode, error.ErrorMessage);
        Debug.Log(callStatus);
    }

    public void ConfigurePlayer()
    {
        if (PlayFabData.AuthKey != null) LoadUserData();
        else PlayFabData.LoggedIn += LoadUserData;


        leaderboardLoaded = false;
        if (PlayFabData.AuthKey != null) refreshLeaderboard();
        else PlayFabData.LoggedIn += refreshLeaderboard;

        if (PlayFabData.AuthKey != null) ConfigurePlayerName();
        else PlayFabData.LoggedIn += ConfigurePlayerName;

        InvokeRepeating("SavePlayerState", 10, 5);
    }

    public void ConfigurePlayerName(string authKey = null)
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = SystemInfo.deviceName,

        }, SetPlayerName, null);
      //  PlayFabClientAPI.UpdateUserTitleDisplayName req = new PlayFabClientAPI.UpdateUserTitleDisplayName();
      //  UpdateDisplayNameRequest udnRequest = new UpdateDisplayNameRequest.DisplayName("CherryWorm").build();
    }

    private void SetPlayerName(UpdateUserTitleDisplayNameResult result) {
        Debug.Log("Player name Updated");
    }

    private void LoadUserData(string authKey = null)
    {
        GetUserDataRequest request = new GetUserDataRequest();
        PlayFabClientAPI.GetUserData(request, SetPlayerData, OnPlayFabError);
    }

    private void SetPlayerData(GetUserDataResult result)
    {
        Debug.Log("Player data loaded.");
        if (result.Data.ContainsKey("TotalWins"))
            PlayFabGameBridge.totalWins = int.Parse(result.Data["TotalWins"].Value);
        else
            PlayFabGameBridge.totalWins = 0;
        totalWins = PlayFabGameBridge.totalWins;
    }

    void OnPlayFabError(PlayFabError error)
    {
        Debug.Log("Got an error: " + error.ErrorMessage);
    }

    private void SavePlayerState()
    {
        
        if (PlayFabGameBridge.totalWins != totalWinsOld && PlayFabData.AuthKey != null)
        {	// we need to save
            // first save as a player property (poor man's save game)
            Debug.Log("Saving player data...");
            UpdateUserDataRequest request = new UpdateUserDataRequest();
            request.Data = new Dictionary<string, string>();
            request.Data.Add("TotalWins", PlayFabGameBridge.totalWins.ToString());
            PlayFabClientAPI.UpdateUserData(request, PlayerDataSaved, OnPlayFabError);

            // also save score as a stat for the leaderboard use...
            Dictionary<string, int> stats = new Dictionary<string, int>();
            stats.Add("score", PlayFabGameBridge.totalWins);
            storeStats(stats);
        }
        totalWinsOld = PlayFabGameBridge.totalWins;
    }

    public void storeStats(Dictionary<string, int> stats)
    {
        PlayFab.ClientModels.UpdateUserStatisticsRequest request = new PlayFab.ClientModels.UpdateUserStatisticsRequest();
        request.UserStatistics = stats;
        if (PlayFabData.AuthKey != null)
            PlayFabClientAPI.UpdateUserStatistics(request, StatsUpdated, OnPlayFabError);
    }

    void LoginWithCustomIDRequest(string titleId)
    {
        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest()
        {
            TitleId = titleId,
            CreateAccount = true,
            CustomId = SystemInfo.deviceUniqueIdentifier
        };

        PlayFabClientAPI.LoginWithCustomID(request, (result) =>
        {
            PlayFabId = result.PlayFabId;

            if (result.NewlyCreated)
            {
                Debug.Log("(new account)");
            }
            else
            {
                Debug.Log("(existing account)");
            }
            onLoginSuccess(result);
        },
        (error) =>
        {
            onLoginError(error);
            
        });
    }

    private void PlayerDataSaved(UpdateUserDataResult result)
    {
        Debug.Log("Player Data saved.");
    }

    private void StatsUpdated(PlayFab.ClientModels.UpdateUserStatisticsResult result)
    {
        Debug.Log("Stats updated");
    }

    public void refreshLeaderboard(string authKey = null)
    {
        PlayFab.ClientModels.GetLeaderboardRequest request = new PlayFab.ClientModels.GetLeaderboardRequest();
        request.MaxResultsCount = 50;
        request.StatisticName = "Score";

        PlayFabClientAPI.GetLeaderboard(request, ConstructLeaderboard, OnPlayFabError);
    }

    private void ConstructLeaderboard(PlayFab.ClientModels.GetLeaderboardResult result)
    {
        leaderboardHighScores.Clear();

      
        foreach (PlayFab.ClientModels.PlayerLeaderboardEntry entry in result.Leaderboard)
        {
            if (entry.DisplayName != null)
                leaderboardHighScores.Add(entry.DisplayName, (uint)entry.StatValue);
            else
                leaderboardHighScores.Add(entry.PlayFabId, (uint)entry.StatValue);
            
            
        }

        PlayFabGameBridge.LeaderboardHighScores = leaderboardHighScores;
        Debug.Log(result.Leaderboard[0].DisplayName + "----------------------------------");
        leaderboardLoaded = true;
        PlayFabGameBridge.leaderboardLoaded = leaderboardLoaded;
    }



}
public class UpdateDisplayNameRequest : RegisterPlayFabUserRequest {
    public string DisplayName { get; set; }
}
