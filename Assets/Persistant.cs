﻿using UnityEngine;
using System.Collections;

public class Persistant : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(GameObject.FindGameObjectsWithTag("GameManager").Length > 1) 
		{
			Destroy(this.gameObject);
		}
		else
		{
			DontDestroyOnLoad(this.gameObject);
		}
	
	}

}
