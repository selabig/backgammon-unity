﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.Events;

public class MyMessageBox : MessageBox
{

    public static MessageBox Show(string message, Action<string> onFinished, MessageBoxButtons buttons = MessageBoxButtons.OK)
    {
        return Show(message, null, onFinished, buttons);
    }

    public static MessageBox Show(string message, string title = null, Action<string> onFinished = null, MessageBoxButtons buttons = MessageBoxButtons.OK)
    {
        var box = MessageBox.Show( message, title, onFinished, buttons);

        //thesun
        //GameObject.Find ("UI Root/Camera").GetComponent<UICamera>().enabled = false;
        UICamera.eventHandler.useTouch = false;
        UICamera.eventHandler.useMouse = false;

		float scaleFactor = (NGUITools.screenSize.x + NGUITools.screenSize.y) / (1136.0f + 640.0f); 

        if (TSUtil.isIPad())
        {
            scaleFactor = (NGUITools.screenSize.x + NGUITools.screenSize.y) / (1024.0f + 768.0f);
        }
        scaleFactor *= 1.5f;

        GameObject panel = box.transform.Find("Panel").gameObject;
        panel.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);

        panel.transform.localEulerAngles = new Vector3(0, 0, 0);
        if (TSUtil.isIPhone())
        {
            if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Landscape)
            {
                panel.transform.localEulerAngles = new Vector3(0, 0, 90);
            }
        }

        return box;
    }


    public override void Close()
    {
        //thesun
        // UICamera.current.enabled = true;
        //GameObject.Find ("UI Root/Camera").GetComponent<UICamera>().enabled = true;
        UICamera.eventHandler.useTouch = true;
        UICamera.eventHandler.useMouse = true;

        base.Close();        
    }
}
