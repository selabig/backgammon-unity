﻿using UnityEngine;
using System.Collections;

public class SuccessPopUp : BaseMenu
{

    public GameObject exitPopup;
    protected bool isProcessing = false;

    public void ExitPopUpYesBtnCallback()
    {
        if (isProcessing == false)
        {
            GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
            isProcessing = true;
            TweenScale tw = exitPopup.GetComponent<TweenScale>();
            tw.PlayReverse();
            tw.SetOnFinished(ExitPopUpAndRestart);
        }
    }

    protected void ExitPopUpAndRestart()
    {

        GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewMatch();

        //thesun
        HidePopUp();

        //GameManager.Instance.menuHandler.CloseMenu();

        //		Application.LoadLevel (Application.loadedLevelName);

        //MenuManager.Instance.PopMenuToState(GameManager.GameState.LevelSelection);

        //        MenuManager.Instance.PushMenu(GameManager.GameState.GamePlay);

    }

    public void ExitPopUpNoBtnCallback()
    {
        if (isProcessing == false)
        {
            GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
            isProcessing = true;
            TweenScale tw = exitPopup.GetComponent<TweenScale>();
            tw.PlayReverse();
            tw.SetOnFinished(ExitPopUpAndMainMenu);
        }

    }

    protected void ExitPopUpAndMainMenu()
    {
        isProcessing = false;
        MenuManager.Instance.PopMenuToState(GameManager.GameState.MainMenu);
    }

    protected void HidePopUp()
    {
        isProcessing = false;
        MenuManager.Instance.PopMenu();
    }

    void OnEnable()
    {
        if (isProcessing == false)
        {
            isProcessing = true;
            exitPopup.transform.localScale = new Vector3(0, 0, 0);
            TweenScale tw = exitPopup.GetComponent<TweenScale>();
            tw.PlayForward();
            tw.SetOnFinished(CompletedProcessing);
        }

        this.transform.localEulerAngles = new Vector3(0, 0, 0);
        if (TSUtil.isIPhone())
        {
            if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Landscape)
            {
                this.transform.localEulerAngles = new Vector3(0, 0, -90);
            }
        }
    }

    protected void CompletedProcessing()
    {
        isProcessing = false;
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Win);
    }
}
