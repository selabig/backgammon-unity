﻿using UnityEngine;
using System.Collections;

public class ExitPopUp : BaseMenu
{

    public GameObject exitPopup;
    private bool isProcessing = false;

    public void ExitPopUpYesBtnCallback()
    {
        if (isProcessing == false)
        {
            GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
            isProcessing = true;
            exitPopup.GetComponent<TweenScale>().PlayReverse();
            Invoke("ExitPopUpAndMainMenu", 1f);
        }
    }

    void ExitPopUpAndMainMenu()
    {
        isProcessing = false;
        MenuManager.Instance.PopMenuToState(GameManager.GameState.LevelSelection);
        GameManager.Instance.menuHandler.CloseMenu();
    }

    public void ExitPopUpNoBtnCallback()
    {
        if (isProcessing == false)
        {
            GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
            isProcessing = true;
            exitPopup.GetComponent<TweenScale>().PlayReverse();
            Invoke("HidePopUp", 1f);
        }

    }

    void HidePopUp()
    {
        isProcessing = false;
        MenuManager.Instance.PopMenu();
    }

    void OnEnable()
    {
        if (isProcessing == false)
        {
            isProcessing = true;
            exitPopup.transform.localScale = new Vector3(0, 0, 0);
            exitPopup.GetComponent<TweenScale>().PlayForward();
            Invoke("CompletedProcessing", 1f);
        }
    }

    void CompletedProcessing()
    {
        isProcessing = false;
    }
}
