﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class SettingPanel : BaseMenu
{
    public GameObject switchSound;
    public GameObject switchShowPossibleMove;
    public GameObject switchOneTouchMove;
    public GameObject switchDoubleCube;
    public GameObject switchInGameNotification;
    public GameObject switchColorOfCheckers;
    public GameObject switchBoardViewPortrait;
    public GameObject switchBoardViewLandscape;
    public GameObject switchDifficultyEasy;
    public GameObject switchDifficultyMedium;
    public GameObject switchDifficultyHard;

    public GameObject switchShowPipCount;

    public GameObject switchMatchTo1;
    public GameObject switchMatchTo3;
    public GameObject switchMatchTo5;
    public GameObject switchMatchTo7;
    public GameObject switchMatchTo11;
    public GameObject switchMatchTo15;



    public override void Awake()
    {
        base.Awake();


        if (!PlayerPrefs.HasKey("SET_DEFAULT_SETTING"))
        {
            PlayerPrefs.SetInt("SET_DEFAULT_SETTING", 1);

        }


        switchSound.gameObject.GetComponent<UIToggle>().value = TSUtil.Instance.SettingSoundEnable;
        switchShowPossibleMove.gameObject.GetComponent<UIToggle>().value = TSUtil.Instance.SettingShowPossibleMove;
        switchOneTouchMove.gameObject.GetComponent<UIToggle>().value = TSUtil.Instance.SettingOneTouchMove;
        switchDoubleCube.gameObject.GetComponent<UIToggle>().value = TSUtil.Instance.SettingDoubleCube;
        switchInGameNotification.gameObject.GetComponent<UIToggle>().value = TSUtil.Instance.SettingInGameNotification;
        switchColorOfCheckers.gameObject.GetComponent<UIToggle>().value = TSUtil.Instance.SettingColorCheckers;
        switchShowPipCount.gameObject.GetComponent<UIToggle>().value = TSUtil.Instance.SettingShowPipCount;


        if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Portrait)
            switchBoardViewPortrait.gameObject.GetComponent<UIToggle>().value = true;
        else
            switchBoardViewLandscape.gameObject.GetComponent<UIToggle>().value = true;

        if (TSUtil.Instance.SettingDifficulty == (int)TSUtil.DifficulltyLevel.Easy)
            switchDifficultyEasy.gameObject.GetComponent<UIToggle>().value = true;
        else if (TSUtil.Instance.SettingDifficulty == (int)TSUtil.DifficulltyLevel.Medium)
            switchDifficultyMedium.gameObject.GetComponent<UIToggle>().value = true;
        else if (TSUtil.Instance.SettingDifficulty == (int)TSUtil.DifficulltyLevel.Hard)
            switchDifficultyHard.gameObject.GetComponent<UIToggle>().value = true;

        if (TSUtil.Instance.SettingMatchToWin == 1)
            switchMatchTo1.gameObject.GetComponent<UIToggle>().value = true;
        else if (TSUtil.Instance.SettingMatchToWin == 3)
            switchMatchTo3.gameObject.GetComponent<UIToggle>().value = true;
        else if (TSUtil.Instance.SettingMatchToWin == 5)
            switchMatchTo5.gameObject.GetComponent<UIToggle>().value = true;
        else if (TSUtil.Instance.SettingMatchToWin == 7)
            switchMatchTo7.gameObject.GetComponent<UIToggle>().value = true;
        else if (TSUtil.Instance.SettingMatchToWin == 11)
            switchMatchTo11.gameObject.GetComponent<UIToggle>().value = true;
        else if (TSUtil.Instance.SettingMatchToWin == 15)
            switchMatchTo15.gameObject.GetComponent<UIToggle>().value = true;


        if (TSUtil.isIPad())
        {
            this.transform.Find("PanelMainSession/MainSession/ScrollTableView/Table/BoardView").gameObject.SetActive(false);
        }
        else
        {
            this.transform.Find("PanelMainSession").transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
        }
    }

    public void DoneBtnClicked()
    {
        base.HideThis();
    }


    public void SoundBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Sound Button Clicked", 1);

        TSUtil.Instance.SettingSoundEnable = !switchSound.gameObject.GetComponent<UIToggle>().value;

    }

    public void ShowPossibleMoveBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "ShowPossibleMove Button Clicked", 1);

        TSUtil.Instance.SettingShowPossibleMove = !switchShowPossibleMove.gameObject.GetComponent<UIToggle>().value;
    }

    public void OneTouchMoveBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "OneTouchMove Button Clicked", 1);

        TSUtil.Instance.SettingOneTouchMove = !switchOneTouchMove.gameObject.GetComponent<UIToggle>().value;
    }

    public void DoubleCubeBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "DoubleCube Button Clicked", 1);

        TSUtil.Instance.SettingDoubleCube = !switchDoubleCube.gameObject.GetComponent<UIToggle>().value;
    }

    public void InGameNotificationBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "InGameNotification Button Clicked", 1);

        TSUtil.Instance.SettingInGameNotification = !switchInGameNotification.gameObject.GetComponent<UIToggle>().value;
    }

    public void ShowPipCountBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "ShowPipCount Button Clicked", 1);

        TSUtil.Instance.SettingShowPipCount = !switchShowPipCount.gameObject.GetComponent<UIToggle>().value;
    }

    public void ColorCheckersBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "ColorCheckers Button Clicked", 1);

        TSUtil.Instance.SettingColorCheckers = !switchColorOfCheckers.gameObject.GetComponent<UIToggle>().value;
    }

    public void BoardViewPortraitBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "BoardViewPortrait Button Clicked", 1);

        TSUtil.Instance.SettingBoardView = (int)TSUtil.BoardViewMode.Portrait;
    }
    public void BoardViewLandscapeBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "BoardViewLandscape Button Clicked", 1);

        TSUtil.Instance.SettingBoardView = (int)TSUtil.BoardViewMode.Landscape;
    }


    public void DifficultyEasyBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Easy Button Clicked", 1);

        TSUtil.Instance.SettingDifficulty = (int)TSUtil.DifficulltyLevel.Easy;
    }


    public void DifficultyMediumBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Medium Button Clicked", 1);

        TSUtil.Instance.SettingDifficulty = (int)TSUtil.DifficulltyLevel.Medium;
    }


    public void DifficultyHardBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Hard Button Clicked", 1);

        TSUtil.Instance.SettingDifficulty = (int)TSUtil.DifficulltyLevel.Hard;
    }

    public void MatchToBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Match To " + UIButton.current.name + " Clicked", 1);

        int tempValue;
        int.TryParse(UIButton.current.name, out tempValue);

        TSUtil.Instance.SettingMatchToWin = tempValue;
    }

    public void ShareBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Share Button Clicked", 1);

        if( Application.platform == RuntimePlatform.IPhonePlayer )
        {
            string message = "Keep the mind sharp Backgammon! http://apple.co/29Jr03p";            
            TSUtil.shareMessageCrossPlatform(message);
            return;
        }


        if (!FB.IsLoggedIn)
        {
            var perms = new List<string>() { "public_profile", "email", "user_friends" };
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }
        else
        {
            AuthCallback(null);
        }
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // // AccessToken class will have session details
            // var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // // Print current access token's User ID
            // Debug.Log(aToken.UserId);
            // // Print current access token's granted permissions
            // foreach (string perm in aToken.Permissions)
            // {
            //     Debug.Log(perm);
            // }

            StartCoroutine(ShareScreenshot());

            // ShareLink();
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    private void ShareLink()
    {
        FB.ShareLink(
            new Uri("http://apple.co/29Jr03p"),
            callback: ShareCallback
        );
    }

    private void ShareCallback(IShareResult result)
    {
        if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("ShareLink Error: " + result.Error);
        }
        else if (!string.IsNullOrEmpty(result.PostId))
        {
            // Print post identifier of the shared content
            Debug.Log(result.PostId);
        }
        else
        {
            // Share succeeded without postID
            Debug.Log("ShareLink success!");
        }
    }

    private IEnumerator ShareScreenshot()
    {
        yield return new WaitForEndOfFrame();
        Texture2D snap = new Texture2D(Screen.width, Screen.height,
                TextureFormat.RGB24, false);
        //Texture hiển thị hình vừa chụp cho người dùng
        snap.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        snap.Apply();
        byte[] scren = snap.EncodeToPNG();
        //lấy chuỗi byte của hình để post lên facebook

        var wwwForm = new WWWForm();
        // wwwForm.AddBinaryData("image", scren, "backgammon.png");

        string message = "Keep the mind sharp Backgammon! http://apple.co/29Jr03p";

        wwwForm.AddField("name", message);
        FB.API("me/photos", HttpMethod.POST, null, wwwForm);//post
    }


    public void ContactusBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "ContactUs Button Clicked", 1);


        TSUtil.SendMailCrossPlatform("support@acfreefungames.zendesk.com", "Backgammon version 1.0", "Writing a few suggestions to improve your game or just to say hi :)");
    }



}

