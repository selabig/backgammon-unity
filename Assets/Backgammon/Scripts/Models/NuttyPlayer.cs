﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NuttyPlayer : MonoBehaviour 
{

	#region Variable Declartions

	// The total score of the player i.e the total number of collectables the player has collected
	public int score = 0;

	// The total number of stars the player has collected. NOTE: this is the max number of stars the player has collected in all levels. 
	public int stars = 0; 

	// The maximum number of stars that can be collected (in our case its 25 i.e 5 levels * 5 stars)
	public int maxStars = 25;

	public Dictionary<string, int> levelScores = new Dictionary<string, int>();
	public Dictionary<string, int> levelStars = new Dictionary<string, int>();

	#endregion Variable Declartions



	#region highscore logic


	public bool PostLevelScore(string levelName, int stat)
	{
		score += stat;
		return CheckLevelStats(levelScores, levelName, stat);
	}

	public bool PostLevelStars(string levelName, int stat)
	{
		return CheckLevelStats(levelStars, levelName, stat);
	}

	public int GetLevelScore(string levelName, int stat)
	{
		return GetHighLevelStats(levelScores, levelName, stat);
	}
	
	public int GetLevelStars(string levelName, int stat)
	{
		return GetHighLevelStats(levelStars, levelName, stat);
	}

	private int GetHighLevelStats(Dictionary<string, int> dictionary, string levelName, int stat)
	{
		if (dictionary.ContainsKey(levelName) == false)
		{
			return 0;
		}
		else 
		{
			return dictionary[levelName];
		}
	}

	private bool CheckLevelStats(Dictionary<string, int> dictionary, string levelName, int stat)
	{
		// 1. Check if the key exists. If it doesn't this means that this is the first time and is bound to be the high schore
		if (dictionary.ContainsKey(levelName) == false)
		{

			dictionary.Add(levelName, stat);

			return true;
		}

		// 2. If the entry already exists, get the previous entry
		int previousValue = dictionary[levelName];

		Debug.Log("Last Score " + previousValue + " | Current Score: " + stat);

		// 3. Check if the previous entry is less the the current entry. If it is, it means we have a new high score
		if (previousValue < stat)
		{
			dictionary.Remove(levelName);
			dictionary.Add(levelName, stat);

			return true;
		}

		return false;
	}

	#endregion highscore logic


}
