﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Lane : MonoBehaviour
{

    public Vector2 lanePosition;
    public int laneIndex;
    public List<GameObject> tokensList;

    private List<GameObject> startingTokensList;


    void Start()
    {
        initWithTokenList();
    }

    public void clearAll()
    {
        startingTokensList = new List<GameObject>();
        ResetLane();
    }

    public void initWithTokenList()
    {
        startingTokensList = new List<GameObject>();
        foreach (GameObject token_ in tokensList)
        {
            startingTokensList.Add(token_);
        }
    }

    public void ResetLane()
    {
        if (startingTokensList != null)
        {

            tokensList.Clear();

            foreach (GameObject token_ in startingTokensList)
            {
                tokensList.Add(token_);
            }
        }

    }
}
