﻿using UnityEngine;
using System.Collections;

public class BaseMenu : MonoBehaviour {

	public GameManager.GameState state;
	public bool isPopup = false;
	#region Button Handlers
	
    public virtual void Awake()
    {
		float scaleFactor = (NGUITools.screenSize.x + NGUITools.screenSize.y) / (1136.0f + 640.0f); 
		// Mathf.Min (Screen.width / 640.0f, Screen.height / 1136.0f); 
        this.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
    }

	public virtual void BackBtnPressed() 
	{
		MenuManager.Instance.PopMenu ();
	}
	
	#endregion Button Handlers


	#region BaseMenu: Life Cycle

	/* 
	 * <summary>
	 * This method is called right before your view appears, good for hiding/showing fields or any operations 
	 * that you want to happen every time before the view is visible. Because you might be going back and 
	 * forth between views, this will be called every time your view is about to appear on the screen.
	 * </summary>
	 */
	public void MenuWillAppear()
	{

	}
	
	/* 
	 * <summary>
	 * This method is called after the view appears - great place to start an animations or the loading of 
	 * external data from an API.
	 * </summary>
	 */
	public void MenuDidAppear()
	{
		MoPubAdsManager.Instance.hideBanner();
		if (MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.GamePlay)
		{
			MoPubAdsManager.Instance.showBanner();
		}
		//Debug.Log("MenuDidAppear " + MenuManager.Instance.NavigationStackPeek());
	}

	/* 
	 * <summary>
	 * This method is called right before the view disappears - its a good place to stop all animations, API calls
	 * etc
	 * </summary>
	 */
	public void MenuWillDisappear()
	{

	}

	/* 
	 * <summary>
	 * This method is called after the view disappears
	 * </summary>
	 */
	public void MenuDidDisappear()
	{

	}

	public void HideThis()
	{
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);		
		MenuManager.Instance.PopMenu ();
	}

	#endregion BaseMenu: Life Cycle
	
}
