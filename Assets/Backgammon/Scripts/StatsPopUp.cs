﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatsPopUp : BaseMenu
{

    public GameObject exitPopup;
    private bool isProcessing = false;
    public UIGrid grid;
    public void ExitPopUpYesBtnCallback()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        Application.Quit();
    }

    public void ExitPopUpNoBtnCallback()
    {
        if (isProcessing == false)
        {
            isProcessing = true;
            GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
            exitPopup.GetComponent<TweenScale>().PlayReverse();
            Invoke("HidePopUp", 1f);
        }

    }

    void HidePopUp()
    {
        isProcessing = false;
        MenuManager.Instance.PopMenu();
    }

    void OnEnable()
    {
        if (isProcessing == false)
        {
            
            isProcessing = true;
            foreach (Transform chld in grid.GetChildList())
            {
                if (int.Parse(chld.name) != 0)
                {
                    NGUITools.Destroy(chld.gameObject);
                }
            }
            if (PlayFabGameBridge.leaderboardLoaded)
            {

                foreach (KeyValuePair<string, uint> n in PlayFabGameBridge.LeaderboardHighScores)
                {
                    GameObject g = NGUITools.AddChild(grid.gameObject, grid.GetChild(0).gameObject);
                    g.name = grid.transform.childCount.ToString();
                    g.transform.GetChild(0).GetComponent<UILabel>().text = n.Key;
                    g.transform.GetChild(1).GetComponent<UILabel>().text = n.Value.ToString();
                }
                grid.repositionNow = true;
                grid.Reposition();
            }
            
            exitPopup.transform.localScale = new Vector3(0, 0, 0);
            exitPopup.GetComponent<TweenScale>().PlayForward();
            Invoke("CompletedProcessing", 1f);
        }
    }

    void CompletedProcessing()
    {
        isProcessing = false;

      
    }
}
