﻿using UnityEngine;
using System.Collections;

public class Token : MonoBehaviour
{

    public bool isOpponent;
    public int laneNumber;
    // Use this for initialization
    public bool isMatured;
    public bool isDead;
    public int tokenID;
    public Texture tokenMatureTexture;
    public Texture glowedTexture;

    private int startTokenIdVal;
    private Texture tokenSimpleTexture;
    private int startlaneNumberVal;
    private Vector3 startPosition;

    private Vector2 tokenSize;

    public void setNewSKin(Texture normal, Texture glow)
    {
        if (this.GetComponent<UITexture>().mainTexture == glowedTexture)
        {
            this.GetComponent<UITexture>().mainTexture = glow;
        }
        else
        {
            this.GetComponent<UITexture>().mainTexture = normal;
        }
        tokenMatureTexture = normal;
        tokenSimpleTexture = normal;
        glowedTexture = glow;

    }
    public Vector2 getOrgTokenSize()
    {
        return tokenSize;
    }


    void Start()
    {
        isMatured = false;
        isDead = false;

        startTokenIdVal = tokenID;
        tokenSimpleTexture = this.GetComponent<UITexture>().mainTexture;
        startlaneNumberVal = laneNumber;
        startPosition = this.transform.localPosition;

        tokenSize = this.GetComponent<UIWidget>().localSize;

        if (tokenSize.x == 0 || tokenSize.y == 0)
        {
            tokenSize.x = 51;
            tokenSize.y = 51;
            this.GetComponent<UITexture>().SetDimensions((int)tokenSize.x, (int)tokenSize.y);
        }

    }

    public void resetStartPosition()
    {
        startPosition = this.transform.localPosition;
    }

    public void setTokenIDAndLandNumber(int mTokenID, int mLanNumber)
    {
        tokenID = mTokenID;
        laneNumber = mLanNumber;

        startTokenIdVal = tokenID;
        startlaneNumberVal = laneNumber;
    }

    public void ResetToken()
    {
        if (tokenSimpleTexture != null)
        {
            isMatured = false;
            isDead = false;
            tokenID = startTokenIdVal;
            this.GetComponent<UITexture>().mainTexture = tokenSimpleTexture;
            this.GetComponent<UITexture>().MakePixelPerfect();
            laneNumber = startlaneNumberVal;
            this.transform.localPosition = startPosition;

            this.GetComponent<UITexture>().SetDimensions((int)tokenSize.x, (int)tokenSize.y);
        }

    }

    public void ResetTexture()
    {
        if (tokenSimpleTexture != null)
        {
            this.GetComponent<UITexture>().mainTexture = tokenSimpleTexture;
            //this.GetComponent<UITexture>().MakePixelPerfect();
        }
    }



}
