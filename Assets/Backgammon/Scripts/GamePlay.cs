using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using VoxelBusters.RuntimeSerialization;
//using ChartboostSDK;

public class GamePlay : BaseMenu
{
    private const string kSave2Prefs1PlayerMode = "save-game-data-1-player-mode";
    private const string kSave2Prefs2LocalPlayerMode = "save-game-data-2-local-player-mode";

    private enum PlayerTurn { None, Player1, Player2, GameOver };
    private enum PlayerGameMode { PlayerVSCPU, TwoLocalPlayer };

    public GameObject diceOneLb;
    public GameObject diceTwoLb;

    //	public GameObject[] lanes;
    public GameObject selectedToken;
    public GameObject selectedLane;

    public float timeTakenDuringLerp = 1f;
    public float distanceToMove = 10;
    private bool _isLerping;
    private bool _isUndoing;
    private float _timeStartedLerping;

    public List<GameObject> lanesList;

    public bool isDiceRolled;
    int rolledDiceTurnsLeft;

    public GameObject dice1;
    public GameObject dice2;
    public GameObject dice3;
    public GameObject dice4;

    public GameObject diceRollHand;

    public Texture blinkingTexture;
    public Texture blueSpikeTexture;
    public Texture redSpikeTexture;
    public Texture maturedStackTextureGlow;
    public Texture maturedStackTexturePlain;
    public Texture glowedToken;

    public GameObject Player1MaturityStack;
    public GameObject Player2MaturityStack;

    public UILabel turnLabel;
    public GameObject iosStore;

    int dice1Value = 0;
    int dice2Value = 0;
    PlayerTurn playerTurn;

    public GameObject player1deadStack;
    public GameObject player2deadStack;

    public GameObject noPossibleMoves;
    public GameObject rollDiceButton;

    public GameObject hintHandTexture;

    bool isGameTwoPlayer;

    public bool showPossibleMoves = true;

    public bool isTutorialOn = true;


    public List<GameObject> tokens;

    public PhotonView photonView;

    public string playerTokenColor;

    public bool isPlayer = false;

    float dice1floattime = 0.0f;
    float dice2floattime = 0.0f;

    private bool isRemoteClientTurn = false;

    public List<GameObject> stackList;

    public UILabel player1Score;
    public UILabel player2Score;

    public UILabel player1PipLable;
    public UILabel player2PipLable;


    public GameObject boardGame;
    public GameObject boardGameOverlay;
    public GameObject iPadLandscapeLayout;
    public GameObject iPhonePortraitLayout;
    public GameObject iPhoneLandscapeLayout;
    public GeneralMessageBox generalMessageBox;

    private Stack moveOperationStack = new Stack();
    private Stack<UndoData> undoStack = new Stack<UndoData>();

    private Token samplePlayer1Token;
    private Token samplePlayer2Token;

    public Texture whiteTokenTexture;
    public Texture blackTokenTexture;
    public Texture whiteGlowTokenTexture;
    public Texture blackGlowTokenTexture;

    private int matchCountToWin;
    private int currentMatchCount;

    private int playerScore = 0;
    private int cpuScore = 0;

    private TSUtil.DifficulltyLevel difficulltyLevel;

    private UndoData tempUndoData = new UndoData();

    private PlayerGameMode playerGameMode;

    public UILabel player1NameLabel;
    public UILabel player2NameLabel;

    [Serializable, RuntimeSerializable]
    private class SaveGameData
    {
        public PlayerTurn playerTurn;
        public int[] tokenMap;
        public Stack<UndoData> undoStack;
        public int matchCountToWin;
        public int currentMatchCount;
        public int playerScore;
        public int cpuScore;
        public PlayerGameMode playerGameMode;
        public TSUtil.DifficulltyLevel difficulltyLevel;

    }

    [Serializable, RuntimeSerializable]
    private class UndoData
    {
        public PlayerTurn playerTurn;
        public int[] tokenMap;
        public int rolledDiceTurnsLeft;
        public int dice1Value;
        public int dice2Value;
        public Vector3[] tokenPos;
        public int fromeLaneNumber;
        public int toLaneNumber;
        public bool isKillTokenMove = false;
        public bool isBeginRoll = false;
    }

    public override void Awake()
    {
        base.Awake();

        if (TSUtil.isIPad())
        {
            this.transform.localScale = new Vector3(NGUITools.screenSize.x / 1024.0f, NGUITools.screenSize.y / 768.0f, 2);
            Debug.Log("iPad : " + NGUITools.screenSize.x);
        }
        // else if (TSUtil.isIPhone())
        // {
        //     if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Landscape) //Landscape
        //     {
        //         this.boardGame.transform.localScale = new Vector3(1.14f, 0.8f, 1);
        //     }
        //     else
        //     {

        //     }
        // }

        //Screen.SetResolution(1024, 768, true);
        // Screen.orientation = ScreenOrientation.Landscape;
        initGame();
    }

    void Start()
    {
        // photonView = GetComponent<PhotonView>();
        // if (isGameTwoPlayer == false)
        // {
        //     checkTutorial();
        //     getIniitialScore();
        // }
        // initGame();
        // TSUtil.Instance.StartCoroutine(reloadLayoutWithDelay(0.1f));
    }

    void OnEnable()
    {
        //thesun
        TSUtil.Instance.StartCoroutine(reloadLayoutWithDelay(0.01f));
        MoPubAdsManager.Instance.repositionBanner();
        if (playerGameMode == PlayerGameMode.TwoLocalPlayer)
        {
            player1NameLabel.text = "Player 1";
            player2NameLabel.text = "Player 2";
        }
        else
        {
            player1NameLabel.text = "Player";
            player2NameLabel.text = (TSUtil.isIPhone() ? "iPhone" : "iPad");
        }
    }

    public IEnumerator reloadLayoutWithDelay(float second)
    {
        this.transform.localEulerAngles = new Vector3(0, 0, 0);
        if (TSUtil.isIPhone())
        {
            if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Landscape)
            {
                this.transform.localEulerAngles = new Vector3(0, 0, -90);
            }
        }


        // string folderPath = "Assets/Backgammon/Graphics/GamePlay/";


        // Texture2D normalWhiteToken = (Texture2D)UnityEditor.AssetDatabase.LoadAssetAtPath(folderPath + "ChWhite1.png", typeof(Texture2D));
        // Texture2D glowWhiteToken = (Texture2D)UnityEditor.AssetDatabase.LoadAssetAtPath(folderPath + "ChWhite1Highlight.png", typeof(Texture2D));

        // Texture2D normalBlackToken = (Texture2D)UnityEditor.AssetDatabase.LoadAssetAtPath(folderPath + "ChBlack1.png", typeof(Texture2D));
        // Texture2D glowBlackToken = (Texture2D)UnityEditor.AssetDatabase.LoadAssetAtPath(folderPath + "ChBlack1HighLight.png", typeof(Texture2D));

        // Texture normalWhiteToken = Resources.Load("Backgammon/Graphics/GamePlay/ChWhite1") as Texture;
        // Texture glowWhiteToken = Resources.Load("ChWhite1Highlight") as Texture;

        // Texture normalBlackToken = Resources.Load("ChBlack1") as Texture;
        // Texture glowBlackToken = Resources.Load("ChBlack1HighLight") as Texture;

        foreach (GameObject item in tokens)
        {
            Token itemToken = item.GetComponent<Token>();
            if (TSUtil.Instance.SettingColorCheckers) //player1 is white
            {
                if (itemToken.isOpponent)
                {
                    itemToken.setNewSKin(blackTokenTexture, blackGlowTokenTexture);
                }
                else
                {
                    itemToken.setNewSKin(whiteTokenTexture, whiteGlowTokenTexture);
                }
            }
            else
            {
                if (itemToken.isOpponent)
                {
                    itemToken.setNewSKin(whiteTokenTexture, whiteGlowTokenTexture);
                }
                else
                {
                    itemToken.setNewSKin(blackTokenTexture, blackGlowTokenTexture);
                }
            }
        }
        if (TSUtil.Instance.SettingOneTouchMove == true)
        {
            foreach (GameObject item in tokens)
            {
                item.GetComponent<Token>().ResetTexture();
            }
            selectedToken = null;
            removeGlowEffects();
        }

        updatePlayerTurnLabel(true);
        updatePip();


        yield return new WaitForSeconds(second);
        reloadLayout();
        yield return new WaitForSeconds(second);
        reArrangeTokensOnAllLanes();
        yield return new WaitForSeconds(second);
        reArrangeTokensOnAllStack();

    }
    public void reloadLayout()
    {
        this.moveGameObjectToFront(iPadLandscapeLayout);
        this.moveGameObjectToFront(iPhonePortraitLayout);
        this.moveGameObjectToFront(iPhoneLandscapeLayout);

        GameObject deviceLayout = iPadLandscapeLayout;
        if (TSUtil.isIPad())
        {
            iPadLandscapeLayout.GetComponent<UIWidget>().alpha = 1;
            iPhonePortraitLayout.GetComponent<UIWidget>().alpha = 0;
            iPhoneLandscapeLayout.GetComponent<UIWidget>().alpha = 0;
            deviceLayout = iPadLandscapeLayout;
        }
        else if (TSUtil.isIPhone())
        {
            iPadLandscapeLayout.GetComponent<UIWidget>().alpha = 0;

            if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Portrait)
            {
                iPhonePortraitLayout.GetComponent<UIWidget>().alpha = 1;
                iPhoneLandscapeLayout.GetComponent<UIWidget>().alpha = 0;
                deviceLayout = iPhonePortraitLayout;
            }
            else
            {
                iPhonePortraitLayout.GetComponent<UIWidget>().alpha = 0;
                iPhoneLandscapeLayout.GetComponent<UIWidget>().alpha = 1;
                deviceLayout = iPhoneLandscapeLayout;
            }
        }


        GameObject boardPlaceHolder = deviceLayout.transform.Find("BoardPlaceHolder").gameObject;
        boardGame.transform.localPosition = boardPlaceHolder.transform.localPosition;

        Vector2 boardPlaceHolderSize = boardPlaceHolder.GetComponent<UIWidget>().localSize;
        Vector2 boardSize = boardGame.GetComponent<UIWidget>().localSize;
        boardGame.transform.localScale = new Vector3(boardPlaceHolderSize.x / boardSize.x, boardPlaceHolderSize.y / boardSize.y, 1);

        GameObject stack1PlaceHolder = deviceLayout.transform.Find("Player1InfoWidget/StackPlackHolder").gameObject;
        TSUtil.movPosSrc2Des(Player1MaturityStack, stack1PlaceHolder);
        // TweenTransform.Begin(Player1MaturityStack, 0, stack1PlaceHolder.transform);

        GameObject stack2PlaceHolder = deviceLayout.transform.Find("Player2InfoWidget/StackPlackHolder").gameObject;
        TSUtil.movPosSrc2Des(Player2MaturityStack, stack2PlaceHolder);
        // TweenTransform.Begin(Player2MaturityStack, 0, stack2PlaceHolder.transform);
    }

    private int[] getDefaultTokenMap()
    {
        int[] defaultTokenInLan = {0, 6, 6, 6, 6, 6, 8, 8, 8, 13, 13, 13, 13, 13, 24, 24, //player 2
                                    1, 1, 12, 12, 12, 12, 12, 17, 17, 17, 19, 19, 19, 19, 19}; //player1

        // int[] defaultTokenInLan = {0, 25, 6, 6, 6, 6, 8, 8, 8, 13, 13, 13, 13, 13, 24, 24, //player 2
        //                             0, 1, 12, 12, 12, 12, 12, 17, 17, 17, 19, 19, 19, 19, 19}; //player1


        return defaultTokenInLan;
    }
    public void initGame(int[] tokenMap = null)
    {

        // int[] defaultTokenInLan = {0, 1, 1, 12, 12, 12, 12, 12, 17, 17, 17, 19, 19, 19, 19, 19, //player 2
        //                             6, 6, 6, 6, 6, 8, 8, 8, 13, 13, 13, 13, 13, 24, 24}; //player1

        //default   
        int[] defaultTokenInLan = getDefaultTokenMap();

        // int[] defaultTokenInLan = {0, 1, 1, 12, 19, 19, 19, 19, 17, 17, 17, 19, 19, 19, 19, 19, //player 2
        //                             6, 6, 6, 6, 6, 8, 8, 8, 13, 13, 13, 13, 13, 24, 24}; //player1


        // int[] defaultTokenInLan = {0, 25, 6, 6, 6, 6, 8, 8, 8, 8, 8, 13, 13, 13, 13, 13, //player 2
        //                             0, 1, 12, 12, 12, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24}; //player1

        // int[] defaultTokenInLan = {0, 6, 6, 6, 21, 21, 21, 2, 2, 2, 2, 3, 3, 3, 24, 24,
        //                             19, 19, 19, 22, 22, 22, 22, 23, 23, 23, 22, 22, 19, 19, 19};

        // int[] defaultTokenInLan = {0, 25, 25, 1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 4,
        //                             0, 0, 0, 21, 21, 21, 22, 22, 22, 23, 23, 24, 24, 24, 24};

        // int[] defaultTokenInLan = {0, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 1, 1, 1, //player 2
        //                             26, 26, 26, 26, 26, 26, 26, 26, 22, 22, 24, 24, 24, 24, 24}; //player1

        // int[] defaultTokenInLan = {0, 6, 6, 6, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 1, 1, //player 2
        //                             24, 24, 19, 19, 19, 21, 21, 21, 21, 21, 20, 20, 19, 19, 19}; //player1


        if (tokenMap != null)
            defaultTokenInLan = tokenMap;

        foreach (GameObject g in lanesList)
        {
            g.GetComponent<Lane>().clearAll();
        }
        Player1MaturityStack.GetComponent<Lane>().clearAll();
        Player2MaturityStack.GetComponent<Lane>().clearAll();
        player1deadStack.GetComponent<Lane>().clearAll();
        player2deadStack.GetComponent<Lane>().clearAll();


        tokens = new List<GameObject>();

        for (int i = 1; i <= 30; i++)
        {
            Token token = null;
            if (i < 16)
            {
                token = this.boardGameOverlay.transform.Find("opponentTokens/Token" + i).GetComponent<Token>();
                samplePlayer2Token = token;
            }
            else
            {
                token = this.boardGameOverlay.transform.Find("userTokens/Token" + i).GetComponent<Token>();
                samplePlayer1Token = token;
            }
            token.setTokenIDAndLandNumber(i, defaultTokenInLan[i]);
            tokens.Add(token.gameObject);

            GameObject laneGameObject = GetLanebyId(token.laneNumber);
            if (defaultTokenInLan[i] == 26)
            {
                laneGameObject = Player1MaturityStack;
                token.isMatured = true;
            }
            else if (defaultTokenInLan[i] == 27)
            {
                laneGameObject = Player2MaturityStack;
                token.isMatured = true;
            }
            else if (defaultTokenInLan[i] == 0 || (defaultTokenInLan[i] == 25))
            {
                token.isDead = true;
            }
            laneGameObject.GetComponent<Lane>().tokensList.Add(token.gameObject);
        }

        foreach (GameObject g in lanesList)
        {
            g.GetComponent<Lane>().initWithTokenList();
        }
        Player1MaturityStack.GetComponent<Lane>().initWithTokenList();
        Player2MaturityStack.GetComponent<Lane>().initWithTokenList();

        ResetGame();

        for (int i = 1; i <= 30; i++)
        {
            Token token = null;
            string goPath = (i < 16 ? "opponentTokens/Token" : "userTokens/Token");
            token = this.boardGameOverlay.transform.Find(goPath + i).GetComponent<Token>();
            if (defaultTokenInLan[i] >= 26)
                token.isMatured = true;
            else if (defaultTokenInLan[i] == 0 || (defaultTokenInLan[i] == 25))
                token.isDead = true;
        }
        updateScore();
    }

    private SaveGameData makeSaveGameData()
    {
        SaveGameData saveGameData = new SaveGameData();

        saveGameData.playerTurn = playerTurn;

        saveGameData.tokenMap = getCurrentTokenMap();

        saveGameData.undoStack = undoStack;

        saveGameData.matchCountToWin = matchCountToWin;
        saveGameData.currentMatchCount = currentMatchCount;
        saveGameData.playerScore = playerScore;
        saveGameData.cpuScore = cpuScore;
        saveGameData.difficulltyLevel = difficulltyLevel;
        saveGameData.playerGameMode = playerGameMode;
        // foreach (GameObject currLane in lanesList)
        // {
        //     foreach (GameObject currToken in currLane.GetComponent<Lane>().tokensList)
        //     {
        //         Token token = currToken.GetComponent<Token>();
        //         if (token.isOpponent)
        //         {

        //         }
        //         else
        //         {

        //         }
        //     }
        // }

        return saveGameData;
    }
    public void saveGame()
    {
        if (playerGameMode == PlayerGameMode.PlayerVSCPU)
            TSUtil.Instance.hasSaveGamePlayerVSCPU = true;
        else if (playerGameMode == PlayerGameMode.TwoLocalPlayer)
            TSUtil.Instance.hasSaveGame2LocalPlayer = true;

        SaveGameData saveGameData = makeSaveGameData();
        string saveGameFileName = playerGameMode == PlayerGameMode.PlayerVSCPU ? kSave2Prefs1PlayerMode : kSave2Prefs2LocalPlayerMode;
        RSManager.Serialize<SaveGameData>(saveGameData, saveGameFileName, eSaveTarget.PLAYER_PREFS);
    }

    public void loadGame1Player()
    {
        loadGame(kSave2Prefs1PlayerMode);
    }

    public void loadGame2LocalPlayer()
    {
        loadGame(kSave2Prefs2LocalPlayerMode);
    }

    public void loadGame(string saveGameFileName)
    {
        SaveGameData saveGameData = new SaveGameData();
        saveGameData = RSManager.Deserialize<SaveGameData>(saveGameFileName);

        initGame(saveGameData.tokenMap);
        // initGame();

        playerTurn = saveGameData.playerTurn;
        updatePlayerTurnLabel(true);
        undoStack = saveGameData.undoStack;
        matchCountToWin = saveGameData.matchCountToWin;
        currentMatchCount = saveGameData.currentMatchCount;
        playerScore = saveGameData.playerScore;
        cpuScore = saveGameData.cpuScore;
        difficulltyLevel = saveGameData.difficulltyLevel;
        playerGameMode = saveGameData.playerGameMode;

        updateScore();
    }

    public void loadNewMatch1Player()
    {
        loadNewMatch();
        playerGameMode = PlayerGameMode.PlayerVSCPU;
    }

    public void loadNewMatch2LocalPlayer()
    {
        loadNewMatch();
        playerGameMode = PlayerGameMode.TwoLocalPlayer;
    }
    public void loadNewMatch()
    {
        initGame();

        matchCountToWin = TSUtil.Instance.SettingMatchToWin;
        playerScore = 0;
        cpuScore = 0;
        updateScore();
        difficulltyLevel = (TSUtil.DifficulltyLevel)TSUtil.Instance.SettingDifficulty;
    }

    public void loadNewGame()
    {
        initGame();
    }

    public void getIniitialScore()
    {

        // int pScore = PlayerPrefs.GetInt("PlayerScore");
        // player1Score.text = pScore + " K";
        // int cpuScore = PlayerPrefs.GetInt("CPUScore");
        // player2Score.text = cpuScore + " K";
    }

    public void updateScore()
    {
        player1Score.text = playerScore + "";
        player2Score.text = cpuScore + "";
    }

    public void updatePip()
    {
        if (TSUtil.Instance.SettingShowPipCount == false)
        {
            player1PipLable.text = "";
            player2PipLable.text = "";
            return;
        }

        int player1Pip = 0;
        int player2Pip = 0;

        foreach (GameObject goLane in lanesList)
        {
            int laneIndex = goLane.GetComponent<Lane>().laneIndex;
            foreach (GameObject goToken in goLane.GetComponent<Lane>().tokensList)
            {
                Token token = goToken.GetComponent<Token>();
                if (laneIndex == 0)
                {
                    player1Pip += 24;
                }
                else if (laneIndex == 25)
                {
                    player2Pip += 24;
                }
                else
                {
                    if (token.isOpponent)
                    {
                        player2Pip += laneIndex;
                    }
                    else
                    {
                        player1Pip += 25 - laneIndex;
                    }
                }

            }
        }

        player1PipLable.text = player1Pip + "";
        player2PipLable.text = player2Pip + "";
    }


    public int calculateScore()
    {
        int points = 1;
        int factor = 0;

        if (playerTurn == PlayerTurn.Player1)
        {
            if (player2deadStack.GetComponent<Lane>().tokensList.Count > 0 ||
                isExistTokensInOppositeHomeBoard())
            {
                factor = 3;
            }
            else if (Player2MaturityStack.GetComponent<Lane>().tokensList.Count > 1)
            {
                factor = 1;
            }
            //the losing player has not borne off any of their checkers
            else
            {
                factor = 2;
            }
        }
        else if (playerTurn == PlayerTurn.Player2)
        {

            if (player1deadStack.GetComponent<Lane>().tokensList.Count > 0 ||
                isExistTokensInOppositeHomeBoard())
            {
                factor = 3;
            }
            else if (Player1MaturityStack.GetComponent<Lane>().tokensList.Count > 1)
            {
                factor = 1;
            }
            //the losing player has not borne off any of their checkers
            else
            {
                factor = 2;
            }

        }
        return points * factor;
    }

    public bool isExistTokensInOppositeHomeBoard()
    {
        if (playerTurn == PlayerTurn.Player1)
        {
            foreach (GameObject tk in tokens)
            {
                if (tk.GetComponent<Token>().laneNumber > 18 &&
                    tk.GetComponent<Token>().isMatured == false &&
                    tk.GetComponent<Token>().isDead == false &&
                    tk.GetComponent<Token>().isOpponent == true)
                {
                    return true;
                }
            }

        }
        else if (playerTurn == PlayerTurn.Player2)
        {
            foreach (GameObject tk in tokens)
            {
                if (tk.GetComponent<Token>().laneNumber < 7 &&
                tk.GetComponent<Token>().isMatured == false &&
                tk.GetComponent<Token>().isDead == false &&
                tk.GetComponent<Token>().isOpponent == false)
                {
                    return true;
                }

            }

        }
        return false;
    }

    public void ResetGame()
    {

        _isLerping = false;
        _isUndoing = false;
        undoStack.Clear();

        isGameTwoPlayer = GameManager.Instance.isGameMultiplayer;
        selectedToken = null;
        playerTurn = PlayerTurn.None;
        isDiceRolled = false;

        rolledDiceTurnsLeft = 0;

        foreach (GameObject token_ in tokens)
        {
            token_.GetComponent<Token>().ResetToken();
        }

        foreach (GameObject lane_ in lanesList)
        {
            lane_.GetComponent<Lane>().ResetLane();
        }

        dice1Value = 0;
        dice2Value = 0;

        dice1floattime = 0;
        dice2floattime = 0;

        Player1MaturityStack.GetComponent<Lane>().ResetLane();
        Player2MaturityStack.GetComponent<Lane>().ResetLane();
        player1deadStack.GetComponent<Lane>().ResetLane();
        player2deadStack.GetComponent<Lane>().ResetLane();

        hideNoPossibleMovesLabel();
        hideAllDices();
        removeGlowEffects();
        diceRollHand.gameObject.SetActive(false);


        dice1.gameObject.GetComponent<UI2DSpriteAnimation>().Pause();
        dice1.gameObject.SetActive(false);

        dice2.gameObject.GetComponent<UI2DSpriteAnimation>().Pause();
        dice2.gameObject.SetActive(false);
        dice3.gameObject.SetActive(false);
        dice4.gameObject.SetActive(false);
        rollDiceButton.SetActive(true);
        StopAllCoroutines();
        TSUtil.Instance.StartCoroutine(reloadLayoutWithDelay(0.01f));

        foreach (GameObject item in tokens)
        {
            item.GetComponent<BoxCollider>().enabled = true;
        }
        foreach (GameObject item in lanesList)
        {
            item.GetComponent<BoxCollider>().enabled = true;
        }


        updatePlayerTurnLabel(true);
        updatePip();
    }

    public GameObject GetTokenbyId(int id)
    {
        GameObject selectedObj = null;
        foreach (GameObject g in tokens)
        {

            if (g.GetComponent<Token>().tokenID == id)
            {
                selectedObj = g;
            }

        }
        return selectedObj;
    }

    public GameObject GetLanebyId(int id)
    {
        GameObject selectedObj = null;
        foreach (GameObject g in lanesList)
        {

            if (g.GetComponent<Lane>().laneIndex == id)
            {
                selectedObj = g;
            }

        }
        return selectedObj;
    }


    [PunRPC]
    public void SelectPlayerTokens(bool status)
    {

        isPlayer = status;
        if (isPlayer)
        {
            playerTokenColor = "Yellow";
            foreach (GameObject g in tokens)
            {
                if (g.GetComponent<Token>().isOpponent == true)
                {

                    g.GetComponent<PhotonView>().RequestOwnership();
                }
            }
        }
        else
        {
            playerTokenColor = "Brown";
            foreach (GameObject g in tokens)
            {
                if (g.GetComponent<Token>().isOpponent == false)
                {

                    g.GetComponent<PhotonView>().RequestOwnership();
                }
            }
        }

    }
    void OnGUI()
    {
        //  GUI.Label(Rect(Screen.width - 200, Screen.height - 35, 200, 80), "something: ");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 35, 200, 80), playerTokenColor.ToString());
    }

    public void onDiceClicked(GameObject mDice)
    {
        if (!isDiceRolled)
            return;
        if (_isLerping)
            return;
        if (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.PlayerVSCPU)
            return;
        if (TSUtil.Instance.SettingOneTouchMove == false)
            return;
        if (rolledDiceTurnsLeft != 2)
            return;

        if (dice2.GetComponent<TweenPosition>())
            return;

        int temp = dice1Value;
        dice1Value = dice2Value;
        dice2Value = temp;

        GameObject tempDice = dice1;
        dice1 = dice2;
        dice2 = tempDice;

        Vector3 posDice1 = dice1.transform.localPosition;
        Vector3 posDice2 = dice2.transform.localPosition;

        TweenPosition tw1 = TweenPosition.Begin(dice1, 0.35f, posDice2);
        tw1.AddOnFinished(() =>
        {
            Destroy(tw1);
        });

        TweenPosition tw2 = TweenPosition.Begin(dice2, 0.35f, posDice1);
        tw2.AddOnFinished(() =>
        {
            Destroy(tw2);
        });


    }
    public void onTokenClicked(GameObject token)
    {
        if (_isUndoing)
            return;
        if (!isDiceRolled)
            return;
        if (_isLerping)
            return;
        if (rollDiceButton.GetActive())
            return;
        if (dice1Value == 0 && dice2Value == 0)
            return;


        if (playerTurn == PlayerTurn.Player1)
        {
            if (player1deadStack.GetComponent<Lane>().tokensList.Count > 0)
            {
                if (player1deadStack.GetComponent<Lane>().tokensList.Contains(token) == false)
                    return;
            }
        }
        else if (playerTurn == PlayerTurn.Player2)
        {
            if (player2deadStack.GetComponent<Lane>().tokensList.Count > 0)
            {
                if (player2deadStack.GetComponent<Lane>().tokensList.Contains(token) == false)
                    return;
            }
        }



        if (selectedToken != null)
        {
            if (playerTurn == PlayerTurn.Player1)
            {
                if (player1deadStack.GetComponent<Lane>().tokensList.Contains(selectedToken))
                {
                    if (token != selectedToken)
                    {
                        GameObject tempLane = getLanewithIndexNo(token.GetComponent<Token>().laneNumber);
                        if (tempLane != player1deadStack)
                            onLaneClicked(tempLane);
                    }
                    return;
                }
            }
            else if (playerTurn == PlayerTurn.Player2)
            {
                if (player2deadStack.GetComponent<Lane>().tokensList.Contains(selectedToken))
                {
                    if (token != selectedToken)
                    {
                        GameObject tempLane = getLanewithIndexNo(token.GetComponent<Token>().laneNumber);
                        if (tempLane != player2deadStack)
                            onLaneClicked(tempLane);
                    }
                    return;
                }
            }
        }

        //unslect token
        if (token == selectedToken ||
            (selectedToken && token.GetComponent<Token>().laneNumber == selectedToken.GetComponent<Token>().laneNumber))
        {
            if (playerTurn == PlayerTurn.Player1)
            {
                if (player1deadStack.GetComponent<Lane>().tokensList.Count > 0)
                {
                    return;
                }
            }
            else if (playerTurn == PlayerTurn.Player2)
            {
                if (player2deadStack.GetComponent<Lane>().tokensList.Count > 0)
                {
                    return;
                }
            }

            removeGlowEffects();
            selectedToken.GetComponent<Token>().ResetTexture();
            selectedToken = null;
            return;
        }


        if (isGameTwoPlayer)
        {
            if (isRemoteClientTurn)
            {
                return;
            }
            photonView.RPC("onTokenClickedRPC", PhotonTargets.Others, token.GetComponent<Token>().tokenID);
        }

        foreach (GameObject tok in tokens)
        {
            if (!tok.GetComponent<Token>().isMatured)
                tok.GetComponent<Token>().ResetTexture();
        }

        removeGlowEffects();

        if (selectedToken != null && token.GetComponent<Token>().isMatured == false)
        {
            if (token.GetComponent<Token>().laneNumber > 0 && token.GetComponent<Token>().laneNumber < 25)
                onLaneClicked(getLanewithIndexNo(token.GetComponent<Token>().laneNumber));

            //thesun
            if (!_isLerping)
            {
                // if ((playerTurn == PlayerTurn.Player1 && token.GetComponent<Token>().isOpponent == true) || (playerTurn == PlayerTurn.Player2 && token.GetComponent<Token>().isOpponent == false))
                // {
                //     token = selectedToken;
                // }

                selectedToken = null;
            }
        }
        else if (selectedToken != null && token.GetComponent<Token>().isMatured == true)
        {

            if (token.GetComponent<Token>().isOpponent == false)
                onStackClicked(Player1MaturityStack);
            else
                onStackClicked(Player2MaturityStack);
        }

        if ((playerTurn == PlayerTurn.Player1 && token.GetComponent<Token>().isOpponent == false) ||
            (playerTurn == PlayerTurn.Player2 && token.GetComponent<Token>().isOpponent == true))
        {
            if (_isLerping == false && token.GetComponent<Token>().isMatured == false)
            {
                selectedToken = token;

                // Debug.Log("selected Token clicked");
                // add if token in killed stack ///

                GameObject currentLane = getLanewithIndexNo(selectedToken.GetComponent<Token>().laneNumber); //////////////////////////////  current Lane Number

                if (currentLane.GetComponent<Lane>().tokensList.Count > 1)
                {
                    selectedToken = null;
                    selectedToken = currentLane.GetComponent<Lane>().tokensList[currentLane.GetComponent<Lane>().tokensList.Count - 1];
                }
                selectedToken.GetComponent<UITexture>().mainTexture = selectedToken.GetComponent<Token>().glowedTexture;
                // selectedToken.GetComponent<UITexture>().MakePixelPerfect();

                //thesun
                moveGameObjectToFront(selectedToken);
            }
            glowPossibleLane();

            //thesun
            if (TSUtil.Instance.SettingOneTouchMove)
            {
                int direction = 0;
                if (playerTurn == PlayerTurn.Player1)
                {
                    if (Player1MaturityStack.GetComponent<UITexture>().mainTexture == maturedStackTextureGlow)
                    {
                        onStackClicked(Player1MaturityStack);
                        return;
                    }
                    direction = 1;
                }
                else if (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.TwoLocalPlayer)
                {
                    if (Player2MaturityStack.GetComponent<UITexture>().mainTexture == maturedStackTextureGlow)
                    {
                        onStackClicked(Player2MaturityStack);
                        return;
                    }
                    direction = -1;
                }

                if (direction != 0)
                {
                    int dice1Destination = selectedToken.GetComponent<Token>().laneNumber + dice1Value * direction;
                    if (dice1Value > 0 && isAPossibleLane(dice1Destination))
                    {
                        onLaneClicked(getLanewithIndexNo(dice1Destination));
                        return;
                    }

                    if (selectedToken.GetComponent<Token>().isDead == true)
                        direction *= -1;

                    for (int i = 1; i <= 24; i++)
                    {
                        int laneIndex = (direction == -1 ? i : 25 - i);
                        GameObject currentLane = getLanewithIndexNo(laneIndex);

                        if (currentLane.transform.Find("glow").gameObject.GetActive() == true)
                        {
                            onLaneClicked(currentLane);
                            return;
                        }
                    }
                }
            }
        }
    }

    [PunRPC]
    public void onTokenClickedRPC(int id)
    {
        // Debug.Log(id);

        GameObject token = GetTokenbyId(id);
        removeGlowEffects();

        if ((playerTurn == PlayerTurn.Player1 && token.GetComponent<Token>().isOpponent == false) ||
            (playerTurn == PlayerTurn.Player2 && token.GetComponent<Token>().isOpponent == true))
        {

            if (_isLerping == false && token.GetComponent<Token>().isMatured == false)
            {
                selectedToken = token;

                // Debug.Log("selected Token clicked");
                // add if token in killed stack ///

                GameObject currentLane = getLanewithIndexNo(selectedToken.GetComponent<Token>().laneNumber); //////////////////////////////  current Lane Number

                if (currentLane.GetComponent<Lane>().tokensList.Count > 1)
                {
                    selectedToken = null;
                    selectedToken = currentLane.GetComponent<Lane>().tokensList[currentLane.GetComponent<Lane>().tokensList.Count - 1];
                }
            }
            glowPossibleLane();
        }
    }



    public void onLaneClicked(GameObject lane)
    {
        //thesun
        if (_isUndoing)
            return;
        if (!isDiceRolled)
            return;
        if (_isLerping)
            return;
        if (rollDiceButton.GetActive())
            return;


        if (isGameTwoPlayer)
        {
            if (isRemoteClientTurn)
            {
                return;
            }
            photonView.RPC("onLaneClickedRPC", PhotonTargets.Others, lane.GetComponent<Lane>().laneIndex);
        }
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Pointer_Click);

        // Debug.Log("on Lane Clicked");

        int laneIndex = lane.GetComponent<Lane>().laneIndex;
        //		bool isDestiantionLaneOpp = isDestinationLaneOpponents (laneIndex);

        //thesun
        //optimize select unselect token
        if (playerTurn == PlayerTurn.Player1 ||
            (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.TwoLocalPlayer))
        {
            Lane laneComp = lane.GetComponent<Lane>();
            if (laneComp.tokensList.Count > 0)
            {
                Token tokenComp = laneComp.tokensList[0].GetComponent<Token>();
                if (selectedToken == null &&
                    (tokenComp.isOpponent == false || (tokenComp.isOpponent == true && playerTurn == PlayerTurn.Player2)))
                {
                    onTokenClicked(tokenComp.gameObject);
                    return;
                }
                if (selectedToken != null && selectedToken.GetComponent<Token>().laneNumber == laneIndex)
                {
                    onTokenClicked(selectedToken);
                    return;
                }

            }
        }

        if (selectedToken != null && isDiceRolled == true && isAPossibleLane(laneIndex))
        {

            selectedLane = lane;

            int selectedLaneIndex = selectedLane.GetComponent<Lane>().laneIndex;
            // Debug.Log("selected lane index" + selectedLaneIndex);

            //thesun
            updateTempUndoData();

            GameObject currentLane = getLanewithIndexNo(selectedToken.GetComponent<Token>().laneNumber); //////////////////////////////  current Lane Number
            selectedLaneIndex = isAPossibleMove(selectedToken.GetComponent<Token>().laneNumber, selectedLaneIndex, true);

            if (selectedLaneIndex != -1)
            {
                selectedLane = getLanewithIndexNo(selectedLaneIndex);

                // to check if single token layer of opponent
                if (((playerTurn == PlayerTurn.Player1 && isDestinationLaneOpponents(selectedLaneIndex) == true) ||
                    (playerTurn == PlayerTurn.Player2 && isDestinationLaneOpponents(selectedLaneIndex) == false))
                    && destinationLaneTokenCount(selectedLaneIndex) == 1)
                {
                    saveDataForUndo(selectedToken.GetComponent<Token>().laneNumber, selectedLane.GetComponent<Lane>().laneIndex, true);
                    killTokenAtLane(selectedLaneIndex);
                }

                //thesun
                else
                    saveDataForUndo(selectedToken.GetComponent<Token>().laneNumber, selectedLane.GetComponent<Lane>().laneIndex);

                // Debug.Log("move");
                selectedToken.GetComponent<Token>().laneNumber = selectedLane.GetComponent<Lane>().laneIndex;
                selectedLane.GetComponent<Lane>().tokensList.Add(selectedToken); //////////////////////////////// current Lane Number
                currentLane.GetComponent<Lane>().tokensList.Remove(selectedToken);
                StartLerping();

                if (_isLerping)
                {
                    GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Swap);
                }
            }
        }
    }

    [PunRPC]
    public void onLaneClickedRPC(int id)
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Pointer_Click);
        GameObject lane = GetLanebyId(id);


        // Debug.Log("on Lane Clicked");

        int laneIndex = lane.GetComponent<Lane>().laneIndex;
        //		bool isDestiantionLaneOpp = isDestinationLaneOpponents (laneIndex);

        if (selectedToken != null && isDiceRolled == true && isAPossibleLane(laneIndex))
        {

            selectedLane = lane;

            int selectedLaneIndex = selectedLane.GetComponent<Lane>().laneIndex;
            // Debug.Log("selected lane index" + selectedLaneIndex);

            GameObject currentLane = getLanewithIndexNo(selectedToken.GetComponent<Token>().laneNumber); //////////////////////////////  current Lane Number
            selectedLaneIndex = isAPossibleMove(selectedToken.GetComponent<Token>().laneNumber, selectedLaneIndex, true);
            if (selectedLaneIndex != -1)
            {
                selectedLane = getLanewithIndexNo(selectedLaneIndex);

                // to check if single token layer of opponent
                if (((playerTurn == PlayerTurn.Player1 && isDestinationLaneOpponents(selectedLaneIndex) == true) ||
                    (playerTurn == PlayerTurn.Player2 && isDestinationLaneOpponents(selectedLaneIndex) == false)) &&
                    destinationLaneTokenCount(selectedLaneIndex) == 1)
                {
                    killTokenAtLane(selectedLaneIndex);
                }

                // Debug.Log("move");
                selectedToken.GetComponent<Token>().laneNumber = selectedLane.GetComponent<Lane>().laneIndex;
                selectedLane.GetComponent<Lane>().tokensList.Add(selectedToken); //////////////////////////////// current Lane Number
                currentLane.GetComponent<Lane>().tokensList.Remove(selectedToken);
                StartLerping();

                if (_isLerping)
                {
                    GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Swap);
                }

            }

        }

    }

    ///////// to check if a token is a possible move

    //////////////////////////////////////////// kill Token Methods

    void killTokenAtLane(int laneIndex)
    {

        GameObject killTokenLane = getLanewithIndexNo(laneIndex);

        GameObject deadStack;


        //	killTokenLane.GetComponent<Lane> ().tokensList [0].GetComponent<Token> ().laneNumber = 0;

        GameObject toKillToken = killTokenLane.GetComponent<Lane>().tokensList[0];

        Vector2 tokenSize = selectedToken.GetComponent<UITexture>().localSize;

        if (toKillToken.GetComponent<Token>().isOpponent == true)
        {
            deadStack = player2deadStack;
        }
        else
        {
            deadStack = player1deadStack;
        }
        float addedY = (deadStack.GetComponent<Lane>().tokensList.Count - 1) * tokenSize.y;
        Vector3 destinationLocation = new Vector3(deadStack.gameObject.transform.localPosition.x, deadStack.gameObject.transform.localPosition.y + addedY, deadStack.gameObject.transform.localPosition.z);

        toKillToken.gameObject.transform.localPosition = Vector3.Lerp(toKillToken.gameObject.transform.localPosition, destinationLocation, 10.0f);

        toKillToken.GetComponent<Token>().laneNumber = deadStack.gameObject.GetComponent<Lane>().laneIndex;
        toKillToken.GetComponent<Token>().isDead = true;
        killTokenLane.GetComponent<Lane>().tokensList.Remove(killTokenLane.GetComponent<Lane>().tokensList[0]);

        deadStack.GetComponent<Lane>().tokensList.Add(toKillToken); //////////////////////////////// current Lane Number

        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Token_Dead);

        //thesun
        saveDataForUndo(selectedLane, deadStack);
    }


    public bool ifKilledToken()
    {
        if (playerTurn == PlayerTurn.Player1)
        {
            for (int i = 0; i < player1deadStack.GetComponent<Lane>().tokensList.Count; i++)
                if (player1deadStack.GetComponent<Lane>().tokensList[i].GetComponent<Token>().isOpponent == false)
                {
                    onTokenClicked(player1deadStack.GetComponent<Lane>().tokensList[i]);
                    return true;
                }
        }
        else if (playerTurn == PlayerTurn.Player2)
        {
            for (int i = 0; i < player2deadStack.GetComponent<Lane>().tokensList.Count; i++)
                if (player2deadStack.GetComponent<Lane>().tokensList[i].GetComponent<Token>().isOpponent == true)
                {
                    onTokenClicked(player2deadStack.GetComponent<Lane>().tokensList[i]);
                    return true;
                }
        }
        return false;
    }

    bool isAPossibleLane(int destinationLaneIndex)
    {
        //thesun
        if (selectedToken == null)
            return false;
        return canBeAPossibleLane(destinationLaneIndex, selectedToken.GetComponent<Token>().laneNumber);
    }


    int isAPossibleMove(int currentLaneIndex, int destinationLaneIndex, bool isDecrementTurns)
    {
        return isAPossibleMove(currentLaneIndex, destinationLaneIndex, isDecrementTurns,
                selectedToken.GetComponent<Token>().isOpponent);
    }

    ///////// to check if a token can be moved on a lane
    //return value: -1: impossible; != -1: possible
    int isAPossibleMove(int currentLaneIndex, int destinationLaneIndex, bool isDecrementTurns, bool isSelectedTokenOpponents)
    {
        // bool isSelectedTokenOpponents = selectedToken.GetComponent<Token>().isOpponent;

        int distanceBtwLanes = calculateDistanceBetweenLanes(currentLaneIndex, destinationLaneIndex);

        int dice1Destination;

        int dice2Destination;

        int bothDicesCombinedDest;

        if (playerTurn == PlayerTurn.Player1)
        {
            dice1Destination = currentLaneIndex + dice1Value;
            dice2Destination = currentLaneIndex + dice2Value;
            bothDicesCombinedDest = currentLaneIndex + dice1Value + dice2Value;
        }
        else
        {
            dice1Destination = currentLaneIndex - dice1Value;
            dice2Destination = currentLaneIndex - dice2Value;
            bothDicesCombinedDest = currentLaneIndex - dice1Value - dice2Value;
        }
        ////////////////////////// check if player clicked his relevant token
        if ((playerTurn == PlayerTurn.Player1 && isSelectedTokenOpponents == false && distanceBtwLanes > 0) ||
            (playerTurn == PlayerTurn.Player2 && isSelectedTokenOpponents == true && distanceBtwLanes < 0))
        {
            if ((dice1Destination == destinationLaneIndex) && dice1Value > 0)
            {
                if (isDecrementTurns)
                    decrementTurns(true);
                return destinationLaneIndex;
            }
            else if ((dice2Destination == destinationLaneIndex) && dice2Value > 0)
            {
                if (isDecrementTurns)
                    decrementTurns(false);
                return destinationLaneIndex;
            }
            else if ((bothDicesCombinedDest == destinationLaneIndex) && dice2Value > 0 && dice1Value > 0
                //thesun
                // && isSelectedTokenOpponents == false
                // && ((playerTurn == PlayerTurn.Player1 && player1deadStack.GetComponent<Lane>().tokensList.Count <= 1) ||
                //     (playerTurn == PlayerTurn.Player2 && player2deadStack.GetComponent<Lane>().tokensList.Count <= 1))
                && (isAPossibleLane(dice1Destination) || isAPossibleLane(dice2Destination)))
            {

                if (playerTurn == PlayerTurn.Player1 && player1deadStack.GetComponent<Lane>().tokensList.Count >= 1)
                    return -1;
                if (playerTurn == PlayerTurn.Player2 && player2deadStack.GetComponent<Lane>().tokensList.Count >= 1)
                    return -1;

                if (isDecrementTurns)
                {
                    int midleDestination = dice1Destination;

                    if (isAPossibleLane(dice1Destination) && isAPossibleLane(dice2Destination))
                    {
                        // make AI smarter
                        Lane laneDes1 = getLanewithIndexNo(dice1Destination).GetComponent<Lane>();
                        if (laneDes1.tokensList.Count > 0 &&
                            laneDes1.tokensList[0].GetComponent<Token>().isOpponent != isSelectedTokenOpponents)
                        {
                            midleDestination = dice1Destination;
                        }
                        else
                        {
                            midleDestination = dice2Destination;
                        }
                    }
                    else if (isAPossibleLane(dice1Destination))
                    {
                        midleDestination = dice1Destination;
                    }
                    else
                    {
                        midleDestination = dice2Destination;
                    }


                    decrementTurns(midleDestination == dice1Destination);
                    int[] moveInfo = { midleDestination, destinationLaneIndex };
                    moveOperationStack.Push(moveInfo);

                    destinationLaneIndex = midleDestination;
                }
                Debug.Log("TheSun bothDicesCombinedDest = " + bothDicesCombinedDest);
                return destinationLaneIndex;
            }
        }
        else
            return -1;

        return -1;
    }

    int destinationLaneTokenCount(int destinationLaneindex)
    {

        GameObject destinationLane = getLanewithIndexNo(destinationLaneindex);

        return destinationLane.GetComponent<Lane>().tokensList.Count;

    }

    bool isDestinationLaneOpponents(int destinationLaneIndex)
    {


        GameObject destinationLane = getLanewithIndexNo(destinationLaneIndex);

        if (destinationLane.GetComponent<Lane>().tokensList.Count > 0)
        {

            GameObject tempToken = destinationLane.GetComponent<Lane>().tokensList[0];
            bool isDestinationOpponents = tempToken.GetComponent<Token>().isOpponent;

            if (isDestinationOpponents == true)
                return true;
            else return false;

        }
        else
            return true;

    }

    bool isAPossibleMaturityMove(int currentLaneIndex, int destinationLaneIndex)
    {

        GameObject tempLane = getLanewithIndexNo(currentLaneIndex);
        GameObject tempToken = tempLane.GetComponent<Lane>().tokensList[0];

        bool isSelectedTokenOpponents = tempToken.GetComponent<Token>().isOpponent;
        int distanceBtwLanes = calculateDistanceBetweenLanes(currentLaneIndex, destinationLaneIndex);
        int dice1Destination;
        int dice2Destination;

        if (playerTurn == PlayerTurn.Player1)
        {
            dice1Destination = currentLaneIndex + dice1Value;
            dice2Destination = currentLaneIndex + dice2Value;
        }
        else
        {
            dice1Destination = currentLaneIndex - dice1Value;
            dice2Destination = currentLaneIndex - dice2Value;
        }

        if (playerTurn == PlayerTurn.Player1 && isSelectedTokenOpponents == false && distanceBtwLanes > 0)
        {
            //thesun
            if ((dice1Destination == destinationLaneIndex) && dice1Value > 0)
            {
                decrementTurns(true);
                return true;
            }
            if ((dice2Destination == destinationLaneIndex) && dice2Value > 0)
            {
                decrementTurns(false);
                return true;
            }

            if ((dice1Destination > destinationLaneIndex) && dice1Value > 0 &&
                (dice2Destination > destinationLaneIndex) && dice2Value > 0)
            {
                decrementTurns(dice1Value < dice2Value);
                return true;
            }

            if ((dice1Destination > destinationLaneIndex) && dice1Value > 0)
            {
                decrementTurns(true);
                return true;
            }
            if ((dice2Destination > destinationLaneIndex) && dice2Value > 0)
            {
                decrementTurns(false);
                return true;
            }

            // if ((dice1Destination >= destinationLaneIndex) && dice1Value > 0)
            // {
            //     decrementTurns(true);

            //     return true;
            // }
            // else if ((dice2Destination >= destinationLaneIndex) && dice2Value > 0)
            // {
            //     decrementTurns(false);

            //     return true;
            // }
        }
        else if (playerTurn == PlayerTurn.Player2 && isSelectedTokenOpponents == true && distanceBtwLanes < 0)
        {
            //thesun
            if ((dice1Destination == destinationLaneIndex) && dice1Value > 0)
            {
                decrementTurns(true);
                return true;
            }
            if ((dice2Destination == destinationLaneIndex) && dice2Value > 0)
            {
                decrementTurns(false);
                return true;
            }

            if ((dice1Destination < destinationLaneIndex) && dice1Value > 0 &&
                (dice2Destination < destinationLaneIndex) && dice2Value > 0)
            {
                decrementTurns(dice1Value < dice2Value);
                return true;
            }

            if ((dice1Destination < destinationLaneIndex) && dice1Value > 0)
            {
                decrementTurns(true);
                return true;
            }
            if ((dice2Destination < destinationLaneIndex) && dice2Value > 0)
            {
                decrementTurns(false);
                return true;
            }
        }
        else
            return false;

        return false;

    }

    GameObject getLanewithIndexNo(int indexNo)
    {
        for (int i = 0; i < lanesList.Count; i++)
        {
            GameObject tempLane = lanesList[i];
            int tempLaneindex = tempLane.GetComponent<Lane>().laneIndex;

            if (indexNo == tempLaneindex)
            {
                //				Debug.Log("current Lane "+tempLaneindex);
                return tempLane;

            }
        }
        return null;
    }

    ////////////////////////////// <moves>

    int calculateDistanceBetweenLanes(int stIndex, int endIndex)
    {

        // Debug.Log("difference in lanes " + (endIndex - stIndex));
        return (endIndex - stIndex);

    }


    void glowPossibleLane()
    {
        if (showPossibleMoves == false)
            return;
        GameObject currentLane = getLanewithIndexNo(selectedToken.GetComponent<Token>().laneNumber);

        if (currentLane.GetComponent<Lane>().tokensList.Count > 0)
        {
            bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;

            if ((playerTurn == PlayerTurn.Player1 && isTokenOpponent == false) ||
                (playerTurn == PlayerTurn.Player2 && isTokenOpponent == true))
            {
                //					Debug.Log(" checkPossibleLanes " + i);

                bool haspossiblemoves = glowDestinationLanes(currentLane.GetComponent<Lane>().laneIndex);

                //					if(haspossiblemoves==false){
                //					noPossibleMoves.gameObject.SetActive(true);
                //					Invoke ("stopRollDiceHandAnimation", 2.0f);
                //					nextPlayersTurn();
                //					}
            }

        }

    }
    void showNoPossibleMoves()
    {
        //thesun
        if (selectedToken != null)
        {
            selectedToken.GetComponent<Token>().ResetTexture();
            selectedToken = null;
        }

        // if (playerTurn == PlayerTurn.Player1)
        {
            TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
                                "Skipping Turn.",
                                "NO VALID MOVES AVAILABLE.",
                                (result) =>
                                {
                                    nextPlayersTurn(false);
                                },
                                new List<string>() { "Okay" }), 0.5f);
        }
        // else
        // {
        //     noPossibleMoves.gameObject.SetActive(true);
        //     Invoke("hideNoPossibleMovesLabel", 2.0f);
        //     nextPlayersTurn(false);
        // }



    }

    void hideNoPossibleMovesLabel()
    {
        noPossibleMoves.gameObject.SetActive(false);
    }



    bool glowDestinationLanes(int currentLaneIndex)
    {
        bool hasPossibleLnes = false;
        int dice1Destination;
        int dice2Destination;
        int bothDiceDestination;

        if (playerTurn == PlayerTurn.Player1)
        {
            dice1Destination = currentLaneIndex + dice1Value;
            dice2Destination = currentLaneIndex + dice2Value;
            bothDiceDestination = currentLaneIndex + dice1Value + dice2Value;
        }
        else
        {
            dice1Destination = currentLaneIndex - dice1Value;
            dice2Destination = currentLaneIndex - dice2Value;
            bothDiceDestination = currentLaneIndex - dice1Value - dice2Value;
        }

        // Debug.Log("Destination Lane Index " + currentLaneIndex);

        ////////////////////////// glow matured lanes


        if (((dice1Destination > 24) || (dice2Destination > 24)) &&
            selectedToken.GetComponent<Token>().isDead == false &&
            ifAllTokensAreInLastQuater())
        {
            hasPossibleLnes = true;
            for (int i = 19; i < currentLaneIndex; i++)
            {
                GameObject currentLane = getLanewithIndexNo(i);
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == false)
                    {
                        hasPossibleLnes = false;
                        break;
                    }
                }
            }


            if (dice1Destination == 25 || dice2Destination == 25)
            {
                hasPossibleLnes = true;
            }

            if (hasPossibleLnes == true)
                Player1MaturityStack.GetComponent<UITexture>().mainTexture = maturedStackTextureGlow;
        }
        else if (((dice1Destination <= 0) || (dice2Destination <= 0)) &&
                selectedToken.GetComponent<Token>().isDead == false &&
                ifAllTokensAreInLastQuater())
        {
            hasPossibleLnes = true;
            for (int i = currentLaneIndex; i <= 6; i++)
            {
                GameObject currentLane = getLanewithIndexNo(i);
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == true)
                    {
                        hasPossibleLnes = false;
                        break;
                    }
                }
            }


            if (dice1Destination == 0 || dice2Destination == 0)
            {
                hasPossibleLnes = true;
            }

            if (hasPossibleLnes == true)
                Player2MaturityStack.GetComponent<UITexture>().mainTexture = maturedStackTextureGlow;



            // Player2MaturityStack.GetComponent<UITexture>().mainTexture = maturedStackTextureGlow;
            // hasPossibleLnes = true;
        }

        /////////////////////////////////////////////////////////

        //thesun
        Player1MaturityStack.GetComponent<UIWidget>().alpha = TSUtil.Instance.SettingShowPossibleMove ? 1 : 0;
        Player2MaturityStack.GetComponent<UIWidget>().alpha = (TSUtil.Instance.SettingShowPossibleMove && playerGameMode == PlayerGameMode.TwoLocalPlayer) ? 1 : 0;


        bool hasPossibleLnesForDice1 = false;
        bool hasPossibleLnesForDice2 = false;

        if ((dice1Destination < (lanesList.Count - 1)) && dice1Destination > 0 && dice1Value > 0 && isAPossibleLane(dice1Destination))
        {
            hasPossibleLnesForDice1 = true;
            GameObject desiredLocation1 = getLanewithIndexNo(dice1Destination);
            if (TSUtil.Instance.SettingShowPossibleMove && TSUtil.Instance.SettingOneTouchMove == false &&
                (playerTurn == PlayerTurn.Player1 || (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.TwoLocalPlayer)))
                desiredLocation1.GetComponent<UITexture>().mainTexture = blinkingTexture;
            else
                desiredLocation1.transform.Find("glow").gameObject.GetComponent<UIWidget>().alpha = 0;

            desiredLocation1.transform.Find("glow").gameObject.SetActive(true);
        }

        if ((dice2Destination < (lanesList.Count - 1)) && dice2Destination > 0 && dice2Value > 0 && isAPossibleLane(dice2Destination))
        {
            hasPossibleLnesForDice2 = true;
            GameObject desiredLocation2 = getLanewithIndexNo(dice2Destination);

            if (TSUtil.Instance.SettingShowPossibleMove && TSUtil.Instance.SettingOneTouchMove == false &&
                (playerTurn == PlayerTurn.Player1 || (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.TwoLocalPlayer)))
                desiredLocation2.GetComponent<UITexture>().mainTexture = blinkingTexture;
            else
                desiredLocation2.transform.Find("glow").gameObject.GetComponent<UIWidget>().alpha = 0;

            desiredLocation2.transform.Find("glow").gameObject.SetActive(true);
        }
        // both dices
        if (TSUtil.Instance.SettingOneTouchMove == false && selectedToken.GetComponent<Token>().isDead == false)
        {
            if (((playerTurn == PlayerTurn.Player1 && player1deadStack.GetComponent<Lane>().tokensList.Count <= 1) ||
                 (playerTurn == PlayerTurn.Player2 && player2deadStack.GetComponent<Lane>().tokensList.Count <= 1))
                && (hasPossibleLnesForDice1 || hasPossibleLnesForDice2))
            {
                if ((bothDiceDestination < (lanesList.Count - 1)) && bothDiceDestination > 0 && bothDiceDestination > 0 && isAPossibleLane(bothDiceDestination))
                {
                    hasPossibleLnes = true;
                    GameObject desiredLocationBoth = getLanewithIndexNo(bothDiceDestination);

                    if (TSUtil.Instance.SettingShowPossibleMove &&
                        (playerTurn == PlayerTurn.Player1 || (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.TwoLocalPlayer)))
                        desiredLocationBoth.GetComponent<UITexture>().mainTexture = blinkingTexture;
                    else
                        desiredLocationBoth.transform.Find("glow").gameObject.GetComponent<UIWidget>().alpha = 0;

                    desiredLocationBoth.transform.Find("glow").gameObject.SetActive(true);
                }
            }

        }

        return (hasPossibleLnes || hasPossibleLnesForDice1 || hasPossibleLnesForDice2);

    }

    /////////////////////////////// </moves>

    public void removeGlowEffects()
    {
        //-1 to remove the deadstack
        hintHandTexture.gameObject.SetActive(false);
        Player1MaturityStack.GetComponent<UITexture>().mainTexture = maturedStackTexturePlain;
        Player2MaturityStack.GetComponent<UITexture>().mainTexture = maturedStackTexturePlain;

        Player1MaturityStack.transform.Find("glow").gameObject.SetActive(false);
        Player2MaturityStack.transform.Find("glow").gameObject.SetActive(false);

        for (int i = 0; i < lanesList.Count; i++)
        {

            GameObject currentLane = getLanewithIndexNo(i);

            if (currentLane.GetComponent<UITexture>() != null)
            {
                if (currentLane.GetComponent<Lane>().laneIndex % 2 == 0)
                    currentLane.GetComponent<UITexture>().mainTexture = redSpikeTexture;
                else
                    currentLane.GetComponent<UITexture>().mainTexture = blueSpikeTexture;
            }

            //thesun
            currentLane.transform.Find("glow").gameObject.SetActive(false);
            currentLane.transform.Find("glow").gameObject.GetComponent<UIWidget>().alpha = 1;
        }
    }


    void iAmRollingDice(bool status)
    { // On remote side isRemoteClientTurn is true, and on client side isRemoteClientTurn is false
        if (isRemoteClientTurn == false)
        {
            photonView.RPC("RollDice", PhotonTargets.Others, dice1floattime, dice2floattime, true); // Tell Remote players that it mine turn
            RollDice(dice1floattime, dice2floattime, false);// Remote Players are waiting for their turn
        }
    }


    ////////////////////////////////////////////////                          Dice Rolling Methods

    void RollDiceBtnClicked()
    {
        dice1floattime = UnityEngine.Random.Range(0.25f, 0.5f) + 0.5f;
        dice2floattime = UnityEngine.Random.Range(0.10f, 0.5f) + 0.5f;

        if (isGameTwoPlayer)
        {
            iAmRollingDice(true);
        }
        else
        {

            RollDice(dice1floattime, dice2floattime, false);// Remote Players are waiting for their turn

        }

    }


    [PunRPC]
    public void RollDice(float dice1time, float dice2time, bool isRPC)
    {
        if (isRPC)
        {
            isRemoteClientTurn = true;
        }
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Dice_Drop);


        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Roll Dice Button Clicked", 1);
        //	TSUtil.LogEvent (Analytic      UniversalAnalytics.LogEven.GA_CATEGORY_TYPE_GAMEPLAY,Analytic      UniversalAnalytics.LogEven.GA_ACTION_TYPE_BUTTON,"Roll Dice Button Clicked",1);

        dice1floattime = dice1time;
        dice2floattime = dice2time;

        removeGlowEffects();

        dice1Value = 0;
        dice2Value = 0;

        dice2.gameObject.SetActive(false);

        dice1.gameObject.SetActive(false);

        diceRollHand.gameObject.SetActive(true);

        diceRollHand.gameObject.GetComponent<UI2DSpriteAnimation>().Play();

        //thesun
        //Invoke("stopRollDiceHandAnimation", 1.6f);
        Invoke("startRollDiceAnimation", 0.1f);

        rollDiceButton.SetActive(false);


    }




    void startRollDiceAnimation()
    {

        if (isRemoteClientTurn)
        {
            GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Computer_Roll);

        }
        else
        {
            GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Dice_Rolling);
        }

        dice1.gameObject.SetActive(true);
        dice2.gameObject.SetActive(true);

        //thesun shuffle dice
        dice1.gameObject.GetComponent<UI2DSpriteAnimation>().frames.Shuffle();
        dice2.gameObject.GetComponent<UI2DSpriteAnimation>().frames.Shuffle();
        dice1.gameObject.GetComponent<UI2DSpriteAnimation>().frames.Shuffle();

        dice1.gameObject.GetComponent<UI2DSpriteAnimation>().Play();
        dice2.gameObject.GetComponent<UI2DSpriteAnimation>().Play();

        // isDiceRolled = true;
        rolledDiceTurnsLeft = 2;
        //Orignal Dice Rolling Stop Functionality Commented
        //		Invoke ("stopRollDice1Animation", dice1floattime);
        //		
        //		Invoke ("stopRollDice2Animation", dice2floattime);
        //		
        //		Invoke ("checkIfSameDiceValues", 0.7f);
        //		
        //		Invoke ("ifKilledToken", 0.6f);
        //		
        //		Invoke ("checkPossibleMoves", 0.8f);



        if (isGameTwoPlayer)
        {
            if (isRemoteClientTurn == false)
            {
                Invoke("stopRollDice1Animation", dice1floattime);

                Invoke("stopRollDice2Animation", dice2floattime);

                Invoke("checkIfSameDiceValues", 0.7f);

                Invoke("ifKilledToken", 0.6f);

                Invoke("checkPossibleMoves", 0.8f);

                Invoke("UpdateRemotePlayerForDiceFaces", 1f);



            }


        }
        else
        {
            // Invoke("stopRollDice1Animation", dice1floattime);
            // Invoke("stopRollDice2Animation", dice2floattime);
            // Invoke("checkIfSameDiceValues", 0.1f + Math.Max(dice1floattime, dice2floattime));

            TSUtil.RunLater(() => { stopRollDice1Animation(); }, dice1floattime);
            TSUtil.RunLater(() => { stopRollDice2Animation(); }, dice2floattime);
            TSUtil.RunLater(() => { checkIfSameDiceValues(); }, 0.1f + Math.Max(dice1floattime, dice2floattime));

            // if (playerTurn != PlayerTurn.None)
            // {
            //     // Invoke("ifKilledToken", 0.6f + Math.Max(dice1floattime, dice2floattime));
            //     // Invoke("checkPossibleMoves", 0.8f);

            //     TSUtil.RunLater(() =>
            //     {
            //         ifKilledToken();
            //         checkPossibleMoves();
            //     }, 0.3f + Math.Max(dice1floattime, dice2floattime));
            // }

        }
        //		if(isTutorialOn==true)
        //			Invoke("onHintButtonClicked",1.0f);

    }

    void UpdateRemotePlayerForDiceFaces()
    {
        string sprtieName1 = dice1.gameObject.GetComponent<UI2DSprite>().sprite2D.name;
        string sprtieName2 = dice2.gameObject.GetComponent<UI2DSprite>().sprite2D.name;

        photonView.RPC("ProcessRemoteDices", PhotonTargets.Others, sprtieName1, sprtieName2); // Tell Remote players that it mine turn
    }

    [PunRPC]
    void ProcessRemoteDices(string dice1SpriteName, string dice2SpriteName)
    {

        int.TryParse(dice1SpriteName, out dice1Value);
        int.TryParse(dice2SpriteName, out dice2Value);

        dice1.gameObject.GetComponent<UI2DSprite>().sprite2D = dice1.gameObject.GetComponent<UI2DSpriteAnimation>().frames[dice1Value - 1];
        dice1.gameObject.GetComponent<UI2DSpriteAnimation>().Pause();

        dice2.gameObject.GetComponent<UI2DSprite>().sprite2D = dice2.gameObject.GetComponent<UI2DSpriteAnimation>().frames[dice2Value - 1];
        dice2.gameObject.GetComponent<UI2DSpriteAnimation>().Pause();

        Invoke("checkIfSameDiceValues", 0.7f);

        Invoke("ifKilledToken", 0.6f);

        Invoke("checkPossibleMoves", 0.8f);
    }


    void checkPossibleMoves()
    {

        //		GameObject currentLane=getLanewithIndexNo(selectedToken.GetComponent<Token> ().laneNumber); //////////////////////////////  current Lane Number
        //		
        //		if(isAPossibleMaturityMove(selectedToken.GetComponent<Token> ().laneNumber,selectedLaneIndex) && ifAllTokensAreInLastQuater()) 
        //		{

        //	checkIfGameOver ();

        //thesun
        // if (checkIfHasPossibleMoves() == false)
        // {
        //     if (isGameTwoPlayer == true)
        //         Invoke("hideNoPossibleMovesLabel", 1.5f);

        //     //thesun
        //     //nextPlayersTurn(false);
        // }

    }

    bool checkIfHasPossibleMoves()
    {

        bool hasAPossbileMove = false;

        if (playerTurn == PlayerTurn.Player1 && player1deadStack.GetComponent<Lane>().tokensList.Count > 0)
            return ifLaneIsAPossibleMove(player1deadStack.GetComponent<Lane>().laneIndex);


        else if (playerTurn == PlayerTurn.Player2 && player2deadStack.GetComponent<Lane>().tokensList.Count > 0)
            return ifLaneIsAPossibleMove(player2deadStack.GetComponent<Lane>().laneIndex);

        for (int i = 0; i < lanesList.Count; i++)
        {

            GameObject temp = lanesList[i];
            GameObject currentLane = getLanewithIndexNo(temp.GetComponent<Lane>().laneIndex);

            if (currentLane.GetComponent<Lane>().tokensList.Count > 0)
            {
                bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;

                if ((playerTurn == PlayerTurn.Player1 && isTokenOpponent == false) ||
                    (playerTurn == PlayerTurn.Player2 && isTokenOpponent == true))
                {
                    // Debug.Log(" checkPossibleLanes " + i);

                    bool result = ifLaneIsAPossibleMove(currentLane.GetComponent<Lane>().laneIndex);

                    if (result == true)
                    {
                        hasAPossbileMove = true;
                        break;
                    }
                    //return true;
                }

            }
        }
        if (isOnlyMaturityMovesLeft() == true)
            return true;
        //		else
        //			return false;



        return hasAPossbileMove;
    }

    bool isOnlyMaturityMovesLeft()
    {

        //thesun
        // if (ifAllTokensAreInLastQuater())
        // {
        //     if (playerTurn == PlayerTurn.Player1)
        //         selectedLane = Player1MaturityStack;
        //     else
        //         selectedLane = Player2MaturityStack;

        //     int selectedLaneIndex = selectedLane.GetComponent<Lane>().laneIndex;
        //     Debug.Log("selected lane index" + selectedLaneIndex);

        //     for (int i = 0; i < lanesList.Count; i++)
        //     {

        //         GameObject temp = lanesList[i];
        //         GameObject currentLane = getLanewithIndexNo(temp.GetComponent<Lane>().laneIndex);

        //         if (currentLane.GetComponent<Lane>().tokensList.Count > 0)
        //         {

        //             GameObject tempToken = currentLane.GetComponent<Lane>().tokensList[0];

        //             if (isAPossibleMaturityMove(tempToken.GetComponent<Token>().laneNumber, selectedLaneIndex) == true)
        //                 return true;

        //         }
        //     }
        // }
        return false;
    }


    bool ifLaneIsAPossibleMove(int currentLaneIndex)
    {
        bool hasPossibleLnes = false;
        int dice1Destination;
        int dice2Destination;

        if (playerTurn == PlayerTurn.Player1)
        {
            dice1Destination = currentLaneIndex + dice1Value;
            dice2Destination = currentLaneIndex + dice2Value;
        }
        else
        {
            dice1Destination = currentLaneIndex - dice1Value;
            dice2Destination = currentLaneIndex - dice2Value;
        }


        //		Debug.Log ("Destination Lane Index " + currentLaneIndex);

        ////////////////////////// glow matured lanes
        /// 
        // GameObject currentLane = getLanewithIndexNo(currentLaneIndex);
        // GameObject token = currentLane.GetComponent<Lane>().tokensList[0];


        //thesun
        if (((dice1Destination > 24) || (dice2Destination > 24)) && ifAllTokensAreInLastQuater())
        {
            hasPossibleLnes = true;
            for (int i = 19; i < currentLaneIndex; i++)
            {
                GameObject currentLane = getLanewithIndexNo(i);
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == false)
                    {
                        hasPossibleLnes = false;
                        break;
                    }
                }
            }
            if (dice1Destination == 25 || dice2Destination == 25)
            {
                hasPossibleLnes = true;
            }
        }
        else if (((dice1Destination <= 0) || (dice2Destination <= 0)) && ifAllTokensAreInLastQuater())
        {
            hasPossibleLnes = true;

            for (int i = 6; i > currentLaneIndex; i--)
            {
                GameObject currentLane = getLanewithIndexNo(i);
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == true)
                    {
                        hasPossibleLnes = false;
                        break;
                    }
                }
            }
            if (dice1Destination == 0 || dice2Destination == 0)
            {
                hasPossibleLnes = true;
            }
        }


        if ((dice1Destination < (lanesList.Count - 1)) && dice1Destination > 0 && dice1Value > 0
            && canBeAPossibleLane(dice1Destination, currentLaneIndex))
        {
            hasPossibleLnes = true;
        }

        if ((dice2Destination < (lanesList.Count - 1)) && dice2Destination > 0 && dice2Value > 0
            && canBeAPossibleLane(dice2Destination, currentLaneIndex))
        {
            hasPossibleLnes = true;
        }
        return hasPossibleLnes;
    }


    bool canBeAPossibleLane(int destinationLaneIndex, int currentLaneIndex)
    {

        GameObject destinationLane = getLanewithIndexNo(destinationLaneIndex);
        GameObject currentLane = getLanewithIndexNo(currentLaneIndex);

        if (destinationLane == null || currentLane == null)
        {
            return false;
        }

        bool isDestLaneOpponents = false;
        bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;

        if (destinationLane.GetComponent<Lane>().tokensList.Count == 1)
            return true;

        /////////////////////////// if a lane has more than 1 token it should be of the same player // cannot move a token on other players stack
        if (destinationLane.GetComponent<Lane>().tokensList.Count > 0)
            isDestLaneOpponents = destinationLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
        else
            return true;

        if ((isTokenOpponent == true && isDestLaneOpponents == true) ||
            (isDestLaneOpponents == false && isTokenOpponent == false))
            return true;

        return false;
    }

    ///////////////////////////////	



    void stopRollDiceHandAnimation()
    {

        diceRollHand.gameObject.GetComponent<UI2DSpriteAnimation>().ResetToBeginning();
        diceRollHand.gameObject.SetActive(false);
        startRollDiceAnimation();

    }

    void stopRollDice1Animation()
    {

        //	dice1.gameObject.SetActive (false);
        dice1.gameObject.GetComponent<UI2DSpriteAnimation>().Pause();

        string sprtieName1 = dice1.gameObject.GetComponent<UI2DSprite>().sprite2D.name;

        //		int dice1value;
        int.TryParse(sprtieName1, out dice1Value);

        diceOneLb.GetComponent<UILabel>().text = dice1Value + "";

    }


    void stopRollDice2Animation()
    {
        //	dice2.gameObject.SetActive (false);
        dice2.gameObject.GetComponent<UI2DSpriteAnimation>().Pause();

        string sprtieName2 = dice2.gameObject.GetComponent<UI2DSprite>().sprite2D.name;

        //		int dice2value;
        int.TryParse(sprtieName2, out dice2Value);

        diceTwoLb.GetComponent<UILabel>().text = dice2Value + "";




    }


    void StartLerping()
    {

        _isLerping = true;
        _timeStartedLerping = Time.time;

        if (rolledDiceTurnsLeft == 0)
            isDiceRolled = false;

    }

    public void StopGame()
    {
        playerTurn = PlayerTurn.GameOver;
        _isLerping = false;
    }

    void FixedUpdate()
    {
        if (_isLerping || _isUndoing)
        {
            //thesun
            if (selectedLane == null || selectedToken == null)
            {
                _isLerping = false;
                return;
            }

            float timeSinceStarted = Time.time - _timeStartedLerping;
            float percentageComplete = timeSinceStarted / timeTakenDuringLerp;
            //Debug.Log("percentageComplete ==" + percentageComplete);

            int tokenLaneCount = selectedLane.GetComponent<Lane>().tokensList.Count;

            UITexture selectedLaneTexture = selectedLane.GetComponent<UITexture>();

            Vector2 tokenSize = selectedToken.GetComponent<UITexture>().localSize;
            Vector2 laneSize = mapPosBoardBame2BoardGameOverlay(selectedLaneTexture.localSize);


            float topLanPos = selectedLaneTexture.GetSides(this.boardGameOverlay.transform)[1].y - 32;
            float bottomLanPos = selectedLaneTexture.GetSides(this.boardGameOverlay.transform)[3].y + 32;
            float leftLanPos = selectedLaneTexture.GetSides(this.boardGameOverlay.transform)[0].x;

            Vector3 lanePos = mapPosBoardBame2BoardGameOverlay(selectedLane.transform.localPosition);

            float tokenDestinationY = 0f;
            float tokenDestinationX = lanePos.x;

            int laneNumber = selectedToken.GetComponent<Token>().laneNumber;

            if (selectedLane == Player1MaturityStack || selectedLane == Player2MaturityStack)
            {
                tokenDestinationY = selectedLaneTexture.GetSides(this.boardGameOverlay.transform)[1].y - laneSize.y / 2.0f;
                tokenDestinationX = leftLanPos + (tokenSize.x / 5.0f) * (tokenLaneCount - 1) + 10;

                Vector2 newSize = Vector2.Lerp(selectedToken.GetComponent<UITexture>().localSize, new Vector2(35, 35), percentageComplete);
                selectedToken.GetComponent<UITexture>().SetDimensions((int)newSize.x, (int)newSize.y);

                // selectedToken.GetComponent<UITexture>().SetDimensions (35, 35);
            }
            // else if (selectedLane == player1deadStack)
            // {

            // }
            // else if (selectedLane == player2deadStack)
            // {

            // }
            else
            {
                float laneHight = laneSize.y;
                if (TSUtil.isIPhone())
                {
                    if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Portrait)
                    {
                        laneHight -= 50;
                    }
                }

                float tokenHigh = laneHight / 5.0f;
                if (tokenLaneCount > 5)
                {
                    // tokenHigh = laneHight / tokenLaneCount;
                    tokenLaneCount = 5;
                }

                if (laneNumber <= 12)
                    tokenDestinationY = bottomLanPos + tokenHigh * (tokenLaneCount - 1);
                else
                    tokenDestinationY = topLanPos - tokenHigh * (tokenLaneCount - 1);


                //thesun
                if (_isUndoing)
                {
                    Vector2 newSize = Vector2.Lerp(selectedToken.GetComponent<UITexture>().localSize, selectedToken.GetComponent<Token>().getOrgTokenSize(), percentageComplete);
                    selectedToken.GetComponent<UITexture>().SetDimensions((int)newSize.x, (int)newSize.y);
                }
            }

            Vector3 newPosition = new Vector3(tokenDestinationX, tokenDestinationY, selectedLane.transform.localPosition.z);

            // Debug.Log("Token Destination y = " + tokenDestinationY);

            selectedToken.transform.localPosition = Vector3.Lerp(selectedToken.gameObject.transform.localPosition, newPosition, percentageComplete);
            if (percentageComplete >= 1.0f)
            {
                reArrangeTokensOnAllLanes();
                reArrangeTokensOnAllStack();

                if (selectedToken.GetComponent<Token>().isMatured == true)
                {
                    selectedToken.GetComponent<UITexture>().mainTexture = selectedToken.GetComponent<Token>().tokenMatureTexture;
                    //selectedToken.GetComponent<UITexture>().MakePixelPerfect(); //thesun
                    //selectedToken.GetComponent<UITexture>().SetDimensions (35, 35);
                }
                else
                {
                    selectedToken.GetComponent<Token>().ResetTexture();
                }

                //thesun
                updatePip();
                if (_isUndoing)
                {
                    if (selectedLane != player1deadStack && selectedLane != player2deadStack)
                        selectedToken.GetComponent<Token>().isDead = false;
                    else
                        selectedToken.GetComponent<Token>().isDead = true;

                    //activeDeactiveDice();

                    _isUndoing = false;
                    selectedToken = null;
                    selectedLane = null;

                    if (undoStack.Count > 0)
                    {
                        UndoData undoData = undoStack.Peek();
                        if ((playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.PlayerVSCPU) ||
                            undoData.isKillTokenMove ||
                            (playerTurn == PlayerTurn.Player2 && undoData.isBeginRoll && playerGameMode == PlayerGameMode.PlayerVSCPU))
                        {
                            TSUtil.RunLater(() => { doUndoGame(); }, 0.01f);
                        }

                        if (playerTurn == PlayerTurn.Player1 ||
                            (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.TwoLocalPlayer))
                        {
                            if (TSUtil.Instance.SettingOneTouchMove == false)
                                ifKilledToken();
                        }

                    }
                    else
                    {
                        if (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.PlayerVSCPU)
                            doUndoGame();
                    }


                    return;
                }

                _isLerping = false;
                selectedToken.GetComponent<Token>().isDead = false;
                selectedToken = null;
                selectedLane = null;


                if (checkIfGameOver() == true)
                {
                    percentageComplete = 0.0f;
                    return;
                }

                if (moveOperationStack.Count > 0)
                {
                    activeDeactiveDice();
                    int[] moveInfo = (int[])moveOperationStack.Pop();

                    doMoveToken(moveInfo[0], moveInfo[1]);
                    return;
                }

                if (rolledDiceTurnsLeft > 0)
                {
                    removeGlowEffects();
                    activeDeactiveDice();
                    if ((playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.PlayerVSCPU) ||
                        TSUtil.Instance.SettingOneTouchMove == false)
                    {
                        ifKilledToken();
                    }


                    //thesun
                    if (checkIfHasPossibleMoves())
                    {
                        if (playerGameMode == PlayerGameMode.PlayerVSCPU)
                        {
                            if (isGameTwoPlayer == false && playerTurn == PlayerTurn.Player2)
                                autoMovePlayer2(false);
                        }

                    }
                    else
                    {
                        showNoPossibleMoves();
                    }
                }
                else
                {

                    if (isGameTwoPlayer)
                    {
                        if (isRemoteClientTurn == false)
                        { // Other Players are Idle, I am taking Turn
                            // Debug.Log("Rolled Dice Turns " + rolledDiceTurnsLeft);
                            iHaveCompletedmyTurn();
                            //nextPlayersTurn(true);
                        }

                    }
                    else
                    {
                        activeDeactiveDice();
                        nextPlayersTurn(false);

                        if (playerTurn == PlayerTurn.Player1 ||
                            (playerGameMode == PlayerGameMode.TwoLocalPlayer))
                            saveGame();
                    }
                }

            }
        }
    }


    public void reArrangeTokensOnAllStack()
    {
        foreach (GameObject stackLane in stackList)
        {
            Lane currentLane = stackLane.GetComponent<Lane>();
            UITexture currentLaneTexture = currentLane.GetComponent<UITexture>();
            Vector2 laneSize = currentLaneTexture.localSize;

            float leftLanPos = currentLaneTexture.GetSides(this.boardGameOverlay.transform)[0].x;

            for (int i = 0; i < currentLane.tokensList.Count; i++)
            {
                GameObject currentToken = currentLane.tokensList[i];
                moveGameObjectToFront(currentToken);

                currentToken.GetComponent<UITexture>().SetDimensions(35, 35);
                Vector2 tokenSize = currentToken.GetComponent<UITexture>().localSize;


                float tokenDestinationY = currentLaneTexture.GetSides(this.boardGameOverlay.transform)[1].y - laneSize.y / 2.0f;
                float tokenDestinationX = leftLanPos + (tokenSize.x / 5.0f) * i + 10;

                currentToken.transform.localPosition = new Vector3(tokenDestinationX, tokenDestinationY, currentLane.transform.localPosition.z);

            }

        }
    }

    public void reArrangeTokensOnAllLanes()
    {
        foreach (GameObject g in lanesList)
        {
            reArrangeTokensOnLane(g.GetComponent<Lane>().laneIndex);
        }
    }

    public void reArrangeTokensOnLane(int laneNumber)
    {
        // if (laneNumber <= 0 || laneNumber > 24)
        //     return;

        GameObject currentLane = getLanewithIndexNo(laneNumber);

        if (currentLane == null)
            return;

        Vector2 laneSize = mapPosBoardBame2BoardGameOverlay(currentLane.GetComponent<UITexture>().localSize);

        float topLanPos = currentLane.GetComponent<UITexture>().GetSides(this.boardGameOverlay.transform)[1].y - 32;
        float bottomLanPos = currentLane.GetComponent<UITexture>().GetSides(this.boardGameOverlay.transform)[3].y + 32;

        Vector3 lanePos = mapPosBoardBame2BoardGameOverlay(currentLane.transform.localPosition);

        int tokenLaneCount = currentLane.GetComponent<Lane>().tokensList.Count;

        float laneHight = laneSize.y;
        if (TSUtil.isIPhone())
        {
            if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Portrait)
            {
                laneHight -= 50;
            }
        }

        for (int k = 0; k < tokenLaneCount; k++)
        {
            GameObject currentToken = currentLane.GetComponent<Lane>().tokensList[k];
            Vector2 tokenSize = currentToken.GetComponent<UITexture>().localSize;

            float tokenHigh = laneHight / 5.0f;//tokenSize.y;
            float tokenDestinationY;

            if (tokenLaneCount > 5)
            {
                moveGameObjectToFront(currentToken);
                tokenHigh = laneHight / tokenLaneCount;
            }

            if (laneNumber <= 12)
                tokenDestinationY = bottomLanPos + (tokenHigh * k);
            else
                tokenDestinationY = topLanPos - (tokenHigh * k);

            Vector3 newPosition = new Vector3(lanePos.x, tokenDestinationY, currentLane.gameObject.transform.localPosition.z);
            currentToken.transform.localPosition = newPosition;

        }



    }


    // isRemoteClientTurn on client side is true and on remote side isRemoteClientTurn is false;
    public void iHaveCompletedmyTurn()
    {
        isRemoteClientTurn = true;
        photonView.RPC("activeDeactiveDice", PhotonTargets.All);
        photonView.RPC("nextPlayersTurn", PhotonTargets.Others, true);
        nextPlayersTurn(false);
    }

    public void skipOtherPlayersTurn()
    {

        photonView.RPC("skipOtherPlayersTurnRPC", PhotonTargets.Others);
    }

    [PunRPC]
    public void skipOtherPlayersTurnRPC()
    {
        rollDiceButton.SetActive(false);
        isRemoteClientTurn = true;
        activeDeactiveDice();
        //nextPlayersTurn (true);
    }


    [PunRPC]
    public void nextPlayersTurn(bool isRPC)
    {

        hideAllDices();
        if (isGameTwoPlayer == false)
        {
            removeGlowEffects();
            rollDiceButton.SetActive(true);

        }
        else
        {

            rollDiceButton.SetActive(true);
            if (isRPC)
            {
                isRemoteClientTurn = !isRemoteClientTurn;

            }
            else
            {
                rollDiceButton.SetActive(false);
            }
            removeGlowEffects();

        }

        if (playerTurn == PlayerTurn.Player1)
            playerTurn = PlayerTurn.Player2;
        else
            playerTurn = PlayerTurn.Player1;

        if (playerGameMode == PlayerGameMode.PlayerVSCPU)
        {
            foreach (GameObject item in tokens)
            {
                item.GetComponent<BoxCollider>().enabled = playerTurn == PlayerTurn.Player1;
            }
            foreach (GameObject item in lanesList)
            {
                item.GetComponent<BoxCollider>().enabled = playerTurn == PlayerTurn.Player1;
            }
        }

        updatePlayerTurnLabel();

    }
    public void hideAllDices()
    {
        dice1.gameObject.SetActive(false);
        dice2.gameObject.SetActive(false);
        dice3.gameObject.SetActive(false);
        dice4.gameObject.SetActive(false);

    }

    public void updatePlayerTurnLabel(bool updateIconOnly = false)
    {
        GameObject currentLayout = getActiveLayout();
        UITexture player1Icon = currentLayout.transform.Find("Player1InfoWidget/TokenTexture").GetComponent<UITexture>();
        UITexture player2Icon = currentLayout.transform.Find("Player2InfoWidget/TokenTexture").GetComponent<UITexture>();
        if (player1Icon && samplePlayer1Token)
        {
            player1Icon.mainTexture = samplePlayer1Token.tokenMatureTexture;
            player2Icon.mainTexture = samplePlayer2Token.tokenMatureTexture;
        }

        if (playerTurn == PlayerTurn.Player1)
        {
            player1Icon.mainTexture = samplePlayer1Token.glowedTexture;

            turnLabel.text = "Player 1";
            turnLabel.color = new Color(0.819f, 0.619f, 0.3686f);
        }

        else if (playerTurn == PlayerTurn.Player2)
        {
            player2Icon.mainTexture = samplePlayer2Token.glowedTexture;

            turnLabel.text = "Player 2";
            turnLabel.color = new Color(0.6f, 0.2078f, 0.1686f);

            if (playerGameMode == PlayerGameMode.PlayerVSCPU)
            {
                if (isGameTwoPlayer == false && updateIconOnly == false)
                    autoMovePlayer2(true);
            }
        }
    }

    [PunRPC]
    public void activeDeactiveDice()
    {
        if (dice2Value == 0)
            dice2.gameObject.SetActive(false);
        if (dice1Value == 0)
            dice1.gameObject.SetActive(false);

        // dice1.gameObject.SetActive(dice1Value != 0);
        // dice2.gameObject.SetActive(dice2Value != 0);
        // dice3.gameObject.SetActive(rolledDiceTurnsLeft >= 3);
        // dice4.gameObject.SetActive(rolledDiceTurnsLeft >= 4);

    }

    ////////////////////////////////////////////////////////////////////////////////                 level completion stack methods

    public void onStackClicked(GameObject stack)
    {


        if (isGameTwoPlayer)
        {
            if (isRemoteClientTurn)
            {
                return;
            }
            photonView.RPC("onStackClickedRPC", PhotonTargets.Others, stack.GetComponent<Lane>().laneIndex);
        }


        // Debug.Log("StackClicked");


        //thesun
        if (stack.GetComponent<UITexture>().mainTexture != maturedStackTextureGlow)
            return;

        if (selectedToken != null && isDiceRolled == true)
        {

            selectedLane = stack;

            int selectedLaneIndex = selectedLane.GetComponent<Lane>().laneIndex;
            // Debug.Log("selected lane index" + selectedLaneIndex);

            GameObject currentLane = getLanewithIndexNo(selectedToken.GetComponent<Token>().laneNumber); //////////////////////////////  current Lane Number

            if (ifAllTokensAreInLastQuater())
            {
                //thesun
                updateTempUndoData();

                if (isAPossibleMaturityMove(selectedToken.GetComponent<Token>().laneNumber, selectedLaneIndex))
                {

                    // Debug.Log("move");
                    selectedToken.GetComponent<Token>().laneNumber = selectedLaneIndex;
                    selectedToken.GetComponent<Token>().isMatured = true;
                    //selectedToken.GetComponent<UITexture>().mainTexture = selectedToken.GetComponent<Token> ().tokenMatureTexture;
                    selectedLane.GetComponent<Lane>().tokensList.Add(selectedToken); //////////////////////////////// current Lane Number
                    currentLane.GetComponent<Lane>().tokensList.Remove(selectedToken);

                    //thesun
                    saveDataForUndo(currentLane, selectedLane);


                    StartLerping();

                    if (_isLerping)
                    {
                        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.Token_Mature);
                    }

                }
            }
        }
    }

    GameObject getStackById(int id)
    {
        GameObject selectedObj = null;
        foreach (GameObject g in stackList)
        {
            if (g.GetComponent<Lane>().laneIndex == id)
            {
                selectedObj = g;
            }
        }
        return selectedObj;
    }

    bool ifAllTokensAreInLastQuater()
    {

        bool isInLastQuadrant = false;

        for (int i = 0; i < lanesList.Count; i++)
        {

            GameObject temp = lanesList[i];
            GameObject currentLane = getLanewithIndexNo(temp.GetComponent<Lane>().laneIndex);

            if (currentLane.GetComponent<Lane>().tokensList.Count > 0)
            {
                bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                int tokenLaneNumber = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().laneNumber;


                if ((playerTurn == PlayerTurn.Player1 && isTokenOpponent == false) ||
                    (playerTurn == PlayerTurn.Player2 && isTokenOpponent == true))
                {

                    if ((playerTurn == PlayerTurn.Player1 && isTokenOpponent == false && tokenLaneNumber > 18) ||
                        (playerTurn == PlayerTurn.Player2 && isTokenOpponent == true && tokenLaneNumber <= 6))
                    {
                        isInLastQuadrant = true;

                    }
                    else
                    {
                        isInLastQuadrant = false;
                        return false;
                    }

                }
            }
        }

        return isInLastQuadrant;

    }



    /// <summary>           check if same values on dice </summary>
    public void checkIfSameDiceValues()
    {
        // dice1Value = 1;
        // dice2Value = 3;

        if (playerTurn == PlayerTurn.GameOver)
        {
            return;
        }
        
        Debug.Log("dice1Value = " + dice1Value + " dice2Value = " + dice2Value);

        if (playerTurn == PlayerTurn.None)
        {
            // TweenTransform tw1 = TweenTransform.Begin(dice1, 0.35f, getActiveLayout().transform.Find("Player1InfoWidget/ScoreLabel").transform);
            // tw1.SetOnFinished(tw1.PlayReverse);
            // // tw1.AddOnFinished(() =>
            // // {
            // //     TSUtil.RunLater(() =>{tw1.PlayReverse();}, 0.35f);
            // // });

            // TweenTransform tw2 = TweenTransform.Begin(dice2, 0.35f, getActiveLayout().transform.Find("Player2InfoWidget/ScoreLabel").transform);
            // tw2.SetOnFinished(tw2.PlayReverse);
            // // tw2.AddOnFinished(() =>
            // // {
            // //     TSUtil.RunLater(() =>{tw2.PlayReverse();}, 0.35f);
            // // });

            // TSUtil.RunLater(() =>
            // {
            //     Destroy(tw1);
            //     Destroy(tw2);
            // }, 1.3f);


            if (dice1Value > dice2Value)
            {
                //generalMessageBox.showMessageAutoHide("Player 1 has won the dice roll. \n White goes first");                
                playerTurn = PlayerTurn.Player1;

                TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
                       TSUtil.Instance.SettingColorCheckers ? "White goes first" : "Black goes first",
                       "Player 1 has won the dice roll.",
                       (result) =>
                       {
                           //    ifKilledToken();
                           isDiceRolled = true;
                           updatePlayerTurnLabel(true);
                           saveGame();
                       },
                       new List<string>() { "Okay" }), 0.2f);


            }
            else if (dice1Value < dice2Value)
            {                
                playerTurn = PlayerTurn.Player2;
                // generalMessageBox.showMessageAutoHide("Player 2 has won the dice roll. \n Black goes first");
                // // updatePlayerTurnLabel();
                // Invoke("checkAutoPlayerPossibleMoves", 1.5f);

                TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
                    TSUtil.Instance.SettingColorCheckers ? "Black goes first" : "White goes first",
                    "Player 2 has won the dice roll.",
                    (result) =>
                    {
                        isDiceRolled = true;
                        if (playerGameMode == PlayerGameMode.PlayerVSCPU)
                        {
                            updatePlayerTurnLabel(true);
                            checkAutoPlayerPossibleMoves();
                        }
                        else
                        {
                            updatePlayerTurnLabel(true);
                            saveGame();
                        }
                    },
                    new List<string>() { "Okay" }), 0.2f);
            }
            else
            {
                // generalMessageBox.showMessageAutoHide("Two Dices Equal. Roll again!", 0.85f);
                // TSUtil.RunLater(() => RollDiceBtnClicked(), 1.2f);

                TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
                    " ",
                    "Two Dices Equal",
                    (result) =>
                    {
                        RollDiceBtnClicked();
                    },
                    new List<string>() { "Roll again!" }), 0.2f);

            }
            return;
        }


        // dice1Value = 2;
        // dice2Value = 4;
        isDiceRolled = true;
        if (dice1Value == dice2Value)
        {
            rolledDiceTurnsLeft = 4;
            dice3.gameObject.SetActive(true);
            dice4.gameObject.SetActive(true);

            dice3.gameObject.GetComponent<UI2DSprite>().sprite2D = dice1.gameObject.GetComponent<UI2DSprite>().sprite2D;
            dice4.gameObject.GetComponent<UI2DSprite>().sprite2D = dice1.gameObject.GetComponent<UI2DSprite>().sprite2D;
        }

        if (TSUtil.Instance.SettingOneTouchMove == false ||
            (playerGameMode == PlayerGameMode.PlayerVSCPU && playerTurn == PlayerTurn.Player2))
            ifKilledToken();

        if (playerTurn == PlayerTurn.Player1 ||
            (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.TwoLocalPlayer))
            saveDataForUndoAtBeginRoll();

        if (checkIfHasPossibleMoves())
        {
            if (playerGameMode == PlayerGameMode.PlayerVSCPU)
            {
                if (playerTurn == PlayerTurn.Player2)
                {
                    TSUtil.RunLater(() => { checkAutoPlayerPossibleMoves(); }, 0.7f);
                }
            }
        }
        else
        {
            showNoPossibleMoves();
        }
    }

    public void decrementTurns(bool isDice1)
    {

        if (rolledDiceTurnsLeft == 4)
            dice4.gameObject.SetActive(false);
        else if (rolledDiceTurnsLeft == 3)
            dice3.gameObject.SetActive(false);

        if (rolledDiceTurnsLeft <= 2)
        {
            if (isDice1 == true)
                dice1Value = 0;
            else if (isDice1 == false)
                dice2Value = 0;
        }
        rolledDiceTurnsLeft--;
    }

    bool checkIfGameOver()
    {

        //TSUtil.LogEvent (Analytic      UniversalAnalytics.LogEven.GA_CATEGORY_TYPE_GAMEPLAY,Analytic      UniversalAnalytics.LogEven.GA_ACTION_TYPE_GameOver,"Roll Dice Button Clicked",1);

        //	TSUtil.LogEvent (AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_GameOver, "Game Completed", 1);

        currentMatchCount++;

        if (playerTurn == PlayerTurn.Player1 && Player1MaturityStack.GetComponent<Lane>().tokensList.Count == 15)
        {
            // TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_GameOver, "Game Win", 1);
            // if (isGameTwoPlayer == true)
            //     photonView.RPC("showGameOverPopUp", PhotonTargets.Others, true);
            // else updateScore();

            // //thesun
            // //MenuManager.Instance.PushMenu(GameManager.GameState.SuccessPopup);
            // showGameOverPopUp(true);


            playerScore += calculateScore();
            updateScore();
            showGameOverPopUp(true);


            return true;
        }
        else if (playerTurn == PlayerTurn.Player2 && Player2MaturityStack.GetComponent<Lane>().tokensList.Count == 15)
        {
            // TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_GameOver, "Game Lost", 1);
            // if (isGameTwoPlayer == true)
            //     photonView.RPC("showGameOverPopUp", PhotonTargets.Others, false);
            // else updateScore();

            // //thesun
            // // MenuManager.Instance.PushMenu(GameManager.GameState.FailurePopUp);
            // showGameOverPopUp(false);


            cpuScore += calculateScore();
            updateScore();
            showGameOverPopUp(false);

            return true;
        }
        else
            return false;
    }

    [PunRPC]
    public void showGameOverPopUp(bool success)
    {

        // if (success == true)
        //     MenuManager.Instance.PushMenu(GameManager.GameState.SuccessPopup);
        // else
        //     MenuManager.Instance.PushMenu(GameManager.GameState.FailurePopUp);

        //thesun

        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_GameOver, success ? "Game Won" : "Game Lost", 1);

        undoStack.Clear();

        if (playerScore >= matchCountToWin || cpuScore >= matchCountToWin)
        {
            if (playerGameMode == PlayerGameMode.PlayerVSCPU)
                StatisticsScreen.setResultStatistics(playerScore >= matchCountToWin, playerScore, difficulltyLevel);

            TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_GameOver, playerScore >= matchCountToWin ? "Match Won" : "Match Lost", 1);

            if (playerGameMode == PlayerGameMode.PlayerVSCPU)
                TSUtil.Instance.hasSaveGamePlayerVSCPU = false;
            else if (playerGameMode == PlayerGameMode.TwoLocalPlayer)
                TSUtil.Instance.hasSaveGame2LocalPlayer = false;

            TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
            "  ",
            (success == true ? "MATCH WON" : "YOU LOST"),
            (result) =>
            {
                if (result == "Return to Menu")
                {
                    MenuManager.Instance.PopMenuToState(GameManager.GameState.MainMenu);
                }
                else
                {
                    GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewMatch();
                }

                MoPubAdsManager.Instance.showFullScreen();
            },
            new List<string>() { "New Match", "Return to Menu" }), 0.2f);
        }
        else
        {
            SaveGameData saveGameData = makeSaveGameData();
            saveGameData.tokenMap = getDefaultTokenMap();
            string saveGameFileName = playerGameMode == PlayerGameMode.PlayerVSCPU ? kSave2Prefs1PlayerMode : kSave2Prefs2LocalPlayerMode;
            RSManager.Serialize<SaveGameData>(saveGameData, saveGameFileName, eSaveTarget.PLAYER_PREFS);

            TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
            "  ",
            (success == true ? "GAME WIN" : "GAME LOSE"),
            (result) =>
            {
                GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewGame();
                saveGame();

                MoPubAdsManager.Instance.showFullScreen();
            },
            new List<string>() { "New Game" }), 0.2f);
        }
    }


    /******************************** Hint Functions ***************************************/

    public void onHintButtonClicked()
    {
        if (playerTurn == PlayerTurn.Player1)
        {
            // Debug.Log("Player 1 hint");
            giveHint();
        }
        else
        {
            // Debug.Log("Player 2 hint");
            giveHint();
        }
    }
    public void giveHint()
    {
        GameObject singleToken = returnTokenOnASingleLane();
        if (singleToken != null)
        {
            int singleTokenLaneIndex = singleToken.GetComponent<Token>().laneNumber;

            onTokenClicked(singleToken);

            GameObject glowingLane = returnPossibleHintLane();
            // Debug.Log("glowing lane index " + glowingLane.GetComponent<Lane>().laneIndex);
            if (glowingLane != null)
                showHandAnimation(glowingLane, singleToken);

        }
    }
    public void showHandAnimation(GameObject desriedLane, GameObject singleToken)
    {
        Vector2 laneSize = desriedLane.gameObject.GetComponent<UITexture>().localSize;
        Vector2 tokenSize = singleToken.GetComponent<UITexture>().localSize;

        int multiplier = -1;

        //		if (desriedLane.GetComponent<Lane> ().laneIndex <= 12)
        //			multiplier = -1;

        Vector3 desiredLaneLocalPosition = new Vector3(desriedLane.gameObject.transform.localPosition.x, desriedLane.gameObject.transform.localPosition.y - (laneSize.y * 0.9f) / 2, desriedLane.gameObject.transform.localPosition.z);
        Vector3 desiredTokenLocalPosition = new Vector3(singleToken.gameObject.transform.localPosition.x, singleToken.gameObject.transform.localPosition.y - (tokenSize.y * multiplier), singleToken.gameObject.transform.localPosition.z);


        if (selectedToken.GetComponent<Token>().isOpponent == true)
            hintHandTexture.GetComponent<UITexture>().mainTexture = hintHandTexture.GetComponent<HandObj>().darkColoredToken;


        hintHandTexture.gameObject.SetActive(true);
        hintHandTexture.gameObject.transform.position = singleToken.gameObject.transform.position;
        //hintHandTexture.gameObject.transform.localPosition = desiredLaneLocalPosition;

        hintHandTexture.gameObject.GetComponent<TweenPosition>().to = desiredLaneLocalPosition;
        hintHandTexture.gameObject.GetComponent<TweenPosition>().from = desiredTokenLocalPosition;


        //		hintHandTexture.gameObject.transform.localPosition=new Vector3(singleToken.gameObject.transform.localPosition.x,singleToken.gameObject.transform.localPosition.y,singleToken.gameObject.transform.localPosition.z);
        //	hintHandTexture.gameObject.GetComponent<TweenPosition> ().from = desiredTokenLocalPosition ;
        //	hintHandTexture.gameObject.GetComponent<TweenPosition> ().to = desiredLaneLocalPosition;

    }

    public GameObject returnPossibleHintLane()
    {

        for (int i = 1; i < lanesList.Count; i++)
        {


            //GameObject lane=lanesList[i];
            GameObject lane = getLanewithIndexNo(i);
            // Debug.Log("on Lane Clicked");

            int laneIndex = lane.GetComponent<Lane>().laneIndex;
            //		bool isDestiantionLaneOpp = isDestinationLaneOpponents (laneIndex);

            if (selectedToken != null && isDiceRolled == true && isAPossibleLane(laneIndex))
            {

                selectedLane = lane;

                int selectedLaneIndex = selectedLane.GetComponent<Lane>().laneIndex;
                // Debug.Log("selected lane index" + selectedLaneIndex);

                GameObject currentLane = getLanewithIndexNo(selectedToken.GetComponent<Token>().laneNumber); //////////////////////////////  current Lane Number

                selectedLaneIndex = isAPossibleMove(selectedToken.GetComponent<Token>().laneNumber, selectedLaneIndex, false);
                if (selectedLaneIndex != -1)
                {
                    return selectedLane;

                }
            }

        }
        return null;
    }

    public GameObject returnTokenOnASingleLane()
    {
        if (playerTurn == PlayerTurn.Player1)
        {
            for (int i = 1; i < lanesList.Count; i++)
            {

                //GameObject currentLane = lanesList [i];
                GameObject currentLane = getLanewithIndexNo(i);

                if (currentLane.GetComponent<Lane>().tokensList.Count == 1 && ifLaneIsAPossibleMove(currentLane.GetComponent<Lane>().laneIndex) == true)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;

                    if ((playerTurn == PlayerTurn.Player1 && isTokenOpponent == false) || (playerTurn == PlayerTurn.Player2 && isTokenOpponent == true))
                    {

                        return currentLane.GetComponent<Lane>().tokensList[0];

                    }
                }
            }

            for (int i = 1; i < lanesList.Count; i++)
            {

                //GameObject currentLane = lanesList [i];
                GameObject currentLane = getLanewithIndexNo(i);
                int maxToken = currentLane.GetComponent<Lane>().tokensList.Count - 1;
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1 && ifLaneIsAPossibleMove(currentLane.GetComponent<Lane>().laneIndex) == true)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;

                    if ((playerTurn == PlayerTurn.Player1 && isTokenOpponent == false) || (playerTurn == PlayerTurn.Player2 && isTokenOpponent == true))
                    {

                        return currentLane.GetComponent<Lane>().tokensList[maxToken];

                    }
                }
            }
        }
        else
        {

            for (int i = lanesList.Count - 1; i > 0; i--)
            {
                GameObject currentLane = getLanewithIndexNo(i);

                if (currentLane.GetComponent<Lane>().tokensList.Count == 1 &&
                    ifLaneIsAPossibleMove(currentLane.GetComponent<Lane>().laneIndex) == true)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;

                    if (isTokenOpponent == true)
                    {
                        return currentLane.GetComponent<Lane>().tokensList[0];
                    }
                }
            }

            for (int i = lanesList.Count - 1; i > 0; i--)
            {
                GameObject currentLane = getLanewithIndexNo(i);
                int maxToken = currentLane.GetComponent<Lane>().tokensList.Count - 1;

                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1 &&
                    ifLaneIsAPossibleMove(currentLane.GetComponent<Lane>().laneIndex) == true)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == true)
                    {
                        return currentLane.GetComponent<Lane>().tokensList[maxToken];
                    }
                }
            }
        }
        return null;

    }

    //////////////////////////////////////////////////////       autoMovePlayer 

    void autoMovePlayer2(bool rollDiceAgain)
    {

        selectedToken = null;
        selectedLane = null;

        if (rollDiceAgain == true)
        {
            RollDiceBtnClicked();
            //thesun
            //Invoke("checkAutoPlayerPossibleMoves", 2.5f);
        }
        else Invoke("checkAutoPlayerPossibleMoves", 0.5f);
    }

    void checkAutoPlayerPossibleMoves()
    {
        //thesun
        if (isAPossbileAutoPlayerMaturityMove() == true)
        {
            return;
        }
        //thesun
        //try to kick single token of opponent
        if (player2deadStack.GetComponent<Lane>().tokensList.Count == 0)
        {
            List<Lane> player1LaneListWithSingleToken = new List<Lane>();
            List<Lane> player2LaneList = new List<Lane>();
            for (int i = 24; i > 0; i--)
            {
                GameObject currentLane = getLanewithIndexNo(i);
                int tokenCount = currentLane.GetComponent<Lane>().tokensList.Count;
                if (tokenCount >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;

                    if (isTokenOpponent == true)
                    {
                        player2LaneList.Add(currentLane.GetComponent<Lane>());
                    }
                    else
                    {
                        if (tokenCount == 1)
                            player1LaneListWithSingleToken.Add(currentLane.GetComponent<Lane>());
                    }
                }
            }
            if (player1LaneListWithSingleToken.Count > 0)
            {
                foreach (Lane player2Lane in player2LaneList)
                {
                    foreach (Lane player1Lane in player1LaneListWithSingleToken)
                    {
                        if (isAPossibleMove(player2Lane.laneIndex, player1Lane.laneIndex, false, true) != -1)
                        {
                            //Debug.Log("TheSun Kick:" + player2Lane.laneIndex + " " + player1Lane.laneIndex);
                            onTokenClicked(player2Lane.tokensList[0]);
                            onLaneClicked(player1Lane.gameObject);
                            return;
                        }
                    }
                }
            }
        }


        GameObject singleToken = returnTokenOnASingleLane();
        //	int singleTokenLaneIndex = singleToken.GetComponent<Token> ().laneNumber;
        if (singleToken != null)
        {
            onTokenClicked(singleToken);
            GameObject glowingLane = returnPossibleHintLane();

            if (glowingLane != null)
                onLaneClicked(glowingLane);
            else
            {
                // Debug.Log("No Possible Moves");
                showNoPossibleMoves();
            }
        }

        //thesun
        else
        {
            Debug.Log("No Possible Moves");
            showNoPossibleMoves();
        }
    }

    bool isAPossbileAutoPlayerMaturityMove()
    {
        if (ifAllTokensAreInLastQuater() == true)
        {
            GameObject token = returnAutoPlayerMaturityMoveToken();
            if (token == null)
                return false;
            bool isTokenOpponent = token.GetComponent<Token>().isOpponent;
            if (playerTurn == PlayerTurn.Player2 && isTokenOpponent == true)
            {
                onTokenClicked(token);
                if (playerTurn == PlayerTurn.Player2)
                    onStackClicked(Player2MaturityStack);
                else if (playerTurn == PlayerTurn.Player2)
                    onStackClicked(Player1MaturityStack);

                return true;
            }
            else if (playerTurn == PlayerTurn.Player1 && isTokenOpponent == false)
            {
                return true;
            }

        }
        return false;
    }

    GameObject returnAutoPlayerMaturityMoveToken()
    {
        if (playerTurn == PlayerTurn.Player1)
        {
            for (int i = 1; i < lanesList.Count; i++)
            {
                //GameObject currentLane = lanesList [i];
                GameObject currentLane = getLanewithIndexNo(i);
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == false)
                    {
                        return currentLane.GetComponent<Lane>().tokensList[0];
                    }
                }
            }
        }
        else
        {
            //thesun
            int maxDice = Math.Max(dice1Value, dice2Value);
            int minDice = Math.Min(dice1Value, dice2Value);
            GameObject currentLane = getLanewithIndexNo(maxDice);
            if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
            {
                bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                if (isTokenOpponent == true)
                    return currentLane.GetComponent<Lane>().tokensList[0];
            }
            else if (minDice > 0)
            {
                currentLane = getLanewithIndexNo(minDice);
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == true)
                        return currentLane.GetComponent<Lane>().tokensList[0];
                }
            }

            for (int i = maxDice; i <= 6; i++)
            {
                currentLane = getLanewithIndexNo(i);
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == true)
                        return null;
                }
            }

            for (int i = maxDice - 1; i > 0; i--)
            // for (int i = 6; i > 0; i--)            
            {
                //GameObject currentLane = lanesList [i];
                currentLane = getLanewithIndexNo(i);
                if (currentLane.GetComponent<Lane>().tokensList.Count >= 1)
                {
                    bool isTokenOpponent = currentLane.GetComponent<Lane>().tokensList[0].GetComponent<Token>().isOpponent;
                    if (isTokenOpponent == true)
                        return currentLane.GetComponent<Lane>().tokensList[0];
                }
            }
        }
        return null;
    }


    ////////////////////////////////////////////////

    void checkTutorial()
    {

        if (isTutorialOn == true)
        {
            MenuManager.Instance.PushMenu(GameManager.GameState.TutorialPopUp);
            isTutorialOn = false;
        }


    }

    void showHandAnimationAt(GameObject gameObject)
    {

        Vector2 handSize = hintHandTexture.GetComponent<UITexture>().localSize;
        Vector3 desiredPosition = new Vector3(hintHandTexture.transform.localPosition.x, hintHandTexture.transform.localPosition.y - handSize.y / 2 + handSize.y / 1.85f, hintHandTexture.transform.localPosition.z);

        hintHandTexture.gameObject.SetActive(true);
        hintHandTexture.gameObject.transform.localPosition = desiredPosition;

    }



    /// /////////////////////////////////////////////

    //thesun
    void moveGameObjectToFront(GameObject gameObject)
    {
        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    private Vector3 mapPosBoardBame2BoardGameOverlay(Vector3 pos)
    {
        Vector3 res = boardGameOverlay.transform.InverseTransformPoint(boardGame.transform.TransformPoint(pos));
        return res;
    }

    private GameObject getActiveLayout()
    {
        if (iPadLandscapeLayout.GetComponent<UIWidget>().alpha == 1)
            return iPadLandscapeLayout;
        if (iPhoneLandscapeLayout.GetComponent<UIWidget>().alpha == 1)
            return iPhoneLandscapeLayout;
        if (iPhonePortraitLayout.GetComponent<UIWidget>().alpha == 1)
            return iPhonePortraitLayout;
        return iPadLandscapeLayout;
    }

    private void doMoveToken(int fromLaneNumber, int toLaneNumber)
    {
        GameObject fromLane = getLanewithIndexNo(fromLaneNumber);
        GameObject toLane = getLanewithIndexNo(toLaneNumber);

        isDiceRolled = true;
        onTokenClicked(fromLane.GetComponent<Lane>().tokensList[0]);
        onLaneClicked(toLane);
    }

    public void onUndoBtnClicked()
    {
        if (playerTurn == PlayerTurn.Player2 && playerGameMode == PlayerGameMode.PlayerVSCPU)
            return;
        if (_isLerping)
            return;
        if (_isUndoing)
            return;
        doUndoGame();
    }

    private void doUndoGame()
    {
        if (undoStack.Count == 0)
        {
            playerTurn = PlayerTurn.None;
            isDiceRolled = false;
            rollDiceButton.SetActive(true);
            updatePlayerTurnLabel(true);
            hideAllDices();
            return;
        }


        UndoData undoData = undoStack.Peek();
        if (playerTurn == PlayerTurn.Player1 ||
            (playerTurn == PlayerTurn.Player2 && undoData.isBeginRoll) ||
            (undoData.playerTurn == PlayerTurn.Player1 && playerGameMode == PlayerGameMode.TwoLocalPlayer))
        {
            if (rollDiceButton.GetActive() == true)
            {
                rollDiceButton.SetActive(false);
                isDiceRolled = true;
            }
            else if (undoData.playerTurn == PlayerTurn.Player2 ||
                    undoData.isBeginRoll)
            {
                if (undoData.isBeginRoll)
                {
                    playerTurn = undoData.playerTurn;
                    updatePlayerTurnLabel(true);
                    undoStack.Pop();
                }


                removeGlowEffects();
                if (selectedToken != null)
                {
                    selectedToken.GetComponent<Token>().ResetTexture();
                    selectedToken = null;
                }

                hideAllDices();
                rollDiceButton.SetActive(true);
                isDiceRolled = false;

                return;
            }
        }


        undoStack.Pop();

        GameObject srcLane = null, desLane = null;
        if (undoData.toLaneNumber == 26)
            srcLane = Player1MaturityStack;
        else if (undoData.toLaneNumber == 27)
            srcLane = Player2MaturityStack;
        else
            srcLane = getLanewithIndexNo(undoData.toLaneNumber);

        if (undoData.fromeLaneNumber == 26)
            desLane = Player1MaturityStack;
        else if (undoData.fromeLaneNumber == 27)
            desLane = Player2MaturityStack;
        else
            desLane = getLanewithIndexNo(undoData.fromeLaneNumber);

        selectedLane = desLane;
        selectedToken = srcLane.GetComponent<Lane>().tokensList[srcLane.GetComponent<Lane>().tokensList.Count - 1];

        if (undoData.isKillTokenMove)
            selectedToken = srcLane.GetComponent<Lane>().tokensList[0];

        selectedToken.GetComponent<Token>().laneNumber = selectedLane.GetComponent<Lane>().laneIndex;
        selectedLane.GetComponent<Lane>().tokensList.Add(selectedToken);
        srcLane.GetComponent<Lane>().tokensList.Remove(selectedToken);

        playerTurn = undoData.playerTurn;
        rolledDiceTurnsLeft = undoData.rolledDiceTurnsLeft;
        dice1Value = undoData.dice1Value;
        dice2Value = undoData.dice2Value;

        removeGlowEffects();
        selectedToken.GetComponent<UITexture>().mainTexture = selectedToken.GetComponent<Token>().glowedTexture;
        selectedToken.GetComponent<Token>().isMatured = false;
        moveGameObjectToFront(selectedToken);
        updatePlayerTurnLabel(true);


        dice1.gameObject.SetActive(dice1Value > 0);
        dice2.gameObject.SetActive(dice2Value > 0);
        dice3.gameObject.SetActive(rolledDiceTurnsLeft >= 3);
        dice4.gameObject.SetActive(rolledDiceTurnsLeft >= 4);

        if (dice1Value > 0)
            dice1.gameObject.GetComponent<UI2DSprite>().sprite2D = dice3.gameObject.GetComponent<UI2DSpriteAnimation>().frames[dice1Value - 1];
        if (dice2Value > 0)
            dice2.gameObject.GetComponent<UI2DSprite>().sprite2D = dice3.gameObject.GetComponent<UI2DSpriteAnimation>().frames[dice2Value - 1];

        if (dice1Value > 0 && dice1Value == dice2Value)
        {
            dice3.gameObject.GetComponent<UI2DSprite>().sprite2D = dice1.gameObject.GetComponent<UI2DSprite>().sprite2D;
            dice4.gameObject.GetComponent<UI2DSprite>().sprite2D = dice1.gameObject.GetComponent<UI2DSprite>().sprite2D;
        }


        isDiceRolled = true;
        _isUndoing = true;
        _timeStartedLerping = Time.time;

    }

    private GameObject getUndoBtn()
    {
        return getActiveLayout().transform.Find("UndoBtn").gameObject;
    }

    private void saveDataForUndo(GameObject fromLane, GameObject toLane, bool isKillTokenMove = false)
    {
        int fromLaneNumber, toLaneNumber;
        if (fromLane == Player1MaturityStack)
            fromLaneNumber = 26;
        else if (fromLane == Player2MaturityStack)
            fromLaneNumber = 27;
        else
            fromLaneNumber = fromLane.GetComponent<Lane>().laneIndex;

        if (toLane == Player1MaturityStack)
            toLaneNumber = 26;
        else if (toLane == Player2MaturityStack)
            toLaneNumber = 27;
        else
            toLaneNumber = toLane.GetComponent<Lane>().laneIndex;

        saveDataForUndo(fromLaneNumber, toLaneNumber, isKillTokenMove);
    }


    private void saveDataForUndo(int fromLaneNumber, int toLaneNumber, bool isKillTokenMove = false)
    {
        undoStack.Push(makeDataForUndo(fromLaneNumber, toLaneNumber, isKillTokenMove));
    }

    private void saveDataForUndoAtBeginRoll()
    {
        UndoData undoData = makeDataForUndo(0, 0);
        undoData.isBeginRoll = true;
        undoStack.Push(undoData);
    }

    private UndoData makeDataForUndo(int fromLaneNumber, int toLaneNumber, bool isKillTokenMove = false)
    {
        UndoData undoData = new UndoData();
        undoData.rolledDiceTurnsLeft = tempUndoData.rolledDiceTurnsLeft;
        undoData.playerTurn = playerTurn;
        undoData.dice1Value = tempUndoData.dice1Value;
        undoData.dice2Value = tempUndoData.dice2Value;
        undoData.isKillTokenMove = isKillTokenMove;
        //undoData.tokenMap = getCurrentTokenMap();
        //undoData.tokenPos = new Vector3[30];
        // foreach (GameObject currToken in tokens)
        // {

        // }

        undoData.fromeLaneNumber = fromLaneNumber;
        undoData.toLaneNumber = toLaneNumber;
        return undoData;
    }

    private void updateTempUndoData()
    {
        tempUndoData.rolledDiceTurnsLeft = rolledDiceTurnsLeft;
        tempUndoData.dice1Value = dice1Value;
        tempUndoData.dice2Value = dice2Value;

    }

    private int[] getCurrentTokenMap()
    {
        int[] tokenMap = new int[32];
        int i = 1, j = 16;
        foreach (GameObject currToken in tokens)
        {
            Token token = currToken.GetComponent<Token>();
            if (token.isOpponent)
            {
                tokenMap[i] = token.laneNumber;
                if (token.isMatured)
                    tokenMap[i] = 27;
                i++;
            }
            else
            {
                tokenMap[j] = token.laneNumber;
                if (token.isMatured)
                    tokenMap[j] = 26;
                j++;
            }
        }
        return tokenMap;
    }
}


