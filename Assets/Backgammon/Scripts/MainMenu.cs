﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : BaseMenu
{



    public override void Awake()
    {
        base.Awake();

        if (TSUtil.isIPad())
        {
            transform.Find("SimpleLayoutIPhone").gameObject.SetActive(false);
            transform.Find("SimpleLayoutIPad").gameObject.SetActive(true);
        }
        else{
            transform.Find("SimpleLayoutIPhone").gameObject.SetActive(true);
            transform.Find("SimpleLayoutIPad").gameObject.SetActive(false);
        }

    }
    public void PlayBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Play Button Clicked", 1);
        MenuManager.Instance.PushMenu(GameManager.GameState.LevelSelection);
        
    }

    public void FeedbackBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        MenuManager.Instance.PushMenu(GameManager.GameState.FeedbackPopUp);
    }


    public void StartNewGameBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Start New Game Button Clicked", 1);
        //MenuManager.Instance.PushMenu(GameManager.GameState.NewGameSelectionPanel);

        if (TSUtil.Instance.hasSaveGamePlayerVSCPU)
        {
            TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
            "  ",
            "Do you want to resume playing?",
            (result) =>
            {
                if (result == "Yes")
                {
                    GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadGame1Player();                    
                }
                else
                    GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewMatch1Player();
                MenuManager.Instance.PushMenu(GameManager.GameState.GamePlay);            
            },
            new List<string>() { "Yes", "No" }), 0.1f);
        }
        else
        {
            MenuManager.Instance.PushMenu(GameManager.GameState.GamePlay);
            GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewMatch1Player();            
        }
    }

    public void StartNewGameTwoPlayerBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "New Game 2 Players Button Clicked", 1);
        //MenuManager.Instance.PushMenu(GameManager.GameState.NewGameSelectionPanel);

        if (TSUtil.Instance.hasSaveGame2LocalPlayer)
        {
            TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
            "  ",
            "Do you want to resume playing?",
            (result) =>
            {
                if (result == "Yes")
                {
                    GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadGame2LocalPlayer();                    
                }
                else
                    GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewMatch2LocalPlayer();
                MenuManager.Instance.PushMenu(GameManager.GameState.GamePlay);            
            },
            new List<string>() { "Yes", "No" }), 0.1f);
        }
        else
        {
            MenuManager.Instance.PushMenu(GameManager.GameState.GamePlay);
            GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewMatch2LocalPlayer();            
        }
    }


    public void SettingsBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Settings Button Clicked", 1);
        MenuManager.Instance.PushMenu(GameManager.GameState.SettingPanel);
    }

    public void HelpBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Helps Button Clicked", 1);
        MenuManager.Instance.PushMenu (GameManager.GameState.HelpScreen);
    }

    public void StatisticsBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Statistics Button Clicked", 1);
        MenuManager.Instance.PushMenu (GameManager.GameState.StatisticsScreen);
    }
}
