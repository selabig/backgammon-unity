﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GameManager : Singleton<GameManager> {

    public PhotonView gamePlayPhotonView;

    public NuttySoundManager  nuttySoundManager;
	#region Game States

	// An enum with all the possible states of the game. NOTE: depending on the game the game states may change. Please add at the end of the screen.
	public enum GameState { MainMenu, LevelSelection, GamePlay, FeedbackPopUp, ExitPopUp, 
	GameEndPopUp, FailurePopUp, SuccessPopup, StatsPopup, HelpPopUp, TutorialPopUp, WaitingMultiPlayer, 
	SettingPanel, NewGameSelectionPanel, HelpScreen, StatisticsScreen, NullScreen};

	// the current state of the game
	public GameState initialState = GameState.MainMenu;

	// the delegate for state change
	public delegate void OnGameStateChange(GameState g);
	public event OnGameStateChange GameStateChanged;

	public bool isGameMultiplayer=false;

    public MenuHandler menuHandler;

	public RandomMatchmaker randomMatchmaker;
	public PhotonView photonView;
	public GameLogic gameLogic;
	public ShowStatusWhenConnecting showStatus;

	public GameObject gamePlayObj;

	void HandleGameStateChanged (GameState g)
	{
		
	}
	
	public void ChangeGameStateTo (GameState g)
	{
		GameStateChanged (g);
	}

	#endregion Game States

	// The Player
	public NuttyPlayer player;

    #region Navigation Manager

	public NavigationManager navigationManager = new NavigationManager();

	#endregion Navigation Manager

	#region Pause

	public bool isPaused = false;


	public void PauseGame()
	{
		isPaused = true;
		Time.timeScale = 0.0f;
	}

	public void UnPauseGame()
	{
		isPaused = false;
		Time.timeScale = 1.0f;
	}

	#endregion Pause

	#region Mono Life Cycle

	void Awake() 
	{

		if(GameObject.FindGameObjectsWithTag("GameManager").Length > 1) 
		{
			Destroy(gameObject);
		}
		else
		{
			DontDestroyOnLoad(gameObject);
		}

		GameManager.Instance.GameStateChanged += HandleGameStateChanged;
        GameManager.Instance.GameStateChanged += nuttySoundManager.OnGameStateChangedSetMusic;			
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void OnDestroy () 
	{
		if(GameObject.FindGameObjectsWithTag("GameManager").Length < 1)
		{
			applicationIsQuitting = true;
		}
	}


	public void activateMultiplayerComponents(){

		randomMatchmaker.enabled = true;
		photonView.enabled = true;
		gameLogic.enabled = true;
		showStatus.enabled	 = true;


	}
	public void deActivateMultiplayerComponents(){

		randomMatchmaker.enabled = false;
		photonView.enabled = false;
		gameLogic.enabled = false;
		showStatus.enabled = false;

		
	}

	public void skipTurnOnRPc(){

	
		if(gamePlayObj != null)
			gamePlayObj.GetComponent<GamePlay>().skipOtherPlayersTurn();

	}
	#endregion Mono Life Cycle
}
