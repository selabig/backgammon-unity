﻿using UnityEngine;
using System.Collections;

public class FeedBack : BaseMenu {

	public GameObject basePopUp;

	public void FeedbackYesButtonClicked()
	{	
		Application.OpenURL ("");
	}

	public void FeedbackNoButtonClicked()
	{	
		FeedbackCrossButtonClicked ();
	}
	
	public void FeedbackCrossButtonClicked()
	{	
		basePopUp.GetComponent<TweenScale> ().AddOnFinished (HideFeedbackPopup);
		basePopUp.GetComponent<TweenScale>().PlayReverse ();
	}

	void HideFeedbackPopup()
	{
		GameManager.Instance.isPaused = false;
		MenuManager.Instance.PopMenu ();
	}
}
