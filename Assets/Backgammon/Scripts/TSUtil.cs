using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using System;
using System.Runtime.InteropServices;
using Facebook.Unity;

namespace System
{
    public static class MSSystemExtenstions
    {
        private static Random rng = new Random();
        public static void Shuffle<T>(this T[] array)
        {
            rng = new Random();
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n);
                n--;
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
    }
}

public class TSUtil : Singleton<TSUtil>
{
	[DllImport("__Internal")]
	private static extern void sendMailFeedback();

	[DllImport("__Internal")]
	private static extern void shareMessage(string message);
    
    public enum DifficulltyLevel { Easy, Medium, Hard };
    public enum BoardViewMode { Portrait, Landscape };

    //public enum MatchCountToWin { Match1, Match3, Match5, Match7, Match11, Match15 };
    void Awake()
    {
        try
        {
            if (GameObject.FindGameObjectsWithTag("TSUtil").Length > 1)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
            }
        }
        catch
        {

        }
        finally
        {
            DontDestroyOnLoad(gameObject);
        }

        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            //Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnDestroy()
    {
        try
        {
            if (GameObject.FindGameObjectsWithTag("TSUtil").Length < 1)
            {
                applicationIsQuitting = true;
            }

        }
        catch
        {

        }
        finally
        {
            applicationIsQuitting = true;
        }
    }

    public void moveGameObjectToXPos()
    {
        TweenPosition.Begin(this.gameObject, 0.2f, new Vector3(UIButton.current.transform.localPosition.x, this.transform.localPosition.y, this.transform.localPosition.z));
    }

    public void testLog(int x)
    {

    }

    public bool SettingSoundEnable
    {
        get
        {
            // return NuttySoundManager.Instance.SoundEnabled;

            if (!PlayerPrefs.HasKey("GAME_SOUND"))
            {
                PlayerPrefs.SetString("GAME_SOUND", "True");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("GAME_SOUND"));
        }
        set
        {
            // NuttySoundManager.Instance.SoundEnabled = value;
            PlayerPrefs.SetString("GAME_SOUND", value.ToString());
        }
    }

    public bool SettingShowPossibleMove
    {
        get
        {
            if (!PlayerPrefs.HasKey("SHOW_POSSIBLE_MOVE"))
            {
                PlayerPrefs.SetString("SHOW_POSSIBLE_MOVE", "True");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("SHOW_POSSIBLE_MOVE"));
        }
        set
        {
            PlayerPrefs.SetString("SHOW_POSSIBLE_MOVE", value.ToString());
        }
    }

    public bool SettingOneTouchMove
    {
        get
        {
            if (!PlayerPrefs.HasKey("ONE_TOUCH_MOVE"))
            {
                PlayerPrefs.SetString("ONE_TOUCH_MOVE", "False");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("ONE_TOUCH_MOVE"));
        }
        set
        {
            PlayerPrefs.SetString("ONE_TOUCH_MOVE", value.ToString());
        }
    }

    public bool SettingDoubleCube
    {
        get
        {
            if (!PlayerPrefs.HasKey("DOUBLE_CUBE"))
            {
                PlayerPrefs.SetString("DOUBLE_CUBE", "True");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("DOUBLE_CUBE"));
        }
        set
        {
            PlayerPrefs.SetString("DOUBLE_CUBE", value.ToString());
        }
    }


    public bool SettingInGameNotification
    {
        get
        {
            if (!PlayerPrefs.HasKey("IN_GAME_NOTIFICATION"))
            {
                PlayerPrefs.SetString("IN_GAME_NOTIFICATION", "True");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("IN_GAME_NOTIFICATION"));
        }
        set
        {
            PlayerPrefs.SetString("IN_GAME_NOTIFICATION", value.ToString());
        }
    }

    public bool SettingColorCheckers
    {
        get
        {
            if (!PlayerPrefs.HasKey("COLOR_CHECKERS"))
            {
                PlayerPrefs.SetString("COLOR_CHECKERS", "True");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("COLOR_CHECKERS"));
        }
        set
        {
            PlayerPrefs.SetString("COLOR_CHECKERS", value.ToString());
        }
    }


    public int SettingBoardView
    {
        //0: portrait 1:landscape 
        get
        {
            if (!PlayerPrefs.HasKey("BOARD_VIEW"))
            {
                PlayerPrefs.SetInt("BOARD_VIEW", 1);
            }
            return PlayerPrefs.GetInt("BOARD_VIEW");
        }
        set
        {
            PlayerPrefs.SetInt("BOARD_VIEW", value);
        }
    }

    public int SettingDifficulty
    {
        get
        {
            if (!PlayerPrefs.HasKey("DIFFICULTY"))
            {
                PlayerPrefs.SetInt("DIFFICULTY", 0);
            }
            return PlayerPrefs.GetInt("DIFFICULTY");
        }
        set
        {
            PlayerPrefs.SetInt("DIFFICULTY", value);
        }
    }

    public bool SettingShowPipCount
    {
        get
        {
            if (!PlayerPrefs.HasKey("SHOW_PIP_COUNT"))
            {
                PlayerPrefs.SetString("SHOW_PIP_COUNT", "True");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("SHOW_PIP_COUNT"));
        }
        set
        {
            PlayerPrefs.SetString("SHOW_PIP_COUNT", value.ToString());
        }
    }


    public bool hasSaveGamePlayerVSCPU
    {
        get
        {
            if (!PlayerPrefs.HasKey("HAS_SAVE_GAME_PLAYER_VS_CPU"))
            {
                PlayerPrefs.SetString("HAS_SAVE_GAME_PLAYER_VS_CPU", "False");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("HAS_SAVE_GAME_PLAYER_VS_CPU"));
        }
        set
        {
            PlayerPrefs.SetString("HAS_SAVE_GAME_PLAYER_VS_CPU", value.ToString());
        }
    }

    public bool hasSaveGame2LocalPlayer
    {
        get
        {
            if (!PlayerPrefs.HasKey("HAS_SAVE_GAME_2_LOCAL_PLAYER"))
            {
                PlayerPrefs.SetString("HAS_SAVE_GAME_2_LOCAL_PLAYER", "False");
            }
            return Convert.ToBoolean(PlayerPrefs.GetString("HAS_SAVE_GAME_2_LOCAL_PLAYER"));
        }
        set
        {
            PlayerPrefs.SetString("HAS_SAVE_GAME_2_LOCAL_PLAYER", value.ToString());
        }
    }


    public int SettingMatchToWin
    {
        get
        {
            if (!PlayerPrefs.HasKey("MATCH_TO_WIN"))
            {
                PlayerPrefs.SetInt("MATCH_TO_WIN", 3);
            }
            return PlayerPrefs.GetInt("MATCH_TO_WIN");
        }
        set
        {
            PlayerPrefs.SetInt("MATCH_TO_WIN", value);
        }
    }

    public int PlayerSocre
    {
        get
        {
            if (!PlayerPrefs.HasKey("PLAYER_SCORE"))
            {
                PlayerPrefs.SetInt("PLAYER_SCORE", 0);
            }
            return PlayerPrefs.GetInt("PLAYER_SCORE");
        }
        set
        {
            PlayerPrefs.SetInt("PLAYER_SCORE", value);
        }
    }


    public int CPUSocre
    {
        get
        {
            if (!PlayerPrefs.HasKey("CPU_SCORE"))
            {
                PlayerPrefs.SetInt("CPU_SCORE", 0);
            }
            return PlayerPrefs.GetInt("CPU_SCORE");
        }
        set
        {
            PlayerPrefs.SetInt("CPU_SCORE", value);
        }
    }
    public static void LogEvent(string category, string action, string label, int value = 0)
    {
        //UniversalAnalytics.LogEvent (AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY,AnalyticConstants.GA_ACTION_TYPE_BUTTON,"New Game Button Clicked",1);

        var parameters = new Dictionary<string, object>();
        parameters[Facebook.Unity.AppEventParameterName.ContentType] = label;
        FB.LogAppEvent(label, 0, parameters);
    }

    static public bool isIPad()
    {
        // return true;

        bool result = false;
        if (SystemInfo.deviceModel.Contains("iPad"))
            result = true;
        else if (NGUITools.screenSize.x == 1024 && NGUITools.screenSize.y == 768 ||
                NGUITools.screenSize.x == 2048 && NGUITools.screenSize.y == 1536)
            result = true;

        return result;

    }
    static public bool isIPhone()
    {
        // return true;

        bool result = false;
        if (SystemInfo.deviceModel.Contains("iPhone"))
            result = true;
        else if (NGUITools.screenSize.x == 640 && NGUITools.screenSize.y == 1136 ||
                NGUITools.screenSize.x == 750 && NGUITools.screenSize.y == 1334 ||
                NGUITools.screenSize.x == 1242 && NGUITools.screenSize.y == 2208)
            result = true;

        return result;

    }

    static public Vector3 mapPosSrc2Des(GameObject goSrc, GameObject goDes)
    {
        Vector3 res = goDes.transform.InverseTransformPoint(goSrc.transform.parent.transform.TransformPoint(goSrc.transform.localPosition));
        return res;
    }

    static public Vector3 movPosSrc2Des(GameObject goSrc, GameObject goDes)
    {
        Vector3 res = goSrc.transform.parent.transform.InverseTransformPoint(goDes.transform.parent.transform.TransformPoint(goDes.transform.localPosition));
        goSrc.transform.localPosition = res;
        return res;
    }


    /// <summary>
    /// Like MonoBehavior.Invoke("FunctionName", 2f); but can include params. Usage:
    /// Utils.RunLater( ()=> FunctionName(true, Vector.one, "or whatever parameters you want"), 2f);
    /// </summary>
    public static void RunLater(System.Action method, float waitSeconds)
    {
        if (waitSeconds < 0 || method == null)
        {
            return;
        }
        TSUtil.Instance.StartCoroutine(RunLaterCoroutine(method, waitSeconds));
    }
    public static IEnumerator RunLaterCoroutine(System.Action method, float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        method();
    }

    /// <param name="email"> myMail@something.com</param>
    /// <param name="subject">my subject</param>
    /// <param name="body">my body</param>
    public static void SendMailCrossPlatform(string email, string subject, string body)
    {
        if( Application.platform == RuntimePlatform.IPhonePlayer )
        {
            sendMailFeedback();
            return;
        }
        


        subject = MyEscapeURL(subject);
        body = MyEscapeURL(body);
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    public static string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public static void shareMessageCrossPlatform(string message)
    {
        if( Application.platform == RuntimePlatform.IPhonePlayer )
        {
            shareMessage(message);
        }
        

    }
    
}

