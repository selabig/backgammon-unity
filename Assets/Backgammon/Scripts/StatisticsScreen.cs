﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using VoxelBusters.RuntimeSerialization;
public class StatisticsScreen : BaseMenu
{


    [Serializable, RuntimeSerializable]
    public class StatisticsInfo
    {
        public int gameWon = 0;
        public int gameLost = 0;
        public int highScore = 0;
    }

    static private StatisticsInfo easyStatisticsInfo = new StatisticsInfo();
    static private StatisticsInfo mediumStatisticsInfo = new StatisticsInfo();
    static private StatisticsInfo hardStatisticsInfo = new StatisticsInfo();


    public UILabel gamesPlayedNumber;
    public UILabel gamesWonNumber;
    public UILabel gamesLostNumber;
    public UILabel gamesWinVSLostNumber;
    public UILabel highScoreNumber;

    private string currentDifficulty;

    public override void Awake()
    {
        base.Awake();


        if (!PlayerPrefs.HasKey("SET_DEFAULT_STATISTICS"))
        {
            PlayerPrefs.SetInt("SET_DEFAULT_STATISTICS", 1);
            saveStatisticsData();
        }
        else
        {
            loadStatisticsData();
        }

        if (TSUtil.isIPad())
        {

        }
        else
        {
            this.transform.Find("PanelMainSession").transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        }

        currentDifficulty = "EASY";
        refreshData();
    }
    void OnEnable()
    {
        refreshData();
    }

    public static void saveStatisticsData()
    {
        RSManager.Serialize<StatisticsInfo>(easyStatisticsInfo, "easyStatisticInfo-data-prefs", eSaveTarget.PLAYER_PREFS);
        RSManager.Serialize<StatisticsInfo>(mediumStatisticsInfo, "mediumStatisticsInfo-data-prefs", eSaveTarget.PLAYER_PREFS);
        RSManager.Serialize<StatisticsInfo>(hardStatisticsInfo, "hardStatisticsInfo-data-prefs", eSaveTarget.PLAYER_PREFS);
    }

    public static void loadStatisticsData()
    {
        easyStatisticsInfo = RSManager.Deserialize<StatisticsInfo>("easyStatisticInfo-data-prefs");
        mediumStatisticsInfo = RSManager.Deserialize<StatisticsInfo>("mediumStatisticsInfo-data-prefs");
        hardStatisticsInfo = RSManager.Deserialize<StatisticsInfo>("hardStatisticsInfo-data-prefs");
    }

    public void DoneBtnClicked()
    {
        base.HideThis();
    }

    public void ResetBtnClicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Reset Stat Button Clicked", 1);

        TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
           "  ",
           "Do you want to reset?",
           (result) =>
           {
               if (result == "Yes")
               {
                   easyStatisticsInfo = new StatisticsInfo();
                   mediumStatisticsInfo = new StatisticsInfo();
                   hardStatisticsInfo = new StatisticsInfo();
                   saveStatisticsData();
                   refreshData();
               }
           },
           new List<string>() { "Yes", "No" }), 0.1f);

    }
    public void DifficultSegmentBtnClicked()
    {
        currentDifficulty = UIButton.current.transform.Find("Label").GetComponent<UILabel>().text;
        refreshData();
    }

    private void refreshData()
    {
        StatisticsInfo tempInfo = easyStatisticsInfo;
        if (currentDifficulty == "MEDIUM")
        {
            tempInfo = mediumStatisticsInfo;
        }
        else if (currentDifficulty == "HARD")
        {
            tempInfo = hardStatisticsInfo;
        }
        int gamePlayed = tempInfo.gameWon + tempInfo.gameLost;
        gamesPlayedNumber.text = gamePlayed + "";
        gamesWonNumber.text = tempInfo.gameWon + "";
        gamesLostNumber.text = tempInfo.gameLost + "";
        gamesWinVSLostNumber.text = (gamePlayed == 0 ? 0 : (tempInfo.gameWon / gamePlayed) * 100) + "%";
        highScoreNumber.text = tempInfo.highScore + "";
    }

    static public void setResultStatistics(bool isWin, int highScore, TSUtil.DifficulltyLevel difficulltyLevel)
    {
        StatisticsInfo tempInfo = easyStatisticsInfo;
        if (difficulltyLevel == TSUtil.DifficulltyLevel.Medium)
        {
            tempInfo = mediumStatisticsInfo;
        }
        else if (difficulltyLevel ==  TSUtil.DifficulltyLevel.Hard)
        {
            tempInfo = hardStatisticsInfo;
        }
        if (isWin)
        {
            tempInfo.gameWon++;
            if (tempInfo.highScore < highScore)
            {
                tempInfo.highScore = highScore;
            }
        }
        else
            tempInfo.gameLost++;
        saveStatisticsData();
    }
}

