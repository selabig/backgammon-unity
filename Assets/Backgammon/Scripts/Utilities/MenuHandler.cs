﻿using UnityEngine;
using System.Collections;

public class MenuHandler : MonoBehaviour {

    public GameObject[] childElements;
    private ArrayList initialPos;
    private ArrayList initialRotation;
    private ArrayList initialScale;
    private bool processingMenus = false;
    public bool playForward = false;

    public Texture[] soundTextures; 

	
	void Start () {
        initialPos = new ArrayList();
        initialRotation = new ArrayList();
        initialScale = new ArrayList();
	    foreach(GameObject t in childElements){
           initialPos.Add( t.transform.localPosition);
           initialRotation.Add(t.transform.rotation);
           initialScale.Add(t.transform.localScale);

           t.transform.localScale = new Vector3(0f,0f,0f);
           t.transform.localPosition = transform.localPosition;

           if (t.name == "menu (1)")
           {
               GameObject sndBtn = t;
               EventDelegate.Set(sndBtn.GetComponent<UIButton>().onClick, delegate()
               {
                   PushExitPanel();
               });
           }

           if (t.name == "menu (3)") {
               GameObject sndBtn = t;
               SetVolumeState(sndBtn.GetComponent<UITexture>(), false);
               EventDelegate.Set(sndBtn.GetComponent<UIButton>().onClick, delegate()
               {
                   SetVolumeState(sndBtn.GetComponent<UITexture>(), true);
               });
           }
           if (t.name == "menu (6)")
           {
               GameObject sndBtn = t;
               EventDelegate.Set(sndBtn.GetComponent<UIButton>().onClick, delegate()
               {
                   PushHelpPanel();
               });
           }


           if (t.name == "menu (2)")
           {
               GameObject sndBtn = t;
               EventDelegate.Set(sndBtn.GetComponent<UIButton>().onClick, delegate()
               {
                   PushStatsPanel();
               });
           }
        }
	}


    private void PushExitPanel()
    {
        if (MenuManager.Instance.NavigationStackPeek() != GameManager.GameState.ExitPopUp)
        {
            MenuManager.Instance.PushMenu(GameManager.GameState.ExitPopUp);
			GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        }
    }

    private void PushHelpPanel() {
        if(MenuManager.Instance.NavigationStackPeek() !=GameManager.GameState.HelpPopUp){
            MenuManager.Instance.PushMenu(GameManager.GameState.HelpPopUp);
			GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        }
    }

    private void PushStatsPanel()
    {
        if (MenuManager.Instance.NavigationStackPeek() != GameManager.GameState.StatsPopup)
        {
            MenuManager.Instance.PushMenu(GameManager.GameState.StatsPopup);
			GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        }
    }


    public void CloseMenu() {
        if (processingMenus == false)
        {
            if (playForward == true) {
				GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
                playForward = false;
                for (int i = 0; i < childElements.Length; i++)
                {
                    if ((MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.GamePlay || MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.WaitingMultiPlayer) && i == 0)
                    {
                        ProcessMenuBackward(i, false);
                    }
                    else if ((MenuManager.Instance.NavigationStackPeek() != GameManager.GameState.GamePlay || MenuManager.Instance.NavigationStackPeek() != GameManager.GameState.WaitingMultiPlayer) && i == 0)
                    {
                        childElements[i].transform.localScale = new Vector3(0f, 0f, 0f);
                        childElements[i].transform.localPosition = transform.localPosition;
                    }

                    if (i > 0)
                    {
                        if (MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.GamePlay || MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.WaitingMultiPlayer)
                        {
                            ProcessMenuBackward(i, false);
                        }
                        else
                        {
                            ProcessMenuBackward(i, true);
                        }
                    }
                }

            }
        }
    }

    public void OnMainMenuClick() {
        if (processingMenus== false) {
			GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
            playForward = !playForward;
            processingMenus = true;
            RemoveTweens();
            if (playForward)
            {
                for (int i = 0; i < childElements.Length; i++)
                {
                    if ((MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.GamePlay || MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.WaitingMultiPlayer) && i == 0)
                    {
                            ProcessMenuForward(i, false);
                    }
                    else if ((MenuManager.Instance.NavigationStackPeek() != GameManager.GameState.GamePlay || MenuManager.Instance.NavigationStackPeek() != GameManager.GameState.WaitingMultiPlayer) && i == 0)
                    {
                        childElements[i].transform.localScale = new Vector3(0f, 0f, 0f);
                        childElements[i].transform.localPosition = transform.localPosition;
                    }

                    if(i>0){
                        if (MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.GamePlay || MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.WaitingMultiPlayer)
                        {
                            ProcessMenuForward(i, false);
                        }
                        else {
                            ProcessMenuForward(i, true);
                        }
                    }
                }
            }

            else {
                
                for (int i = 0; i < childElements.Length; i++)
                {
                    if ((MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.GamePlay || MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.WaitingMultiPlayer) && i == 0)
                    {
                        ProcessMenuBackward(i, false);
                    }
                    else if (MenuManager.Instance.NavigationStackPeek() != GameManager.GameState.GamePlay && i == 0)
                    {
                        childElements[i].transform.localScale = new Vector3(0f, 0f, 0f);
                        childElements[i].transform.localPosition = transform.localPosition;
                    }

                    if (i > 0)
                    {
                        if (MenuManager.Instance.NavigationStackPeek() == GameManager.GameState.GamePlay)
                        {
                            ProcessMenuBackward(i, false);
                        }
                        else
                        {
                            ProcessMenuBackward(i, true);
                        }
                    }
                }
            }
        }
    }


    public void ProcessMenuForward(int i, bool rePopulateMenu)
    {
        int index = i;
        if (rePopulateMenu)
        {
            index = i - 1;
        }

        TweenScale tweenScale = childElements[i].AddComponent<TweenScale>();
        tweenScale.from = childElements[i].transform.localScale;
        tweenScale.to = (Vector3)initialScale[i];
        tweenScale.duration = 0.5f;
        EventDelegate.Set(tweenScale.onFinished, delegate()
        {
            ProcessingCompleted();
        });
        tweenScale.PlayForward();

        TweenPosition tweenPos = childElements[i].AddComponent<TweenPosition>();
        tweenPos.from = childElements[i].transform.localPosition;
        tweenPos.to = (Vector3)initialPos[index];
        tweenPos.duration = 0.5f;
        tweenPos.PlayForward();

        TweenAlpha tweenAlpha = childElements[i].AddComponent<TweenAlpha>();
        tweenAlpha.from = 0f;
        tweenAlpha.to = 1f;
        tweenAlpha.duration = 0.5f;
        tweenAlpha.PlayForward();
    }

    public void ProcessMenuBackward(int i, bool rePopulateMenu)
    {
        int index = i;
        if (rePopulateMenu)
        {
            index = i - 1;
        }

        TweenScale tweenScale = childElements[i].AddComponent<TweenScale>();
        tweenScale.from = (Vector3)initialScale[i];
        tweenScale.to = new Vector3(0f, 0f, 0f); ;
        tweenScale.duration = 0.5f;
        EventDelegate.Set(tweenScale.onFinished, delegate()
        {
            ProcessingCompleted();
        });
        tweenScale.PlayForward();

        TweenPosition tweenPos = childElements[i].AddComponent<TweenPosition>();
        tweenPos.from = (Vector3)initialPos[index];
        tweenPos.to = transform.localPosition;
        tweenPos.duration = 0.5f;
        tweenPos.PlayForward();

        TweenAlpha tweenAlpha = childElements[i].AddComponent<TweenAlpha>();
        tweenAlpha.from = 1f;
        tweenAlpha.to = 0f;
        tweenAlpha.duration = 0.5f;
        tweenAlpha.PlayForward();
    }

    public void SetVolumeState(UITexture SoundbuttonTexture,bool changeSoundState )
    {
        if (processingMenus == false)
        {
            if (changeSoundState==true) {
                NuttySoundManager.Instance.SoundEnabled = !NuttySoundManager.Instance.SoundEnabled;
				NuttySoundManager.Instance.MusicEnabled = !NuttySoundManager.Instance.MusicEnabled;
                NuttySoundManager.Instance.MusicEnabled = !NuttySoundManager.Instance.MusicEnabled;
            }
            
            if (NuttySoundManager.Instance.SoundEnabled)
            {
                SoundbuttonTexture.mainTexture = soundTextures[0];
                NuttySoundManager.Instance.PauseAllSounds(true);
            }
            else
            {
                SoundbuttonTexture.mainTexture = soundTextures[1];
                NuttySoundManager.Instance.PauseAllSounds(false);
            }
        }
    }

    void ProcessingCompleted() {
        processingMenus = false;
    }

    void RemoveTweens() {
        for (int i = 0; i < childElements.Length; i++)
        {
            Destroy(childElements[i].GetComponent<TweenScale>());
            Destroy(childElements[i].GetComponent<TweenPosition>());
            Destroy(childElements[i].GetComponent<TweenAlpha>());
        }
    }

}
