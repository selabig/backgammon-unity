using System;
using UnityEngine;
public enum ClipState
{
    BGM_Main, BGM_GamePlay, Computer_Roll, Dice_Drop, Dice_Rolling, GUI_Click, Loose, Win, Wrong, Pointer_Click, Swap, Token_Mature, Token_Dead
}
[Serializable]
public class Sound
{
	public AudioClip clip;
	public float volume = 1f;
	public bool loop;
	public ClipState clipState;
}
