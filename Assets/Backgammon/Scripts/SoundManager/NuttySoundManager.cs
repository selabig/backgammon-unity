


using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using System;

[ExecuteInEditMode]

public class NuttySoundManager : MonoBehaviour
{
    public List<AudioClip> toLoad;
    public bool updateSounds = false;
    public List<Sound> listLoop;
    public List<Sound> listOneShot;
    public bool noSound = false;
    public bool noMusic = false;
    public AudioSource[] audioSourcesLoop;
    public AudioSource[] audioSourcesOneShot;
    public int MAX_SOUND_ONESHOT;
    private List<string> paused = new List<string>();
    private static NuttySoundManager _instance;

    private ClipState clipNotToStop = ClipState.BGM_Main;
    public float gameStateChangeFadeDelay = 0.5f;
    private float globalVolume;

    private bool soundEnabled = true;
    private bool musicEnabled = true;
    public static NuttySoundManager Instance
    {
        get
        {
            return NuttySoundManager._instance;
        }
    }

    public bool SoundEnabled
    {
        get
        {
            if (!PlayerPrefs.HasKey("GAME_SOUND"))
            {
                PlayerPrefs.SetString("GAME_SOUND", "True");
            }
            soundEnabled = Convert.ToBoolean(PlayerPrefs.GetString("GAME_SOUND"));
            return soundEnabled;
        }
        set
        {
            soundEnabled = value;
            PlayerPrefs.SetString("GAME_SOUND", soundEnabled.ToString());
        }
    }


    public float GlobalVolume
    {
        get
        {
            if (!PlayerPrefs.HasKey("globalVolume"))
            {
                PlayerPrefs.SetFloat("globalVolume", 1f);
            }
            globalVolume = PlayerPrefs.GetFloat("globalVolume");
            return globalVolume;
        }
        set
        {
            globalVolume = value;
            PlayerPrefs.SetFloat("globalVolume", globalVolume);
        }
    }


    public bool MusicEnabled
    {
        get
        {
            if (!PlayerPrefs.HasKey("GAME_MUSIC"))
            {
                PlayerPrefs.SetString("GAME_MUSIC", "True");
            }
            musicEnabled = Convert.ToBoolean(PlayerPrefs.GetString("GAME_MUSIC"));
            return musicEnabled;
        }
        set
        {
            musicEnabled = value;
            PlayerPrefs.SetString("GAME_MUSIC", musicEnabled.ToString());
        }
    }



    public void OnVolumeChange(float val)
    {
        GlobalVolume = val;
        AudioListener.volume = val;
    }

    private void AddConfigurations()
    {
        foreach (AudioClip v in this.toLoad)
        {
            if (v == null)
            {
                //UnityEngine.Debug.LogWarning ("Sound/Music in load list is Missing");
                return;
            }
            List<Sound> list = (!this.isLoop(v.name)) ? this.listOneShot : this.listLoop;
            if (list.Find((Sound x) => x.clip == v) == null)
            {
                list.Add(new Sound
                {
                    clip = v,
                    loop = this.isLoop(v.name)
                });
            }
        }
        AudioSource[] arr = transform.GetComponents<AudioSource>();
        foreach (AudioSource source in arr)
        {
            DestroyImmediate(source);
        }


        audioSourcesLoop = new AudioSource[0];
        audioSourcesOneShot = new AudioSource[0];
        if (this.listLoop.Count == 0)
        {
            //UnityEngine.Debug.LogWarning ("There is no loop sound in list");
        }
        else
        {
            this.audioSourcesLoop = new AudioSource[this.listLoop.Count];
            for (int i = 0; i < this.listLoop.Count; i++)
            {
                AudioSource audioSource = gameObject.AddComponent<AudioSource>();
                this.audioSourcesLoop[i] = audioSource;
            }
        }

        if (this.listOneShot.Count == 0)
        {
            //UnityEngine.Debug.LogWarning ("There is no One Shot sound in list");
        }
        else
        {
            this.audioSourcesOneShot = new AudioSource[this.MAX_SOUND_ONESHOT];
            for (int j = 0; j < this.MAX_SOUND_ONESHOT; j++)
            {
                AudioSource audioSource2 = gameObject.AddComponent<AudioSource>();
                this.audioSourcesOneShot[j] = audioSource2;
            }
        }

        SoundEnabled = noSound;
        MusicEnabled = noMusic;
        this.toLoad.Clear();
        this.paused.Clear();
    }

    private void Update()
    {
        if (this.updateSounds)
        {
            this.updateSounds = false;
            AddConfigurations();
        }
    }

    private bool isLoop(string name)
    {
        return name.StartsWith("MUSIC_") || name.StartsWith("LOOP_") || name.StartsWith("BGM_");
    }

    private void Awake()
    {

        if (NuttySoundManager._instance != null && NuttySoundManager._instance != this)
        {
            UnityEngine.Object.Destroy(this);
            UnityEngine.Object.Destroy(gameObject);
            return;
        }
        NuttySoundManager._instance = this;

        //thesun
#if !(UNITY_EDITOR)
        UnityEngine.Object.DontDestroyOnLoad(NuttySoundManager.Instance);
#endif

        //GameManager.Instance.GameStateChanged += OnGameStateChangedSetMusic;

    }

    void Start()
    {
        soundEnabled = SoundEnabled;
        musicEnabled = MusicEnabled;
        globalVolume = GlobalVolume;
        //  GameManager.Instance.GameStateChanged += OnGameStateChangedSetMusic;


    }

    //	void OnDisable()
    //	{
    //		GameManager.Instance.GameStateChanged -= OnGameStateChangedSetMusic;
    //	}

    private Sound getSound(ClipState clipState)
    {
        Sound result = null;
        if (this.isLoop(clipState.ToString()))
        {
            for (int i = 0; i < this.listLoop.Count; i++)
            {
                if (!(this.listLoop[i].clip == null))
                {
                    if (clipState == this.listLoop[i].clipState)
                    {
                        result = this.listLoop[i];
                        break;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < this.listOneShot.Count; i++)
            {
                if (!(this.listOneShot[i].clip == null))
                {
                    if (clipState == this.listOneShot[i].clipState)
                    {
                        result = this.listOneShot[i];
                        break;
                    }
                }
            }
        }
        return result;
    }

    private AudioSource getAudioSource(ClipState clipState)
    {
        return this.getAudioSource(clipState, false);
    }

    private AudioSource getAudioSource(ClipState clipState, bool isPlaying)
    {

        AudioSource result = null;
        if (this.isLoop(clipState.ToString()))
        {
            for (int i = 0; i < this.audioSourcesLoop.Length; i++)
            {
                if (!(this.audioSourcesLoop[i].clip == null))
                {
                    if (this.audioSourcesLoop[i].clip.name.Equals(clipState.ToString()) && (!isPlaying || (isPlaying && this.audioSourcesLoop[i].isPlaying)))
                    {
                        result = this.audioSourcesLoop[i];
                        break;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < this.audioSourcesOneShot.Length; i++)
            {
                if (!(this.audioSourcesOneShot[i].clip == null))
                {
                    if (this.audioSourcesOneShot[i].clip.name.Equals(clipState.ToString()) && (!isPlaying || (isPlaying && this.audioSourcesOneShot[i].isPlaying)))
                    {
                        result = this.audioSourcesOneShot[i];
                        break;
                    }
                }
            }
        }
        return result;
    }

    public void PlaySoundIfNotPlaying(ClipState clipState)
    {
        AudioSource audioSource = this.getAudioSource(clipState, true);

        if (audioSource == null)
        {
            UnityEngine.Debug.Log("notplaying");
            PlaySound(clipState);
            return;
        }
    }

    public AudioSource PlaySound(ClipState clipState)
    {
        //UnityEngine.Debug.Log (clipState);
        Sound sound = this.getSound(clipState);
        if (sound == null)
        {
            //UnityEngine.Debug.LogWarning ("Sound is missing : " + clipState.ToString ());
            return null;
        }
        this.playSound(sound.clip, sound.volume, sound.loop, 0f);
        AudioSource audioSource = this.getAudioSource(clipState);
        if (audioSource == null)
        {
            //UnityEngine.Debug.LogWarning ("Sound is missing : " + clipState.ToString ());
            return null;
        }
        return audioSource;
    }

    public void PlaySoundDelay(ClipState clipState, float delay)
    {
        Sound sound = this.getSound(clipState);
        if (sound == null)
        {
            //UnityEngine.Debug.LogWarning ("Sound is missing : " + clipState.ToString ());
            return;
        }
        this.playSound(sound.clip, sound.volume, sound.loop, delay);
        AudioSource audioSource = this.getAudioSource(clipState);
        if (audioSource == null)
        {
            //UnityEngine.Debug.LogWarning ("Sound is missing : " + clipState.ToString ());
            return;
        }
    }

    public void PlayMusic(ClipState clipState)
    {
        if (!MusicEnabled || this.noMusic)
        {
            return;
        }
        if (this.isMusicPlaying(clipState))
        {
            return;
        }
        Sound sound = this.getSound(clipState);
        if (sound == null)
        {
            //UnityEngine.Debug.LogWarning ("Music is missing : " + clipState.ToString ());
            return;
        }
        this.playSound(sound.clip, sound.volume, sound.loop, 0f);
        this.removePaused(clipState.ToString());
    }

    public void PlayMusic(ClipState clipState, float delay)
    {
        if (!MusicEnabled || this.noMusic)
        {
            return;
        }
        if (this.isMusicPlaying(clipState))
        {
            return;
        }
        this.PlayMusicDelay(clipState, delay);
    }

    private void PlayMusicDelay(ClipState clipState, float delay)
    {
        Sound sound = this.getSound(clipState);
        if (sound == null)
        {
            //UnityEngine.Debug.LogWarning ("Music is missing : " + clipState.ToString ());
            return;
        }
        this.playSound(sound.clip, sound.volume, sound.loop, delay);
        this.removePaused(clipState.ToString());
    }

    public void PlaySoundPositional(ClipState clipState, GameObject obj, float delay)
    {
        Sound sound = this.getSound(clipState);
        if (sound == null)
        {
            //UnityEngine.Debug.LogWarning ("Sound is missing : " + clipState.ToString ());
            return;
        }

        AudioSource component = obj.GetComponent<AudioSource>();
        if (component == null || !component.clip.name.Equals(clipState.ToString()))
        {
            AudioSource audioSource = obj.AddComponent<AudioSource>();
            audioSource.clip = sound.clip;
            audioSource.loop = sound.loop;
            audioSource.volume = sound.volume;
            audioSource.PlayDelayed(delay);
        }
        else
        {
            component.PlayDelayed(delay);
        }

        this.removePaused(clipState.ToString());
    }

    public void PlaySoundState(ClipState clip)
    {

    }


    //PlaySFXClip((int)SoundManager.SoundClipName.PopUp, false, 1);

    private void playSound(AudioClip clip, float volume = 1.0f, bool loop = false, float delay = 0.0f)
    {
        AudioSource audioSource = null;
        int num = 0;
        if (loop)
        {

            do
            {
                if (num >= this.audioSourcesLoop.Length)
                {
                    break;
                }
                if (!this.audioSourcesLoop[num].isPlaying)
                {
                    audioSource = this.audioSourcesLoop[num];

                }
                else
                {
                    num++;
                }
            } while (audioSource == null || num >= this.audioSourcesLoop.Length);
            if (num >= this.audioSourcesLoop.Length)
            {
                audioSource = this.audioSourcesLoop[UnityEngine.Random.Range(0, this.audioSourcesLoop.Length)];
            }
        }
        else
        {

            do
            {
                if (num >= this.audioSourcesOneShot.Length)
                {
                    break;
                }
                if (!this.audioSourcesOneShot[num].isPlaying)
                {
                    audioSource = this.audioSourcesOneShot[num];

                }
                else
                {
                    num++;
                }
            } while (audioSource == null || num >= this.audioSourcesOneShot.Length);
            if (num >= this.audioSourcesOneShot.Length)
            {
                audioSource = this.audioSourcesOneShot[UnityEngine.Random.Range(0, this.audioSourcesOneShot.Length)];
            }
        }
        if (audioSource == null)
        {
            //UnityEngine.Debug.LogWarning ("No AudioSource is Free");
            return;
        }
        audioSource.clip = clip;
        audioSource.volume = volume;
        if (loop)
        {
            if (!MusicEnabled)
            {
                audioSource.volume = 0;
            }
        }
        else
        {
            if (!SoundEnabled)
            {
                audioSource.volume = 0;
            }

        }
        audioSource.loop = loop;
        audioSource.PlayDelayed(delay);
        audioSource.panStereo = 0f;
    }

    public bool isMusicPlaying(ClipState clipState)
    {
        AudioSource audioSource = this.getAudioSource(clipState);
        return !(audioSource == null) && audioSource.isPlaying;
    }

    public void PlaySoundFadeIn(ClipState clipState, float fadeIn)
    {
        //thesun
        if (!SoundEnabled)
            return;

        Sound sound = this.getSound(clipState);
        if (sound == null)
        {
            //UnityEngine.Debug.LogWarning ("Sound is missing : " + clipState.ToString ());
            return;
        }
        this.playSound(sound.clip, sound.volume, sound.loop, 0f);
        this.addPaused(clipState.ToString());
        AudioSource audioSource = this.getAudioSource(clipState);
        if (audioSource == null)
        {
            //UnityEngine.Debug.LogWarning ("Sound is missing : " + clipState.ToString ());
            return;
        }
        if (isLoop(audioSource.clip.name))
        {
            if (!MusicEnabled)
            {
                audioSource.volume = 0;
                return;
            }
        }
        else
        {
            if (!SoundEnabled)
            {
                audioSource.volume = 0;
                return;
            }

        }
        StartCoroutine(this.fadeInSound(audioSource, fadeIn, sound.volume));
    }

    public IEnumerator fadeInSound(AudioSource source, float duration, float volume)
    {

        float i = 0.0f;

        float step = 1.0f / duration;

        while (i <= 1.0f)
        {

            i += step * Time.deltaTime;

            source.volume = Mathf.Lerp(0f, volume, i);
            yield return false;
        }

    }

    public void StopSoundFadeOut(ClipState clipState, float duration)
    {
        AudioSource audioSource = this.getAudioSource(clipState, true);
        if (audioSource == null)
        {
            return;
        }
        if (!audioSource.isPlaying)
        {
            audioSource.Stop();
            return;
        }
        this.removePaused(clipState.ToString());
        if (isLoop(audioSource.clip.name))
        {
            if (!MusicEnabled)
            {
                audioSource.volume = 0;
                return;
            }
        }
        else
        {
            if (!SoundEnabled)
            {
                audioSource.volume = 0;
                return;
            }

        }
        StartCoroutine(this.fadeOutSound(audioSource, duration));
    }

    private IEnumerator fadeOutSound(AudioSource source, float duration)
    {



        float i = 0.0f;

        float step = 1.0f / duration;

        while (i < 1.0f)
        {

            i += step * Time.deltaTime;

            source.volume = Mathf.Lerp(1f, 0f, i);
            yield return false;
        }

        source.Stop();
        source.clip = null;
    }

    public void PauseSound(ClipState clipState)
    {
        if (!SoundEnabled || this.noSound)
        {
            return;
        }
        this.pauseSound(clipState);
    }

    public void ResumeSound(ClipState clipState)
    {
        if (!SoundEnabled || this.noSound)
        {
            return;
        }
        this.resumeSound(clipState);
    }

    public void PauseMusic(ClipState clipState)
    {
        if (!MusicEnabled || this.noMusic)
        {
            return;
        }
        this.pauseSound(clipState);
    }

    public void ResumeMusic(ClipState clipState)
    {
        if (!MusicEnabled || this.noMusic)
        {
            return;
        }
        this.resumeSound(clipState);
    }

    private void pauseSound(ClipState clipState)
    {
        AudioSource audioSource = this.getAudioSource(clipState, true);
        if (audioSource == null || !audioSource.isPlaying)
        {
            return;
        }
        audioSource.Pause();
        this.addPaused(clipState.ToString());
    }

    private void resumeSound(ClipState clipState)
    {
        AudioSource audioSource = this.getAudioSource(clipState);
        if (audioSource == null || !audioSource.isPlaying)
        {
            return;
        }
        audioSource.Play();
        this.addPaused(clipState.ToString());
    }

    public void Pause()
    {
        this.setPause(true);
    }

    public void Resume()
    {
        this.setPause(false);
    }

    private void setPause(bool pause)
    {
        if (SoundEnabled && !this.noSound)
        {
            AudioSource[] array = this.audioSourcesOneShot;
            for (int i = 0; i < array.Length; i++)
            {
                AudioSource audioSource = array[i];
                if (!(audioSource == null) && !(audioSource.clip == null))
                {
                    if (pause && audioSource.isPlaying)
                    {
                        audioSource.Pause();
                        this.addPaused(audioSource.clip.name);
                    }
                    else
                    {
                        if (!pause && this.isPaused(audioSource.clip.name))
                        {
                            audioSource.Play();
                            this.removePaused(audioSource.clip.name);
                        }
                    }
                }
            }
        }
        if (MusicEnabled && !this.noMusic)
        {
            AudioSource[] array2 = this.audioSourcesLoop;
            for (int j = 0; j < array2.Length; j++)
            {
                AudioSource audioSource2 = array2[j];
                if (!(audioSource2 == null) && !(audioSource2.clip == null))
                {
                    if (pause && audioSource2.isPlaying)
                    {
                        audioSource2.Pause();
                        this.addPaused(audioSource2.clip.name);
                    }
                    else
                    {
                        if (!pause && this.isPaused(audioSource2.clip.name))
                        {
                            audioSource2.Play();
                            this.removePaused(audioSource2.clip.name);
                        }
                    }
                }
            }
        }
    }

    private void addPaused(string soundName)
    {
        if (this.paused.Contains(soundName))
        {
            return;
        }
        this.paused.Add(soundName);
    }

    private void removePaused(string soundName)
    {
        if (!this.paused.Contains(soundName))
        {
            return;
        }
        this.paused.RemoveAt(this.paused.IndexOf(soundName));
    }

    private void clearPaused()
    {
        this.paused.Clear();
    }

    private bool isPaused(string soundName)
    {
        return this.paused.Contains(soundName);
    }

    public void PauseAllSounds(bool isEnabled)
    {
        for (int i = 0; i < this.audioSourcesOneShot.Length; i++)
        {
            AudioSource audioSource = this.audioSourcesOneShot[i];
            if (isEnabled)
            {
                audioSource.Play();
            }
            else
            {
                audioSource.Pause();
            }

        }
        for (int w = 0; w < this.audioSourcesLoop.Length; w++)
        {
            AudioSource audioSource = this.audioSourcesLoop[w];
            if (isEnabled)
            {
                audioSource.Play();
            }
            else
            {
                audioSource.Pause();
            }
        }
    }

    #region GameSpecific Code
    /*void Start()
	{
		
	}*/

    public void OnGameStateChangedSetMusic(GameManager.GameState changedState)
    {
        UnityEngine.Debug.Log("Play " + changedState.ToString());

        switch (changedState)
        {
            case GameManager.GameState.MainMenu:
                FadeOutAllPreviousBGMS();
                break;

            case GameManager.GameState.GamePlay:
                if (!isMusicPlaying(ClipState.BGM_Main))
                {
                    PlaySoundFadeIn(ClipState.BGM_Main, gameStateChangeFadeDelay);
                }
                //thesun
                if (!SoundEnabled)
                    FadeOutAllPreviousBGMS();
                break;

            default:
                FadeOutAllPreviousBGMS();
                break;
        }
    }

    public void FadeOutAllPreviousBGMS()
    {
        foreach (Sound sound in this.listLoop)
        {
            if (sound.clipState.ToString().StartsWith("BGM_"))
            {
                AudioSource audioSource = this.getAudioSource(sound.clipState, true);
                if (audioSource != null)
                {

                    StopSoundFadeOut(sound.clipState, gameStateChangeFadeDelay);

                }

            }
        }
    }

    /*private void OnGameStateChangedSetMusic (GameManager.GameState changedState)
	{
		if (changedState == GameManager.GameState.Store 
			|| changedState == GameManager.GameState.SettingsPopup 
			|| changedState == GameManager.GameState.PremiumLockedStagePopup 
			|| changedState == GameManager.GameState.LockedStageDetailsPopup
			|| changedState == GameManager.GameState.UnlockedStageDetailsPopup 
			|| changedState == GameManager.GameState.DailyBonusPopUp
			|| changedState == GameManager.GameState.SocialHub
		    || changedState == GameManager.GameState.CharacterPurchasePopup
		    || changedState == GameManager.GameState.ItemPurchasePopup
		    ) {
			//if (changedState == GameManager.GameState.SettingsPopup){
		
			//}
			//else{
				//VolumeDownPlayingBGMS ();
			//}

		} else {
			FadeOutAllPreviousBGMS ();
		}
		
		switch (changedState) {
		case GameManager.GameState.MainMenu:
			if (!isMusicPlaying (ClipState.BGM_MENU)) {
				clipNotToStop = ClipState.BGM_MENU;
				PlaySoundFadeIn (ClipState.BGM_MENU, gameStateChangeFadeDelay);
			}			
			break;
		case GameManager.GameState.CharacterSelection:
			if (!isMusicPlaying (ClipState.BGM_CHARACTER_SELECTION)) {
				clipNotToStop = ClipState.BGM_CHARACTER_SELECTION;
				PlaySoundFadeIn (ClipState.BGM_CHARACTER_SELECTION, gameStateChangeFadeDelay);
				if (GameManager.Instance.previousState == GameManager.GameState.MainMenu) {
					PlaySound (ClipState.helpMissUniverse);
				}
			}
			break;
		case GameManager.GameState.Wardrobe:
			if (!isMusicPlaying (ClipState.BGM_DRESSUP_SCENE)) {
				clipNotToStop = ClipState.BGM_DRESSUP_SCENE;
				PlaySoundFadeIn (ClipState.BGM_DRESSUP_SCENE, gameStateChangeFadeDelay);
			}
			break;
		case GameManager.GameState.StageSelection:
			if (!isMusicPlaying (ClipState.BGM_MISSION_SELECTION)) {
				clipNotToStop = ClipState.BGM_MISSION_SELECTION;
				PlaySoundFadeIn (ClipState.BGM_MISSION_SELECTION, gameStateChangeFadeDelay);
			}
			break;
		case GameManager.GameState.MiniGame:
			if (!isMusicPlaying (ClipState.BGM_KITCHEN)) {
				clipNotToStop = ClipState.BGM_KITCHEN;
				PlaySoundFadeIn (ClipState.BGM_KITCHEN, gameStateChangeFadeDelay);
			}
			break;
		case GameManager.GameState.StageCelebration:
		case GameManager.GameState.PremiumCelebration:
			if (!isMusicPlaying (ClipState.BGM_STAGE)) {
				clipNotToStop = ClipState.BGM_STAGE;
				PlaySound (ClipState.Crowd_Long);
				PlaySoundDelay (ClipState.BGM_STAGE, gameStateChangeFadeDelay + 1.2f);
			}
			break;
		case GameManager.GameState.Store:
			PlaySound (ClipState.Popup_Shop);
			break;
		case GameManager.GameState.SettingsPopup:
			PlaySound (ClipState.Popup_Setting);
			break;
		case GameManager.GameState.UnlockedStageDetailsPopup:
			PlaySound (ClipState.Popup_Events);
			break;
		case GameManager.GameState.LockedStageDetailsPopup:
			PlaySound (ClipState.Popup_Events);
			break;
		case GameManager.GameState.PremiumLockedStagePopup:
			PlaySound (ClipState.Popup_Events);
			break;
		case GameManager.GameState.DailyBonusPopUp:
			PlaySound (ClipState.Stage_Popup);
			break;
		case GameManager.GameState.SocialHub:
			PlaySound (ClipState.Popup_Login);

			break;
		case GameManager.GameState.TaskCompletePopup:
			PlaySoundDelay (ClipState.Short_Crowd, gameStateChangeFadeDelay + 1.2f);
			PlaySound (ClipState.Popup);
			PlaySound (ClipState.Progress_Bar_Don);
			HalfVolumeofSound (ClipState.BGM_STAGE);
			break;
		case GameManager.GameState.PremiumTaskCompletePopup:
			PlaySoundDelay (ClipState.Short_Crowd, gameStateChangeFadeDelay + 1.2f);
			PlaySound (ClipState.Popup);
			PlaySound (ClipState.Progress_Bar_Don);
			HalfVolumeofSound (ClipState.BGM_STAGE);
			break;
		case GameManager.GameState.MinigameGameOverPopUp:
			PlaySound (ClipState.Loose);
			break;

		case GameManager.GameState.InvitePopUp:
			PlaySoundDelay (ClipState.helpYourFriend, gameStateChangeFadeDelay + 0.35f);
			break;
		}
	}
	
	public void PlayingWardobeThenLowTheVolume (ClipState a)
	{
		if (isMusicPlaying (ClipState.BGM_DRESSUP_SCENE)) {
			AudioSource audioSource = this.getAudioSource (ClipState.BGM_DRESSUP_SCENE);
			
			//UnityEngine.Debug.Log (audioSource.clip.name);
			audioSource.volume = audioSource.volume / 2f;
			AudioSource clipSource = this.getAudioSource (a);
			//UnityEngine.Debug.Log (clipSource.clip.length);
			Invoke ("ResetSoundWardobeBG", clipSource.clip.length);
		}
	}

	public void ResetSoundWardobeBG ()
	{
		
		
		if (isMusicPlaying (ClipState.BGM_DRESSUP_SCENE)) {
			AudioSource audioSource = this.getAudioSource (ClipState.BGM_DRESSUP_SCENE);
			audioSource.volume = audioSource.volume * 2f;
			//UnityEngine.Debug.Log (audioSource.clip.name);
		}
	}
	
	private void StopAllPreviousBGMSExcept (ClipState clipNotToStop)
	{
		foreach (Sound sound in this.listLoop) {
			if (sound.clipState.ToString ().StartsWith ("BGM_") && sound.clipState != clipNotToStop) {
				StopSoundFadeOut (sound.clipState, gameStateChangeFadeDelay);
			}
		}
	}
	
	private void VolumeDownPlayingBGMS ()
	{
		foreach (Sound sound in this.listLoop) {
			if (sound.clipState.ToString ().StartsWith ("BGM_")) {
				HalfVolumeofSound (sound.clipState);
			}
		}
	}
	
	private void FadeOutAllPreviousBGMS ()
	{
		foreach (Sound sound in this.listLoop) {
			if (sound.clipState.ToString ().StartsWith ("BGM_")) {
				AudioSource audioSource = this.getAudioSource (sound.clipState, true);
				if (audioSource != null) {
					if (audioSource.tag.Equals ("Half")) {
						audioSource.volume = audioSource.volume * 3f;
						audioSource.tag = "Full";
						return;
					}
					if (!audioSource.tag.Equals ("Half")) {
						StopSoundFadeOut (sound.clipState, gameStateChangeFadeDelay);
					}

				}
//				else{
//					audioSource = this.getAudioSource (sound.clipState, false);
//					if (audioSource != null) {
//						if (audioSource.tag == "Half") {
//							audioSource.volume = audioSource.volume * 3f;
//							audioSource.tag = "Full";
//							return;
//						}
//					}
//				}
			}
		}
	}



	public void PauseAllBGMS ()
	{
		foreach (Sound sound in this.listLoop) {
			if (sound.clipState.ToString ().StartsWith ("BGM_")) {
				AudioSource audioSource = this.getAudioSource (sound.clipState, true);
				if (audioSource != null) {
					audioSource.Pause();
				}
			}
		}
	}
	public void ResumeAllBGMS ()
	{
		foreach (Sound sound in this.listLoop) {
			if (sound.clipState.ToString ().StartsWith ("BGM_")) {
				AudioSource audioSource = this.getAudioSource (sound.clipState, false);
				if (audioSource != null) {
					if (audioSource.tag == "Half") {
						audioSource.volume = sound.volume / 3f;
					}
					audioSource.Play();
				}
			}
		}
	}


	public void HalfVolumeofSound (ClipState clipState)
	{
		AudioSource audioSource = this.getAudioSource (clipState, true);
		
		if (audioSource == null) {
			return;
		}
		audioSource.tag = "Half";
		//audioSource.Stop ();
		audioSource.volume = audioSource.volume / 3f;
		//audioSource.clip = null;
		StopAllCoroutines ();
		
		//this.removePaused (clipState.ToString ());
	}*/

    #endregion GameSpecific Code;
}
