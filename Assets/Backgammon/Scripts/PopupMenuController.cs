﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopupMenuController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void closeMe()
    {
        this.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void showMe()
    {
        Time.timeScale = 0;
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Menu Button Clicked", 1);
        this.gameObject.SetActive(true);        
    }

    public void mainMenuBtnCLicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Main Menu Button Clicked", 1);
        closeMe();
        MenuManager.Instance.PushMenu(GameManager.GameState.MainMenu);

        GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().StopGame();
    
       // GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().saveGame();
    }

    public void newGameBtnCLicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "New Game Button Clicked", 1);


        // TSUtil.RunLater(() => MessageBox.ShowWithCustomButton(
        // "  ",
        // "Forfeit the current game?",
        // (result) =>
        // {
        //     if (result == "Yes")
        //     {
        //         GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewMatch();
        //     }
        //     closeMe();
        // },
        // new List<string>() { "Yes", "No" }), 0.1f);


        closeMe();
        //MenuManager.Instance.PushMenu(GameManager.GameState.NewGameSelectionPanel);
        GameManager.Instance.gamePlayObj.GetComponent<GamePlay>().loadNewMatch();
    }
    public void settingsBtnCLicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Setting Button Clicked", 1);
        closeMe();
        MenuManager.Instance.PushMenu(GameManager.GameState.SettingPanel);
    }

    public void helpBtnCLicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Help Button Clicked", 1);
        MenuManager.Instance.PushMenu(GameManager.GameState.HelpScreen);
        closeMe();
    }
    public void cancleBtnCLicked()
    {
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Menu Button Clicked", 1);
        closeMe();
    }
}

