﻿using UnityEngine;
using System.Collections;

public class LevelSelection : BaseMenu {


	public void SinglePlayerBtnClicked()
	{
		TSUtil.LogEvent (AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY,AnalyticConstants.GA_ACTION_TYPE_BUTTON,"Single Play Mode",1);
		GameManager.Instance.deActivateMultiplayerComponents ();
		GameManager.Instance.isGameMultiplayer = false;
		MenuManager.Instance.PushMenu (GameManager.GameState.GamePlay);
        GameManager.Instance.menuHandler.CloseMenu();
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);
	}

	public void MultiPlayerBtnClicked()
	{
        TSUtil.LogEvent(AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY, AnalyticConstants.GA_ACTION_TYPE_BUTTON, "Multiplayer Mode", 1);
		GameManager.Instance.activateMultiplayerComponents ();
        GameManager.Instance.isGameMultiplayer = true;
		MenuManager.Instance.PushMenu (GameManager.GameState.GamePlay);
		GameManager.Instance.menuHandler.CloseMenu();
        GameManager.Instance.nuttySoundManager.PlaySound(ClipState.GUI_Click);


	}
}
