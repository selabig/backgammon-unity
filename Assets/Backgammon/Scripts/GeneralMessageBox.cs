﻿using UnityEngine;
using System.Collections;

public class GeneralMessageBox : MonoBehaviour
{

    private TweenAlpha tweenAlpha;

	private float timeAutoHide;

    void Awake()
    {
        tweenAlpha = gameObject.GetComponent<TweenAlpha>();
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void showMessageAutoHide(string msg)
    {
		showMessageAutoHide(msg, 1.5f);        
    }

    public void showMessageAutoHide(string msg, float second)
    {
		timeAutoHide = second;
        gameObject.SetActive(true);
        gameObject.GetComponent<UILabel>().text = msg;
        tweenAlpha.PlayForward();
        tweenAlpha.SetOnFinished(completedShowMessage);
    }


    void completedShowMessage()
    {
        //Invoke("hideMessage", 1.5f);
		StartCoroutine(hideMessageWithDelay(timeAutoHide));
    }

    IEnumerator hideMessageWithDelay(float second)
    {
        yield return new WaitForSeconds(second);
		hideMessage();
    }


    public void hideMessage()
    {
        tweenAlpha.PlayReverse();
        tweenAlpha.SetOnFinished(completedHideMessage);
    }
    public void closeMessage()
    {
        gameObject.SetActive(false);
		StopCoroutine("hideMessageWithDelay");		
    }

    void completedHideMessage()
    {
        closeMessage();
    }
}
