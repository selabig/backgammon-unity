﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

public class MoPubAdsManager : Singleton<MoPubAdsManager>
{
    [DllImport("__Internal")]
    private static extern void repositionMopubBanner(int viewMode);

    private int _selectedToggleIndex;
    private string[] _bannerAdUnits;
    private string[] _interstitialAdUnits;
    private string[] _rewardedVideoAdUnits;

    private string[] _networkList = new string[] {
        "MoPub",
        "Millennial",
        "AdMob",
        "Chartboost",
        "Vungle",
        "Facebook",
        "AdColony",
        "Unity Ads"
    };

#if UNITY_ANDROID
	private Dictionary<string, string[]> _bannerDict = new Dictionary<string, string[]> () {
		{ "320x50", new string[] { "3f8c355c3cce48cd90999d4f939f0c03" } },
		{ "728x90", new string[] { "ed8640b9268e4f08afd75172d0438006" } },		
	};

	private Dictionary<string, string[]> _interstitialDict = new Dictionary<string, string[]> () {
		{ "768x1024", new string[] { "91cf4787eb1b4a5baab2bbfa685d6c4f" } },
		{ "320x480", new string[] { "06fc722bb1e74219a04861a99fc97083" } },		
		{ "480x320", new string[] { "08e092b75d584964998abe745444c80c" } },
	};

	private Dictionary<string, string[]> _rewardedVideoDict;

#elif UNITY_IPHONE
    private Dictionary<string, string[]> _bannerDict = new Dictionary<string, string[]>() {
        { "320x50", new string[] { "3f8c355c3cce48cd90999d4f939f0c03" } },
        { "728x90", new string[] { "ed8640b9268e4f08afd75172d0438006" } },
    };

    private Dictionary<string, string[]> _interstitialDict = new Dictionary<string, string[]>() {
        { "768x1024", new string[] { "91cf4787eb1b4a5baab2bbfa685d6c4f" } },
        { "320x480", new string[] { "06fc722bb1e74219a04861a99fc97083" } },
        { "480x320", new string[] { "08e092b75d584964998abe745444c80c" } },
    };

    private Dictionary<string, string[]> _rewardedVideoDict;
#endif


    public string[] allBannerAdUnits = new string[0];
    public string[] allInterstitialAdUnits = new string[0];
    public string[] allRewardedVideoAdUnits = new string[0];


    static bool IsAdUnitArrayNullOrEmpty(string[] adUnitArray)
    {
        return (adUnitArray == null || adUnitArray.Length == 0);
    }


    void Start()
    {
        foreach (var bannerAdUnits in _bannerDict.Values)
        {
            allBannerAdUnits = allBannerAdUnits.Union(bannerAdUnits).ToArray();
        }

        foreach (var interstitialAdUnits in _interstitialDict.Values)
        {
            allInterstitialAdUnits = allInterstitialAdUnits.Union(interstitialAdUnits).ToArray();
        }

        // foreach (var rewardedVideoAdUnits in _rewardedVideoDict.Values)
        // {
        //     allRewardedVideoAdUnits = allRewardedVideoAdUnits.Union(rewardedVideoAdUnits).ToArray();
        // }

#if UNITY_ANDROID
		MoPub.loadBannerPluginsForAdUnits(allBannerAdUnits);
		MoPub.loadInterstitialPluginsForAdUnits(allInterstitialAdUnits);
		MoPub.loadRewardedVideoPluginsForAdUnits(allRewardedVideoAdUnits);
#elif UNITY_IPHONE
        MoPub.loadPluginsForAdUnits(allBannerAdUnits);
        MoPub.loadPluginsForAdUnits(allInterstitialAdUnits);
        MoPub.loadPluginsForAdUnits(allRewardedVideoAdUnits);
#endif

        if (!IsAdUnitArrayNullOrEmpty(allRewardedVideoAdUnits))
        {
            MoPub.initializeRewardedVideo();
        }
        createBanner();
        requestFullScreen();
    }

    public void requestFullScreen()
    {
        foreach (var interstitialAdUnits in allInterstitialAdUnits)
        {
            MoPub.requestInterstitialAd(interstitialAdUnits);
        }
    }
    public void showFullScreen()
    {
        if (TSUtil.isIPad())
        {
            MoPub.showInterstitialAd(allInterstitialAdUnits[0]);
        }
        else
        {
            if (TSUtil.Instance.SettingBoardView == (int)TSUtil.BoardViewMode.Portrait)
                MoPub.showInterstitialAd(allInterstitialAdUnits[1]);
            else
                MoPub.showInterstitialAd(allInterstitialAdUnits[2]);
        }
    }


    public void createBanner()
    {
        if (TSUtil.isIPhone())
            MoPub.createBanner(allBannerAdUnits[0], MoPubAdPosition.TopCenter);
        else
        {
            MoPub.createBanner(allBannerAdUnits[1], MoPubAdPosition.BottomCenter);
        }
        //TSUtil.RunLater(()=> {hideBanner();}, 0.2f);        
    }

    public void repositionBanner()
    {
        if (TSUtil.isIPhone())
        {
			if( Application.platform == RuntimePlatform.IPhonePlayer )
				repositionMopubBanner(TSUtil.Instance.SettingBoardView);
        }
        else if (TSUtil.isIPad())
        {
			if( Application.platform == RuntimePlatform.IPhonePlayer )
				repositionMopubBanner(2);
        }
		
		if (MenuManager.Instance.NavigationStackPeek() != GameManager.GameState.GamePlay)
		{
			MoPubAdsManager.Instance.hideBanner();
		}
        
        
    }
    public void showBanner()
    {
        if (TSUtil.isIPhone())
            MoPub.showBanner(allBannerAdUnits[0], true);
        else
        {
            MoPub.showBanner(allBannerAdUnits[1], true);
        }
    }
    public void hideBanner()
    {
        foreach (var bannerAdUnits in allBannerAdUnits)
        {
            MoPub.showBanner(bannerAdUnits, false);
        }
    }
    public void destroyBanner()
    {
        foreach (var bannerAdUnits in allBannerAdUnits)
        {
            MoPub.destroyBanner(bannerAdUnits);
        }
    }

#if UNITY_ANDROID || UNITY_IPHONE

    void OnEnable()
    {
        // Listen to all events for illustration purposes
        MoPubManager.onAdLoadedEvent += onAdLoadedEvent;
        MoPubManager.onAdFailedEvent += onAdFailedEvent;
        MoPubManager.onAdClickedEvent += onAdClickedEvent;
        MoPubManager.onAdExpandedEvent += onAdExpandedEvent;
        MoPubManager.onAdCollapsedEvent += onAdCollapsedEvent;

        MoPubManager.onInterstitialLoadedEvent += onInterstitialLoadedEvent;
        MoPubManager.onInterstitialFailedEvent += onInterstitialFailedEvent;
        MoPubManager.onInterstitialShownEvent += onInterstitialShownEvent;
        MoPubManager.onInterstitialClickedEvent += onInterstitialClickedEvent;
        MoPubManager.onInterstitialDismissedEvent += onInterstitialDismissedEvent;
        MoPubManager.onInterstitialExpiredEvent += onInterstitialExpiredEvent;

        MoPubManager.onRewardedVideoLoadedEvent += onRewardedVideoLoadedEvent;
        MoPubManager.onRewardedVideoFailedEvent += onRewardedVideoFailedEvent;
        MoPubManager.onRewardedVideoExpiredEvent += onRewardedVideoExpiredEvent;
        MoPubManager.onRewardedVideoShownEvent += onRewardedVideoShownEvent;
        MoPubManager.onRewardedVideoFailedToPlayEvent += onRewardedVideoFailedToPlayEvent;
        MoPubManager.onRewardedVideoReceivedRewardEvent += onRewardedVideoReceivedRewardEvent;
        MoPubManager.onRewardedVideoClosedEvent += onRewardedVideoClosedEvent;
        MoPubManager.onRewardedVideoLeavingApplicationEvent += onRewardedVideoLeavingApplicationEvent;
    }


    void OnDisable()
    {
        // Remove all event handlers
        MoPubManager.onAdLoadedEvent -= onAdLoadedEvent;
        MoPubManager.onAdFailedEvent -= onAdFailedEvent;
        MoPubManager.onAdClickedEvent -= onAdClickedEvent;
        MoPubManager.onAdExpandedEvent -= onAdExpandedEvent;
        MoPubManager.onAdCollapsedEvent -= onAdCollapsedEvent;

        MoPubManager.onInterstitialLoadedEvent -= onInterstitialLoadedEvent;
        MoPubManager.onInterstitialFailedEvent -= onInterstitialFailedEvent;
        MoPubManager.onInterstitialShownEvent -= onInterstitialShownEvent;
        MoPubManager.onInterstitialClickedEvent -= onInterstitialClickedEvent;
        MoPubManager.onInterstitialDismissedEvent -= onInterstitialDismissedEvent;
        MoPubManager.onInterstitialExpiredEvent -= onInterstitialExpiredEvent;

        MoPubManager.onRewardedVideoLoadedEvent -= onRewardedVideoLoadedEvent;
        MoPubManager.onRewardedVideoFailedEvent -= onRewardedVideoFailedEvent;
        MoPubManager.onRewardedVideoExpiredEvent -= onRewardedVideoExpiredEvent;
        MoPubManager.onRewardedVideoShownEvent -= onRewardedVideoShownEvent;
        MoPubManager.onRewardedVideoFailedToPlayEvent -= onRewardedVideoFailedToPlayEvent;
        MoPubManager.onRewardedVideoReceivedRewardEvent -= onRewardedVideoReceivedRewardEvent;
        MoPubManager.onRewardedVideoClosedEvent -= onRewardedVideoClosedEvent;
        MoPubManager.onRewardedVideoLeavingApplicationEvent -= onRewardedVideoLeavingApplicationEvent;
    }


    // Banner Events

    void onAdLoadedEvent(float height)
    {
        Debug.Log("onAdLoadedEvent. height: " + height);
        TSUtil.RunLater(()=> {repositionBanner();}, 0.1f);
    }

    void onAdFailedEvent(string errorMsg)
    {
        Debug.Log("onAdFailedEvent: " + errorMsg);
    }

    void onAdClickedEvent(string adUnitId)
    {
        Debug.Log("onAdClickedEvent: " + adUnitId);
    }

    void onAdExpandedEvent(string adUnitId)
    {
        Debug.Log("onAdExpandedEvent: " + adUnitId);
    }

    void onAdCollapsedEvent(string adUnitId)
    {
        Debug.Log("onAdCollapsedEvent: " + adUnitId);
    }


    // Interstitial Events

    void onInterstitialLoadedEvent(string adUnitId)
    {
        Debug.Log("onInterstitialLoadedEvent: " + adUnitId);
    }

    void onInterstitialFailedEvent(string errorMsg)
    {
        Debug.Log("onInterstitialFailedEvent: " + errorMsg);
    }

    void onInterstitialShownEvent(string adUnitId)
    {
        Debug.Log("onInterstitialShownEvent: " + adUnitId);
    }

    void onInterstitialClickedEvent(string adUnitId)
    {
        Debug.Log("onInterstitialClickedEvent: " + adUnitId);
    }

    void onInterstitialDismissedEvent(string adUnitId)
    {
        Debug.Log("onInterstitialDismissedEvent: " + adUnitId);
        requestFullScreen();
    }

    void onInterstitialExpiredEvent(string adUnitId)
    {
        Debug.Log("onInterstitialExpiredEvent: " + adUnitId);
        requestFullScreen();
    }


    // Rewarded Video Events

    void onRewardedVideoLoadedEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoLoadedEvent: " + adUnitId);
    }

    void onRewardedVideoFailedEvent(string errorMsg)
    {
        Debug.Log("onRewardedVideoFailedEvent: " + errorMsg);
    }

    void onRewardedVideoExpiredEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoExpiredEvent: " + adUnitId);
    }

    void onRewardedVideoShownEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoShownEvent: " + adUnitId);
    }

    void onRewardedVideoFailedToPlayEvent(string errorMsg)
    {
        Debug.Log("onRewardedVideoFailedToPlayEvent: " + errorMsg);
    }

    void onRewardedVideoReceivedRewardEvent(MoPubManager.RewardedVideoData rewardedVideoData)
    {
        Debug.Log("onRewardedVideoReceivedRewardEvent: " + rewardedVideoData);
    }

    void onRewardedVideoClosedEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoClosedEvent: " + adUnitId);
    }

    void onRewardedVideoLeavingApplicationEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoLeavingApplicationEvent: " + adUnitId);
    }

#endif
}
