﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SyncValues : Photon.MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			stream.SendNext (gameObject.transform.position);
			stream.SendNext (gameObject.transform.rotation);
			stream.SendNext (gameObject.GetComponent<Image>().color);
		}
		else
		{
			gameObject.transform.position = (Vector3)stream.ReceiveNext();
			gameObject.transform.rotation = (Quaternion)stream.ReceiveNext();
			gameObject.GetComponent<Image>().color = (Color)stream.ReceiveNext();
		}
	}
}
