﻿using UnityEngine;
using System.Collections;

public class NetworkManager : Photon.MonoBehaviour {
	GameManager gameManager;
	public GameObject panel;
	GameObject playerClone;
	// Use this for initialization

	void Start () {

		PhotonNetwork.ConnectUsingSettings("0.1");
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}

	void OnJoinedLobby()
	{
		PhotonNetwork.JoinRandomRoom();
	}
	
	void OnPhotonRandomJoinFailed()
	{
		Debug.Log("cant create room");
		PhotonNetwork.CreateRoom("nutty");
	}
	
	void OnJoinedRoom()
	{
		Debug.Log ("Joined Room Name : " + PhotonNetwork.room.name);
		//PhotonNetwork.Instantiate ("FPSController", Vector3.up, Quaternion.identity, 0);
	
		playerClone = PhotonNetwork.Instantiate ("Cube", Vector3.up, Quaternion.identity, 0);

		this.photonView.RPC ("SetParentOfGo", PhotonTargets.AllBuffered, this.playerClone.GetComponent<PhotonView>().viewID);
	}

	// Update is called once per frame
	void Update () {
	
	}


	[PunRPC]
	void CheckPlayerCount()
	{
		Debug.Log ("After Players Count" + PhotonNetwork.playerList.Length);
	}

	[PunRPC]
	void SetParentOfGo(int viewID)
	{
		print (viewID);
		Transform pl = PhotonView.Find (viewID).gameObject.transform;
		pl.SetParent(panel.transform);
		pl.transform.localPosition = Vector3.zero;
	}

}
