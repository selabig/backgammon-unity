using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Player : Photon.MonoBehaviour {

	public float speed = 1;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (photonView.isMine) {
			MovePlayer ();
		}

	}

	public void ChangeColour()
	{
		if (photonView.isMine)
			photonView.RPC ("SendColourChange", PhotonTargets.AllBuffered, gameObject.GetComponent<PhotonView>().viewID, Random.value, Random.value, Random.value);
	}

	public void MovePlayer()
	{
		if (Input.GetKey (KeyCode.A)) {
			gameObject.transform.Translate(-speed * Time.deltaTime, 0, 0);
		}
		if (Input.GetKey (KeyCode.D)) {
			gameObject.transform.Translate(speed * Time.deltaTime, 0, 0);
		}
	}

	[PunRPC]
	void SendColourChange(int viewID, float r, float g, float b)
	{
		GameObject pl = PhotonView.Find (viewID).gameObject;
		pl.GetComponent<Image> ().color = new Color (r, g, b);
	}
}
