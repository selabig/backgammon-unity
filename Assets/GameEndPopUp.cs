﻿using UnityEngine;
using System.Collections;

public class GameEndPopUp : BaseMenu {

	// Use this for initialization

	
	public void GameEndPopUpYesBtnCallback()
	{
		MenuManager.Instance.PushMenu (GameManager.GameState.GamePlay);
	}
	public void GameEndPopUpNoBtnCallback()
	{
		MenuManager.Instance.PushMenu (GameManager.GameState.LevelSelection);

	}

}