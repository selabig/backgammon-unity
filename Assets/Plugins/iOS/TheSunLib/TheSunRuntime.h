//
//  TheSunRuntime.h
//  TheSunRuntime

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIApplication(SupressWarnings)
- (BOOL)application:(UIApplication *)application ts_didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
//thesun
- (void) ts_applicationDidBecomeActive:(UIApplication *)application;
- (void) ts_applicationWillEnterForeground:(UIApplication *)application;

@end


