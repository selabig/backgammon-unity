//
//  TheSunUnityManager.m
//  TheSun
//
//  Created by TheSun on 05/02/14
//  Copyright (c) 2014 THESUN. All rights reserved.
//

#import "TheSunUnityManager.h"
#import <sys/utsname.h>
#import "ViewUtils.h"
#import "MPAdView.h"


UIViewController *UnityGetGLViewController();
//void UnityPause( bool shouldPause );
void UnitySendMessage( const char * className, const char * methodName, const char * param );


@implementation TheSunUnityManager

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

+ (TheSunUnityManager*)sharedManager
{
    static TheSunUnityManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once( &onceToken,
                  ^{
                      instance = [[TheSunUnityManager alloc] init];
                      
                      instance.arrMopubBanner = [NSMutableArray arrayWithCapacity:100];
                      
                  });
    
    return instance;
}

- (id)init
{
    if((self = [super init]))
    {
        [self performSelector:@selector(refreshPositionBanner) withObject:nil afterDelay:10.0];
    }
    return self;
}

- (void) refreshPositionBanner
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return;
    }
    
    [self repositionMopubBanner];
    [self performSelector:@selector(refreshPositionBanner) withObject:nil afterDelay:5.0];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public
- (void) shareMessage: (NSString *) message
{
    NSMutableArray *shareItems = [[NSMutableArray alloc] initWithCapacity:10];
    NSString * strMsg = message;
    [shareItems addObject:strMsg];
    
    UIActivityViewController *shareController = [[UIActivityViewController alloc]
                                                 initWithActivityItems: shareItems applicationActivities :nil];
    
    //    [shareController setValue:@"Sharing" forKey:@"subject"];
    [shareController setValue:strMsg forKey:@"subject"];
    
    shareController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeSaveToCameraRoll]; //UIActivityTypePostToFacebook
    
    shareController.completionHandler = ^(NSString *activityType, BOOL completed)
    {
        //NSLog(@" activityType: %@", activityType);
        //NSLog(@" completed: %i", completed);
    };
    
    UIViewController * rvcl = [self getRootViewController];
    
    //configure for iPad, note if you do not your app will not pass app store review
    if(nil != shareController.popoverPresentationController)
    {
        shareController.popoverPresentationController.sourceView = rvcl.view;
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.size.height /= 2;
        shareController.popoverPresentationController.sourceRect = frame;
    }
    
    [rvcl presentViewController:shareController animated:YES completion:nil];
}


- (void) sendMailFeedback
{
    if ([MFMailComposeViewController canSendMail] == NO)
        return;
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * iOSVersion = [[UIDevice currentDevice] systemVersion];
    NSString * deviceName = [self deviceModelName];//[UIDevice currentDevice].name;
    
    NSLocale *locale = [NSLocale currentLocale];
    
    NSString * language = [locale objectForKey:NSLocaleLanguageCode];
    NSString * country = [locale objectForKey:NSLocaleCountryCode];
    
    NSString * bundleName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    
    NSString * strSubject = [NSString stringWithFormat:@"%@ version %@", bundleName, version];
    
    
    NSString * strMsg = [NSString stringWithFormat:@"%@\n\n\n\n\n\n\n\n\n\n\n\n\nContact us details - \nApp Name: %@\nModel: %@\nSystem Version: %@\nLanguage: %@\nCountry: %@\nApp Version: %@",
                         SEND_MAIL_MSG, bundleName, deviceName, iOSVersion, language, country, version];
    
    
#ifdef SEND_MAIL_SUBJECT
    [picker setSubject:SEND_MAIL_SUBJECT];
#else
    //    [picker setSubject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
    [picker setSubject:strSubject];
    
#endif
    
    [picker setMessageBody:strMsg isHTML:NO];
    [picker setToRecipients:[NSArray arrayWithObjects:SEND_MAIL_RECIPT,nil]];
    if (picker){
        UIViewController * rootVCL = [self getRootViewController];
        [rootVCL presentViewController:picker animated:YES completion:nil ];
        
        //        [self.rootViewController presentModalViewController:picker animated:YES];
    }
    
    //    [picker release];
}


-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    NSString * message = @"";
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"Email Cancelled";
            break;
        case MFMailComposeResultSaved:
            message = @"Email Saved";
            break;
        case MFMailComposeResultSent:
            message = @"Email Sent";
            break;
        case MFMailComposeResultFailed:
            message = @"Email Failed";
            break;
        default:
            message = @"Email Not Sent";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:message
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            //            [alert release];
            break;
    }
    //    [self.rootViewController dismissViewControllerAnimated:YES completion:NULL];
    UIViewController * rootVCL = [self getRootViewController];
    [rootVCL dismissViewControllerAnimated:YES completion:NULL];
    
}


- (id)getRootViewController {
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(window in windows) {
            if (window.windowLevel == UIWindowLevelNormal) {
                break;
            }
        }
    }
    
    UIViewController * vcl  = [self iterateSubViewsForViewController:window]; // iOS 8+ deep traverse
#if 0
    if ([vcl isKindOfClass:[UINavigationController class]]) {
        UINavigationController * nav = (UINavigationController *)vcl;
        return [nav visibleViewController];
    }
#endif
    
    return vcl;
    
#if 0
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (!window)
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    
    
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(window in windows) {
            if (window.windowLevel == UIWindowLevelNormal) {
                break;
            }
        }
    }
    
    for (UIView *subView in [window subviews])
    {
        UIResponder *responder = [subView nextResponder];
        if([responder isKindOfClass:[UIViewController class]]) {
            return [self topMostViewController: (UIViewController *) responder];
        }
    }
    
    return [self topMostViewController:window.rootViewController];
#endif
}


- (id)iterateSubViewsForViewController:(UIView *) parentView {
    for (UIView *subView in [parentView subviews]) {
        UIResponder *responder = [subView nextResponder];
        if([responder isKindOfClass:[UIViewController class]]) {
            return [self topMostViewController: (UIViewController *) responder];
        }
        id found = [self iterateSubViewsForViewController:subView];
        if( nil != found) {
            return found;
        }
    }
    return nil;
}


- (UIViewController *) topMostViewController: (UIViewController *) controller {
    BOOL isPresenting = NO;
    do {
        // this path is called only on iOS 6+, so -presentedViewController is fine here.
        UIViewController *presented = [controller presentedViewController];
        isPresenting = presented != nil;
        if(presented != nil) {
            controller = presented;
        }
        
    } while (isPresenting);
    
    return controller;
}


- (NSString*)deviceModelName {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *machineName = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    //MARK: More official list is at
    //http://theiphonewiki.com/wiki/Models
    //MARK: You may just return machineName. Following is for convenience
    
    NSDictionary *commonNamesDictionary =
    @{
      @"i386":     @"i386 Simulator",
      @"x86_64":   @"x86_64 Simulator",
      
      @"iPhone1,1":    @"iPhone",
      @"iPhone1,2":    @"iPhone 3G",
      @"iPhone2,1":    @"iPhone 3GS",
      @"iPhone3,1":    @"iPhone 4",
      @"iPhone3,2":    @"iPhone 4(Rev A)",
      @"iPhone3,3":    @"iPhone 4(CDMA)",
      @"iPhone4,1":    @"iPhone 4S",
      @"iPhone5,1":    @"iPhone 5(GSM)",
      @"iPhone5,2":    @"iPhone 5(GSM+CDMA)",
      @"iPhone5,3":    @"iPhone 5c(GSM)",
      @"iPhone5,4":    @"iPhone 5c(GSM+CDMA)",
      @"iPhone6,1":    @"iPhone 5s(GSM)",
      @"iPhone6,2":    @"iPhone 5s(GSM+CDMA)",
      
      @"iPhone7,1":    @"iPhone 6+ (GSM+CDMA)",
      @"iPhone7,2":    @"iPhone 6 (GSM+CDMA)",
      
      @"iPhone8,1":    @"iPhone 6S (GSM+CDMA)",
      @"iPhone8,2":    @"iPhone 6S+ (GSM+CDMA)",
      @"iPhone8,4":    @"iPhone SE",
      
      @"iPad1,1":  @"iPad",
      @"iPad2,1":  @"iPad 2(WiFi)",
      @"iPad2,2":  @"iPad 2(GSM)",
      @"iPad2,3":  @"iPad 2(CDMA)",
      @"iPad2,4":  @"iPad 2(WiFi Rev A)",
      @"iPad2,5":  @"iPad Mini 1G (WiFi)",
      @"iPad2,6":  @"iPad Mini 1G (GSM)",
      @"iPad2,7":  @"iPad Mini 1G (GSM+CDMA)",
      @"iPad3,1":  @"iPad 3(WiFi)",
      @"iPad3,2":  @"iPad 3(GSM+CDMA)",
      @"iPad3,3":  @"iPad 3(GSM)",
      @"iPad3,4":  @"iPad 4(WiFi)",
      @"iPad3,5":  @"iPad 4(GSM)",
      @"iPad3,6":  @"iPad 4(GSM+CDMA)",
      
      @"iPad4,1":  @"iPad Air(WiFi)",
      @"iPad4,2":  @"iPad Air(GSM)",
      @"iPad4,3":  @"iPad Air(GSM+CDMA)",
      
      @"iPad5,3":  @"iPad Air 2 (WiFi)",
      @"iPad5,4":  @"iPad Air 2 (GSM+CDMA)",
      
      @"iPad4,4":  @"iPad Mini 2G (WiFi)",
      @"iPad4,5":  @"iPad Mini 2G (GSM)",
      @"iPad4,6":  @"iPad Mini 2G (GSM+CDMA)",
      
      @"iPad4,7":  @"iPad Mini 3G (WiFi)",
      @"iPad4,8":  @"iPad Mini 3G (GSM)",
      @"iPad4,9":  @"iPad Mini 3G (GSM+CDMA)",
      
      @"iPod1,1":  @"iPod 1st Gen",
      @"iPod2,1":  @"iPod 2nd Gen",
      @"iPod3,1":  @"iPod 3rd Gen",
      @"iPod4,1":  @"iPod 4th Gen",
      @"iPod5,1":  @"iPod 5th Gen",
      @"iPod7,1":  @"iPod 6th Gen",
      };
    
    NSString *deviceName = commonNamesDictionary[machineName];
    
    if (deviceName == nil) {
        deviceName = machineName;
    }
    
    return deviceName;
}






- (void)showMesasgeDemo
{
    //UnityGetGLViewController()
}

/*
 - (void)showOfferWallWithApplicationKey:(NSString*)applicationKey userId:(NSString*)userId shouldGetLocation:(BOOL)getLocation extraParameters:(NSDictionary*)parameters
 {
	UnityPause( true );
	[[SupersonicAdsPublisher sharedSupersonicAds] showOfferWallWithApplicationKey:applicationKey
 userId:userId
 delegate:self
 shouldGetLocation:getLocation
 extraParameters:parameters
 parentViewController:UnityGetGLViewController()];
 }
 */


- (void) repositionMopubBanner
{
    if (self.arrMopubBanner.count <= 0)
        return;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [self repositionMopubBannerPhase2];
        return;
    }
    
    for (MPAdView * item in self.arrMopubBanner) {
        
        UIView * parrent = item.superview;
        
        //        NSLog(@"TheSunLog: adsize1: %f %f", item.adContentViewSize.width, item.adContentViewSize.height);
        CGAffineTransform transform = CGAffineTransformIdentity;
        item.transform = transform;
        item.width = 320;//item.adContentViewSize.width;
        item.height = 50;//item.adContentViewSize.height;
        
        item.bottom = parrent.top;
        item.left = parrent.left;
        item.center = CGPointMake(parrent.center.x, item.center.y) ;
        
        
        //        NSLog(@"TheSunLog: adsize2: %f %f", item.adContentViewSize.width, item.adContentViewSize.height);
    }
    [self repositionMopubBannerPhase2];
    //    [self performSelector:@selector(repositionMopubBannerPhase2) withObject:nil afterDelay:2.0];
}


- (void) repositionMopubBannerPhase2
{
    if (self.arrMopubBanner.count <= 0)
        return;
    
    NSLog(@"TheSunLog: banner count = %d", (int)self.arrMopubBanner.count);
    
    for (MPAdView * item in self.arrMopubBanner) {
        
        //item.testing = true;
        
        //item.backgroundColor = [UIColor redColor];
        
        UIView * parrent = item.superview;
        
        if (self.bannerViewMode == 0) //portrait
        {
            //            [item rotateToOrientation:UIInterfaceOrientationPortrait];
            //            [item lockNativeAdsToOrientation:MPNativeAdOrientationPortrait];
            
            CGAffineTransform transform = CGAffineTransformIdentity;
            item.transform = transform;
            
            item.top = parrent.top;
            item.left = parrent.left;
            item.center = CGPointMake(parrent.center.x, item.center.y) ;
        }
        else if (self.bannerViewMode == 1) //landscape
        {
            //            [item rotateToOrientation:UIInterfaceOrientationLandscapeRight];
            //            [item lockNativeAdsToOrientation:MPNativeAdOrientationPortrait];
            
            CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI_2);
            item.transform = transform;
            
            item.top = parrent.top;
            item.left = parrent.left;
            item.center = CGPointMake(item.center.x, parrent.center.y);
        }
        else if (self.bannerViewMode == 2) //landscape ipad
        {
            item.bottom = parrent.bottom;
            item.center = CGPointMake(parrent.center.x, item.center.y);
        }
        //        NSLog(@"TheSunLog: self.bannerViewMode = %d", self.bannerViewMode);
        //        NSLog(@"TheSunLog: %f %f %f %f", item.left, item.top, item.width, item.height);
        //NSLog(@"TheSunLog: parrent %f %f %f %f", parrent.left, parrent.top, parrent.width, parrent.height);
        
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - testDelegate

- (void)testDidClose
{
    //	UnityPause( false );
    UnitySendMessage( "TheSunUnityManager", "testDidClose", "" );
}


///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - BrandConnectDelegate
#ifdef SUPERSONIC_OFFERWALL_KEY
- (void)testDidInitWithCampaignInfo:(NSDictionary*)campaignInfo
{
    UnitySendMessage( "TheSunUnityManager", "testDidInitWithCampaignInfo", [campaignInfo JSONString].UTF8String );
}

- (void)brandConnectDidFailInitWithError:(NSDictionary*)error
{
    UnitySendMessage( "TheSunUnityManager", "brandConnectDidFailInitWithError", [error JSONString].UTF8String );
}

- (void)brandConnectWindowDidClose
{
    UnitySendMessage( "TheSunUnityManager", "brandConnectWindowDidClose", "" );
}

- (void)brandConnectDidFinishAd:(NSDictionary*)campaignInfo
{
    UnitySendMessage( "UIController", "brandConnectDidFinishAd", [campaignInfo JSONString].UTF8String );
}

#endif
@end


#ifdef __cplusplus
extern "C" {
#endif
    
#define GetStringParam( _x_ ) ( _x_ != NULL ) ? [NSString stringWithUTF8String:_x_] : [NSString stringWithUTF8String:""]
#define GetStringParamOrNil( _x_ ) ( _x_ != NULL && strlen( _x_ ) ) ? [NSString stringWithUTF8String:_x_] : nil
    
    void _tsShowMesasgeDemo()
    {
        [[TheSunUnityManager sharedManager] showMesasgeDemo];
    }
    
    void _tsShowOfferWall( const char * applicationKey, const char * userId, BOOL shouldGetLocation, const char * additionalParameters )
    {
        //		NSDictionary *params = nil;
        //		NSString *json = GetStringParamOrNil( additionalParameters );
        //		if( json )
        //			params = (NSDictionary*)[json objectFromJSONString];
        //
        
        
#ifdef SUPERSONIC_OFFERWALL_KEY
        //		[[SuperSonicManager sharedManager] showOfferWallWithApplicationKey:GetStringParam( applicationKey )
        //																	userId:[[TheSunUnityManager sharedManager] superSonicUserID]
        //														 shouldGetLocation:shouldGetLocation
        //														   extraParameters:params];
        NSLog(@"TheSunLog: native ->showOfferWallWithApplicationKey");
#endif
        
    }
    
    void sendMailFeedback()
    {
        [[TheSunUnityManager sharedManager] sendMailFeedback];
    }
    void shareMessage( const char * message)
    {
        [[TheSunUnityManager sharedManager] shareMessage: GetStringParam( message )];		
    }
    
    void repositionMopubBanner(int viewMode)
    {
        [TheSunUnityManager sharedManager].bannerViewMode = viewMode;
        [[TheSunUnityManager sharedManager] repositionMopubBanner];
    }
    
    
    
#ifdef __cplusplus
}
#endif
