//
//  TheSunUnityManager.h
//  TheSun
//
//  Created by TheSun on 05/02/14
//  Copyright (c) 2014 THESUN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AdSupport/ASIdentifierManager.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#define SEND_MAIL_MSG @"Writing a few suggestions to improve your game or just to say hi :)"
#define SEND_MAIL_RECIPT @"support@acfreefungames.zendesk.com"

@interface TheSunUnityManager : NSObject <MFMailComposeViewControllerDelegate>

+ (TheSunUnityManager*)sharedManager;

@property (nonatomic, strong) NSMutableArray * arrMopubBanner;
@property (nonatomic, assign) int bannerViewMode;

- (void)showMesasgeDemo;
- (void) sendMailFeedback;
- (void) shareMessage: (NSString *) message;
- (void) repositionMopubBanner;

// - (void)showOfferWallWithApplicationKey:(NSString*)applicationKey userId:(NSString*)userId shouldGetLocation:(BOOL)getLocation extraParameters:(NSDictionary*)parameters;


@end
