//
//  TheSunRuntime.m
//  TheSunRuntime

#import "TheSunRuntime.h"
#import <objc/runtime.h>
#import "AVFoundation/AVFoundation.h"
#import "TheSunUnityManager.h"
#import "MPAdView.h"

void UnitySendMessage( const char * className, const char * methodName, const char * param );
//void UnityPause( bool pause );





static void swizze(Class class, SEL fromChange, SEL toChange, IMP impl, const char * signature)
{
    Method method = nil;
    method = class_getInstanceMethod(class, fromChange);
    
    if (method) {
        //method exists add a new method and swap with original
        class_addMethod(class, toChange, impl, signature);
        method_exchangeImplementations(class_getInstanceMethod(class, fromChange), class_getInstanceMethod(class, toChange));
    } else {
        //just add as orignal method
        class_addMethod(class, fromChange, impl, signature);
    }
}

@interface MPAdView(SupressWarnings)
- (void)myAdViewDidLoadAd:(MPAdView *)view;
- (void)myAdViewDidFailToLoadAd:(MPAdView *)view;

@end


@implementation MPAdView(Tracking)



+ (void) load {
    
#if 1
    method_exchangeImplementations(class_getInstanceMethod(self, @selector(setDelegate:)), class_getInstanceMethod(self, @selector(mopubSetDelegate:)));
    
#else
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        SEL originalSelector = @selector(initWithAdUnitId:size:);
        SEL swizzledSelector = @selector(xxx_initWithAdUnitId:size:);
        
        Method originalMethod = class_getInstanceMethod(self, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(self, swizzledSelector);
        
        // When swizzling a class method, use the following:
        // Class class = object_getClass((id)self);
        // ...
        // Method originalMethod = class_getClassMethod(class, originalSelector);
        // Method swizzledMethod = class_getClassMethod(class, swizzledSelector);
        
        BOOL didAddMethod = false;
        if (originalMethod)
        {
            didAddMethod = class_addMethod(class,
                                           swizzledSelector,
                                           method_getImplementation(swizzledMethod),
                                           method_getTypeEncoding(swizzledMethod));
        }
        
        if (didAddMethod) {
            class_replaceMethod(class,
                                swizzledSelector,
                                method_getImplementation(originalMethod),
                                method_getTypeEncoding(originalMethod));
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
#endif
}

- (id)xxx_initWithAdUnitId:(NSString *)adUnitId size:(CGSize)size
{
#if 1
    MPAdView * adView = [self xxx_initWithAdUnitId:adUnitId size:size];
    
#else
    MPAdView * adView = nil;
    if ([self respondsToSelector:@selector(initWithAdUnitId:size:)]) {
        adView = [self xxx_initWithAdUnitId:adUnitId size:size];
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
        {
            [adView lockNativeAdsToOrientation:MPNativeAdOrientationPortrait];
        }
    }
#endif
    
    //    NSLog(@"TheSunLog: %@", adView.adUnitId);
    //
    //    if ([[TheSunUnityManager sharedManager].arrMopubBanner containsObject:adView] == false)
    //    {
    //        [[TheSunUnityManager sharedManager].arrMopubBanner addObject:adView];
    //    }
    
    return adView;
}

void dymanicAdViewDidLoadAd(id self, SEL _cmd, MPAdView * adView) {
    NSLog(@"TheSunLog: %@", adView.adUnitId);
    
    if ([[TheSunUnityManager sharedManager].arrMopubBanner containsObject:adView] == false)
    {
        [[TheSunUnityManager sharedManager].arrMopubBanner addObject:adView];
    }
    if ([self respondsToSelector:@selector(adViewDidLoadAd:)]) {
        [self myAdViewDidLoadAd:adView];
    }
}


void dymanicAdViewDidFailToLoadAd(id self, SEL _cmd, id adView) {
    NSLog(@"dymanicAdViewDidFailToLoadAd");
}


- (void) mopubSetDelegate:(id<MPAdViewDelegate>)delegate {
    
    static Class delegateClass = nil;
    
    //do not swizzle the same class twice
    if(delegateClass == [delegate class])
    {
        [self mopubSetDelegate:delegate];
        return;
    }
    
    delegateClass = [delegate class];
    
    //    swizze([delegate class], @selector(application:didFinishLaunchingWithOptions:),
    //           @selector(application:ts_didFinishLaunchingWithOptions:), (IMP)dynamicDidFinishLaunching, "v@:::");
    //
    //    //thesun
    //    swizze([delegate class], @selector(applicationDidBecomeActive:),
    //           @selector(ts_applicationDidBecomeActive:), (IMP)dymanicApplicationDidBecomeActive, "v@:::");
    
    
    //    swizze([delegate class], @selector(adViewDidFailToLoadAd:),
    //           @selector(myAdViewDidFailToLoadAd:), (IMP)dymanicAdViewDidFailToLoadAd, "v@:::");
    
    swizze([delegate class], @selector(adViewDidLoadAd:),
           @selector(myAdViewDidLoadAd:), (IMP)dymanicAdViewDidLoadAd, "v@:::");
    
    
    
    [self mopubSetDelegate:delegate];
}




@end



@implementation UIApplication(TheSunRuntime)

BOOL dynamicDidFinishLaunching(id self, SEL _cmd, id application, id launchOptions) {
    BOOL result = YES;
    
    if ([self respondsToSelector:@selector(application:ts_didFinishLaunchingWithOptions:)]) {
        result = (BOOL) [self application:application ts_didFinishLaunchingWithOptions:launchOptions];
    } else {
        [self applicationDidFinishLaunching:application];
        result = YES;
    }
    
    //thesun
    //don't stop current app music playing.
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    
    
    return result;
}
//thesun
void dymanicApplicationDidBecomeActive(id self, SEL _cmd, id application) {
    if ([self respondsToSelector:@selector(applicationDidBecomeActive:)]) {
        [self ts_applicationDidBecomeActive:application];
    }
}
void dymanicApplicationWillEnterForeground(id self, SEL _cmd, id application) {
    if ([self respondsToSelector:@selector(applicationWillEnterForeground:)]) {
        [self ts_applicationWillEnterForeground:application];
    }
    
    //#ifdef JURRASIC
    //	UnitySendMessage( "AdNetworkController", "ApplicationWillEnterForeground", "" );
    //	UnitySendMessage( "ExitFromGame", "ApplicationWillEnterForeground", "" );
    //
    //#else
    //	UnitySendMessage( "GameController", "ApplicationWillEnterForeground", "" );
    //	UnitySendMessage( "UIController", "ApplicationWillEnterForeground", "" );
    //#endif
    
}


- (void) ts_setDelegate:(id<UIApplicationDelegate>)delegate {
    
    static Class delegateClass = nil;
    
    //do not swizzle the same class twice
    if(delegateClass == [delegate class])
    {
        [self ts_setDelegate:delegate];
        return;
    }
    
    delegateClass = [delegate class];
    
    swizze([delegate class], @selector(application:didFinishLaunchingWithOptions:),
           @selector(application:ts_didFinishLaunchingWithOptions:), (IMP)dynamicDidFinishLaunching, "v@:::");
    
    //thesun
    swizze([delegate class], @selector(applicationDidBecomeActive:),
           @selector(ts_applicationDidBecomeActive:), (IMP)dymanicApplicationDidBecomeActive, "v@:::");
    swizze([delegate class], @selector(applicationWillEnterForeground:),
           @selector(ts_applicationWillEnterForeground:), (IMP)dymanicApplicationWillEnterForeground, "v@:::");
    
    
    [self ts_setDelegate:delegate];
}

//- (void) ts_setApplicationIconBadgeNumber:(NSInteger) badgeNumber {
//	[self ts_setApplicationIconBadgeNumber:badgeNumber];
//}

+ (void) load {
    //	method_exchangeImplementations(class_getInstanceMethod(self, @selector(setApplicationIconBadgeNumber:)), class_getInstanceMethod(self, @selector(ts_setApplicationIconBadgeNumber:)));
    method_exchangeImplementations(class_getInstanceMethod(self, @selector(setDelegate:)), class_getInstanceMethod(self, @selector(ts_setDelegate:)));
    
    UIApplication *app = [UIApplication sharedApplication];
    NSLog(@"Initializing application: %@, %@", app, app.delegate);
}

@end






