using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.IO;

public static class MyBuildPostprocess
{
    [PostProcessBuild(999)]
    public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            string projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);

            string target = pbxProject.TargetGuidByName("Unity-iPhone");
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            pbxProject.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");

            pbxProject.SetBuildProperty(target, "CLANG_ENABLE_MODULES", "YES");

            pbxProject.SetBuildProperty(target, "DEBUG_INFORMATION_FORMAT", "dwarf");

            pbxProject.SetBuildProperty(target, "DEVELOPMENT_TEAM", "6C822E5J3E");


            //add framework & library
            pbxProject.AddFrameworkToProject(target, "CoreData.framework", true);

            pbxProject.AddFileToBuild(target, pbxProject.AddFile("usr/lib/libz.1.2.8.dylib", "Frameworks/libz.1.2.8.dylib", PBXSourceTree.Sdk));
            pbxProject.AddFileToBuild(target, pbxProject.AddFile("usr/lib/libsqlite3.0.dylib", "Frameworks/libsqlite3.0.dylib", PBXSourceTree.Sdk));

//pbxProject.RemoveFileFromBuild(target, pbxProject.FindFileGuidByProjectPath("Libraries/Plugins/Android"));


            pbxProject.WriteToFile(projectPath);


            // Get plist
            string plistPath = path + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            rootDict.SetString("NSCalendarsUsageDescription", "Calendars");
            rootDict.SetString("NSPhotoLibraryUsageDescription", "Photos");

            //add dict
            var dict = rootDict.CreateDict("NSAppTransportSecurity");
            dict.SetString("NSAllowsArbitraryLoads", "Yes");

            // Add array
            var array = rootDict.CreateArray("UISupportedInterfaceOrientations");
            array.AddString("UIInterfaceOrientationPortrait");

            array = rootDict.CreateArray("UISupportedInterfaceOrientations~ipad");
            array.AddString("UIInterfaceOrientationLandscapeLeft");
            array.AddString("UIInterfaceOrientationLandscapeRight");


            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
}