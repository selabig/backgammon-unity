﻿using UnityEngine;
using System.Collections;

public class TutorialPopUp : BaseMenu {

	public GameObject tutorialPopup;
	public UI2DSpriteAnimation spriteAnimation;
	void Start () 
	{
		Debug.Log ("Tutorial PopUp Start");

		TSUtil.LogEvent ( AnalyticConstants.GA_CATEGORY_TYPE_GAMEPLAY,AnalyticConstants.GA_ACTION_TYPE_PopUps,"Tutorial PopUp",1);



		spriteAnimation.ResetToBeginning ();

		spriteAnimation.Play();

	}
	public void tutorialPopUpOkBtnCallback()
	{
		tutorialPopup.GetComponent<TweenScale> ().PlayReverse ();
		Invoke("HidePopUp",1f);
	}
	
	void HidePopUp()
	{
		MenuManager.Instance.PopMenu ();	
	}

}