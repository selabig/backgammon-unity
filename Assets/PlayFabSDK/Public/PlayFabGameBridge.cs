﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Gun
{
    public float Frequency;
    public float ConeAngle;
    public float DamagePerSecond;
    public float HitSoundVolume;
    public float Pitch;
}

public static class PlayFabGameBridge 
{

    /// Game Attributes that are custom to the game.
    /// The player wins count
 
    public static int totalWins = 0;

    public static Dictionary<string, uint> LeaderboardHighScores = new Dictionary<string, uint>();
    public static bool leaderboardLoaded = false;
}
