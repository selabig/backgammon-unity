﻿using UnityEngine;
using System.Collections;

public class WaitingMultiPlayer : BaseMenu {

		
		public GameObject multiPlayerPopup;
		private bool isProcessing = false;
		
		public void multiPlayerPopUpCallback()
		{
			if (isProcessing == false)
			{
				isProcessing = true;
				multiPlayerPopup.GetComponent<TweenScale>().PlayReverse();
			//	Invoke("ExitPopUpAndMainMenu", 1f);
				Invoke("HidePopUp", 1f);

			}
		}
		

		

		
		void HidePopUp()
		{
			isProcessing = false;
			MenuManager.Instance.PopMenu();
		}
		
		void OnEnable()
		{
			if (isProcessing == false)
			{
				isProcessing = true;
				multiPlayerPopup.transform.localScale = new Vector3(0, 0, 0);
				multiPlayerPopup.GetComponent<TweenScale>().PlayForward();
				Invoke("CompletedProcessing", 1f);
			}
		}
		
		void CompletedProcessing()
		{
			isProcessing = false;
		}
	}
