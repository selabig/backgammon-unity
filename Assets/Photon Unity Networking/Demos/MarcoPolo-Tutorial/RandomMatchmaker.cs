using UnityEngine;

public class RandomMatchmaker : Photon.MonoBehaviour
{
    public  PhotonView myPhotonView;
    private bool isGameConfigured = false;
    // Use this for initialization
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

    void OnJoinedLobby()
    {
        Debug.Log("JoinRandom");
        PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed()
    {
        PhotonNetwork.CreateRoom(null);

    }

    void OnJoinedRoom()
    {
        myPhotonView = GameManager.Instance.gamePlayPhotonView;
      
        
    }

    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());

        if (PhotonNetwork.connectionStateDetailed == PeerState.Joined)
        {
           // Debug.LogWarning(PhotonNetwork.player.ID);
            bool shoutMarco = GameLogic.playerWhoIsIt == PhotonNetwork.player.ID;


            if ( isGameConfigured == false)
            {
                isGameConfigured = true;
               

                if (shoutMarco)
                {

                    myPhotonView.RPC("SelectPlayerTokens", PhotonTargets.All, true);
                }
                else
                {
                    myPhotonView.RPC("SelectPlayerTokens", PhotonTargets.All, false);
                }
            }


            if (shoutMarco && GUILayout.Button("Player 1 "))
            {
               // myPhotonView.RPC("Orange", PhotonTargets.All);
            }
            if (!shoutMarco && GUILayout.Button("Player 2"))
            {
                //myPhotonView.RPC("Brown", PhotonTargets.All);
            }
        }
    }
}
